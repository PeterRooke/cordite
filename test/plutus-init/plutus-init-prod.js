const { SSL_OP_EPHEMERAL_RSA } = require("constants")
/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const fs = require("fs")
eval(fs.readFileSync("./common.js", "utf-8"))

function getSalt() {
  if (process.argv.length > 2 && process.argv.slice(2)[0] == 'golive') {
    return ""
  }
  return new Date().getTime()
}

async function entireTest() {

  let salt = getSalt()

  let saltedDaoName = `plutus${salt}`
  let tokenSymbol = `XDC${salt}`
  
  const [csl, bcb, lab, dasl] = await connectToProd()

  console.log("ok, got here")
  let daos = await csl.dao.daoInfo(saltedDaoName)
  console.log(`there were ${daos.length} daos with name ${saltedDaoName}`)

  logStep(`Creating dao`)
  const daoState = await createPlutusDao(csl, saltedDaoName, tokenSymbol)

  logStep(`Adding bcb-csl node to the dao`)
  const cslName = await getX500Name(csl)
  await registerAsPlutusMember(bcb, saltedDaoName, cslName) 

  logStep(`Adding lab577 node to the dao`)
  await registerAsPlutusMember(lab, saltedDaoName, cslName) 

  logStep(`Adding dasl node to the dao`)
  await registerAsPlutusMember(dasl, saltedDaoName, cslName) 

  const proposalKey = await createPlutusProposal(csl, daoState.daoKey, 50)

  logStep(`Voting for and accepting proposal`)
  await voteForProposal(bcb, proposalKey, "DOWN")
  await voteForProposal(lab, proposalKey, "DOWN")
  await voteForProposal(dasl, proposalKey, "DOWN")
  await acceptProposal(csl, proposalKey, "ACCEPTED", "DOWN")

  logStep(`Checking balances on nodes`)
  const symbol = getTokenDescriptorForDao(daoState).symbol
  await checkBalanceForAccount(csl, saltedDaoName, symbol, 250000.00)
  await checkBalanceForAccount(bcb, saltedDaoName, symbol, 250000.00)
  await checkBalanceForAccount(lab, saltedDaoName, symbol, 250000.00)
  await checkBalanceForAccount(dasl, saltedDaoName, symbol, 250000.00)

  logStep(`Creating next proposal`)
  const proposalKey2 = await createPlutusProposal(csl, daoState.daoKey, 50)

  logStep(`Voting for next proposal`)
  await voteForProposal(bcb, proposalKey2, "DOWN")
  await voteForProposal(lab, proposalKey2, "DOWN")
  await voteForProposal(dasl, proposalKey2, "DOWN")

  process.exit(0)
}

try {
  entireTest()
} catch (err) {
  logError(err)
}
