/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const fs = require("fs")
eval(fs.readFileSync("./common.js", "utf-8"))

async function entireTest() {

  const salt = new Date().getTime()
  const saltedDaoName = `testDao-${salt}`
  const tokenSymbol = `XDS-${salt}`
  
  const [csl, plu, ura] = await connectToCorner()

  const daos = await csl.dao.listDaos()
  console.log(`there were ${daos.length} daos`)
  daos.forEach(element => {
    console.log(`${element.name}`)
  });

  process.exit(0)
}

entireTest()
