/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const fs = require("fs")
eval(fs.readFileSync("./common.js", "utf-8"))

function displayDao(dao) {
  if (process.argv.length > 2) {
    return dao.name == process.argv.slice(2)[0]
  }
  return true
}

async function entireTest() {

  const salt = new Date().getTime()
  const saltedDaoName = `testDao-${salt}`
  const tokenSymbol = `XDS-${salt}`
  
  const [csl, plu, ura] = await connectToCorner()

  const daos = await csl.dao.listDaos()
  daos.filter(displayDao).forEach((daoState) => {
    console.log(`\ndao: ${daoState.name}`)
    console.log(`\tPlutusModelData:`)
    const pmd = daoState.modelDataMap["io.cordite.dao.plutus.PlutusModelData"]
    console.log(`\t\tcurrentMintingRate: ${pmd.currentMintingRate}`)
    console.log(`\t\ttotalIssued: ${pmd.totalIssued}`)
    console.log(`\t\tminNextProposal: ${pmd.minNextProposal}`)
    console.log(`\t\tproposalPeriod: ${pmd.proposalPeriod}`)
    console.log(`\tMembers:`)
    daoState.members.forEach(member => console.log(`\t\t${member.name}`))
  })
  process.exit(0)
}

entireTest()
