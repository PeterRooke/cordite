const { SSL_OP_EPHEMERAL_RSA } = require("constants")
/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const fs = require("fs")
eval(fs.readFileSync("./common.js", "utf-8"))

async function entireTest() {

  let salt = new Date().getTime()
  let saltedDaoName = `testDao-${salt}`
  let tokenSymbol = `XDS-${salt}`
  
  const [csl, plu, ura] = await connectToCorner()

  console.log("ok, got here")
  let daos = await csl.dao.daoInfo(saltedDaoName)
  console.log(`there were ${daos.length} daos with name ${saltedDaoName}`)

  logStep(`Creating dao`)
  const daoState = await createPlutusDao(csl, saltedDaoName, tokenSymbol, 30)

  logStep(`Adding plutus node to the dao`)
  const cslName = await getX500Name(csl)
  await registerAsPlutusMember(plu, saltedDaoName, cslName) 

  logStep(`Adding uratus node to the dao`)
  await registerAsPlutusMember(ura, saltedDaoName, cslName) 

  const proposalKey = await createPlutusProposal(csl, daoState.daoKey, 50)

  logStep(`Voting for and accepting proposal`)
  await voteForProposal(plu, proposalKey, "DOWN")
  await acceptProposal(csl, proposalKey, "ACCEPTED", "DOWN")

  logStep(`Checking balances on nodes`)
  const symbol = getTokenDescriptorForDao(daoState).symbol
  await checkBalanceForAccount(csl, saltedDaoName, symbol, 500000.00)
  await checkBalanceForAccount(plu, saltedDaoName, symbol, 500000.00)
  await checkBalanceForAccount(ura, saltedDaoName, symbol, 0)

  logStep(`Now try and accept another proposal too quickly - should not be accepted`)
  const naughtyKey = await createPlutusProposal(csl, daoState.daoKey, 50)
  await voteForProposal(plu, naughtyKey, "DOWN")
  await acceptProposalExpectError(csl, naughtyKey, "REJECTED", "DOWN")

  logStep(`sleeping for 30s`)
  await sleep(30000)

  logStep(`Now trying to accept again`)
  await acceptProposal(csl, naughtyKey, "ACCEPTED", "DOWN")

  logStep(`Checking balances on nodes`)
  await checkBalanceForAccount(csl, saltedDaoName, symbol, 567500.00)
  await checkBalanceForAccount(plu, saltedDaoName, symbol, 567500.00)
  await checkBalanceForAccount(ura, saltedDaoName, symbol, 0)

  process.exit(0)
}

try {
  entireTest()
} catch (err) {
  logError(err)
}
