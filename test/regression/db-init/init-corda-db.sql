/**
The following sets up the databases required by each corda network in the docker-compose test cluster
*/
CREATE DATABASE emea;
CREATE DATABASE amer;
CREATE DATABASE csl;
CREATE DATABASE bootstrap;
