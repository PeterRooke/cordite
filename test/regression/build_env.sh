#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# build environment
# usage ./build_env.sh <cordite image tag> <environment>

IMAGE_TAG=${1:-cordite/cordite:local}
ENVIRONMENT_SLUG=${2:-min}

if [ ${ENVIRONMENT_SLUG} = "min" ]
then
    echo "starting a mini cordite network"
    declare -a notaries=("notary")
    declare -a nodes=("emea amer")
    declare -a ports=("9082 9083")
else
    echo "starting a full cordite network"
    declare -a notaries=("notary")
    declare -a nodes=("csl emea amer")
    declare -a ports=("9084 9082 9083")
fi

echo -e "\xE2\x9C\x94 $(date) create environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"
set -e

# clean up any old docker-compose containers and volumes
docker-compose -p ${ENVIRONMENT_SLUG} down --volumes --remove-orphans
docker volume prune -f

# start NMS (and wait for it to be ready)
# docker login network-map
docker-compose -p ${ENVIRONMENT_SLUG} up -d network-map
until docker-compose -p ${ENVIRONMENT_SLUG} logs network-map | grep -q "io.cordite.networkmap.NetworkMapApp - started"
do
    echo -e "waiting for network-map to start"
    sleep 5
done

# start databasess
IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d corda-db
until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs corda-db | grep -q "database system is ready to accept connections"
do
    echo -e "waiting for corda-db to start up and register..."
    sleep 5
done

# start notaries (and wait for them to be ready)
# docker login cordite
for NOTARY in $notaries
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NOTARY}
done
for NOTARY in $notaries
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NOTARY} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NOTARY} to start up and register..."
    sleep 5
  done
done

# Pause Notaries but not before downloading their NodeInfo-*
for NOTARY in $notaries
do
    NODE_ID=$(docker-compose -p ${ENVIRONMENT_SLUG} ps -q ${NOTARY})
    NODEINFO=$(docker exec ${NODE_ID} ls | grep nodeInfo-)
    docker cp ${NODE_ID}:/opt/cordite/${NODEINFO} ${NODEINFO}
    docker exec ${NODE_ID} rm network-parameters
    docker pause ${NODE_ID}
done

# Register notaries with nms:
NMS_JWT=$(curl -X POST "http://localhost:9080/admin/api/login" -H "accept: text/plain" -H "Content-Type: application/json" -d "{ \"user\": \"admin\", \"password\": \"admin\"}")
echo "JWT: ${NMS_JWT}"

for NODEINFO in nodeInfo-*
do
    echo "  registering ${NODEINFO}"
    curl -X POST "http://localhost:9080/admin/api/notaries/nonValidating" -H "accept: text/plain" -H "Content-Type: application/octet-stream" -H "Authorization: Bearer ${NMS_JWT}" --data-binary "@${NODEINFO}"
    rm ${NODEINFO}
    echo -e "\xE2\x9C\x94 copied ${NODEINFO} to ${NMS_ID}"
done

# re-start the notaries
for NOTARY in $notaries
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} restart ${NOTARY}
done


# start regional nodes (and wait for them to be ready)
for NODE in $nodes
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NODE}
done
for NODE in $nodes
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NODE} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NODE} to start up and register..."
    sleep 5
  done
done

# test endpoints
for PORT in $ports
do
    while [[ "$(curl -sSfk -m 5 -o /dev/null -w ''%{http_code}'' https://localhost:${PORT}/api/)" != "200" ]]
    do
    echo -e "waiting for ${PORT} to return 200..."
    sleep 5
    done
done

echo -e "\xE2\x9C\x94 $(date) created environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"