#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# restart environment
# usage ./restart_env.sh <cordite image tag> <environment>

set -e

IMAGE_TAG=${1:-cordite/cordite:local}
ENVIRONMENT_SLUG=${2:-min}

# determine which nodes we need to startup
if [ ${ENVIRONMENT_SLUG} = "min" ]
then
    echo "starting a mini cordite network"
    declare -a notaries=("notary")
    declare -a nodes=("emea amer")
    declare -a ports=("9082 9083")
else
    echo "starting a full cordite network"
    declare -a notaries=("notary")
    declare -a nodes=("apac emea amer")
    declare -a ports=("9081 9082 9083")
fi

echo -e "\xE2\x9C\x94 $(date) restarting environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"


# clean up any old docker-compose
docker-compose -p ${ENVIRONMENT_SLUG} down

# start NMS (and wait for it to be ready)
# docker login network-map
docker-compose -p ${ENVIRONMENT_SLUG} up -d network-map
until docker-compose -p ${ENVIRONMENT_SLUG} logs network-map | grep -q "io.cordite.networkmap.NetworkMapApp - started"
do
    echo -e "waiting for network-map to start"
    sleep 5
done

# start databasess
IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d corda-db
until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs corda-db | grep -q "database system is ready to accept connections"
do
    echo -e "waiting for corda-db to start up and register..."
    sleep 5
done

# start notaries (and wait for them to be ready)
# docker login cordite
for NOTARY in $notaries
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NOTARY}
done
for NOTARY in $notaries
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NOTARY} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NOTARY} to start up and register..."
    sleep 5
  done
done

# we don't need to pause the notaries and formally register them. this should've been done in the previous epoch of the network

# start regional nodes (and wait for them to be ready)
for NODE in $nodes
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NODE}
done
for NODE in $nodes
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NODE} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NODE} to start up and register..."
    sleep 5
  done
done

# test endpoints
for PORT in $ports
do
    while [[ "$(curl -sSfk -m 5 -o /dev/null -w ''%{http_code}'' https://localhost:${PORT}/api/)" != "200" ]]
    do
    echo -e "waiting for ${PORT} to return 200..."
    sleep 5
    done
done

echo -e "\xE2\x9C\x94 $(date) environment ${ENVIRONMENT_SLUG} restarted with image tag ${IMAGE_TAG}"