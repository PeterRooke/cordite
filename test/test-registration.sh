#!/usr/bin/env bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
# Tests Corda docker by registering with a test doorman
# usage: ./test-docker.sh <IMAGE UNDER TEST> <network>
# example: ./test-docker.sh cordite/cordite:edge local
IMAGE=${1:-cordite/cordite:local}
NETWORK=${2:-local}
SALT=${RANDOM}

if [[ "${NETWORK}" == "local" ]] ; then
    # Start up test-doorman, if not already running
    if [ ! "$(docker ps -q -f name=test-doorman)" ]; then
        if [ "$(docker ps -aq -f status=exited -f name=test-doorman)" ]; then
            echo "TEST-IMAGE-${IMAGE}: test-doorman is in a status=exited state. I will remove."
            docker rm -f test-doorman
        fi
        echo "TEST-IMAGE-${IMAGE}: test-doorman is not running. I will start."
        docker run -d --rm --name test-doorman -p 8080:8080 \
        -e NMS_MONGO_CONNECTION_STRING=embed \
        -e NMS_TLS=false \
        -e NMS_DOORMAN=true \
        -e NMS_CERTMAN=false \
        cordite/network-map
    else
        echo "TEST-IMAGE-${IMAGE}: test-door man is already running. I will use this instance."
    fi

    # Wait for test-doorman and then download truststore
    while [[ "$(curl -s -o test-doorman-network-root-truststore.jks -w ''%{http_code}'' http://localhost:8080/network-map/truststore)" != "200" ]]; do
        echo "TEST-IMAGE-${IMAGE}: waiting 5 seconds for test-doorman to serve..."
        sleep 5
    done

    # Test initial-registration against local doorman
    echo "TEST-IMAGE-${IMAGE}: Run inital-registration in corda docker with image: ${IMAGE} on ${NETWORK}"
    docker run -d --name cordite-test-${SALT} --network=host --hostname=127.0.0.1 \
            -e MY_LEGAL_NAME="O=Test-${SALT},L=Berlin,C=DE"     \
            -e MY_PUBLIC_ADDRESS="localhost"       \
            -e NETWORK_MAP_URL="http://localhost:8080"    \
            -e DOORMAN_URL="http://localhost:8080"      \
            -e TRUST_STORE_NAME="network-root-truststore.jks"       \
            -e NETWORK_TRUST_PASSWORD="trustpass"       \
            -e MY_EMAIL_ADDRESS="norepy@cordite.foundation"      \
            -v $(pwd)/test-doorman-network-root-truststore.jks:/opt/corda/certificates/network-root-truststore.jks \
            -e CORDITE_BRAID_PORT="8081" \
            $IMAGE
    echo "DONE@!!!"
else
    # Test initial-registration against remote doorman
    echo "TEST-IMAGE-${IMAGE}: Run inital-registration in corda docker with image: ${IMAGE} on ${NETWORK}"
    docker run -d --name cordite-test-${SALT} \
            -e MY_LEGAL_NAME="O=Test-${SALT},L=Berlin,C=DE"     \
            -e MY_PUBLIC_ADDRESS="localhost"       \
            -e MY_EMAIL_ADDRESS="norepy@cordite.foundation"      \
            -e MY_NETWORK_MAP="${NETWORK}" \
            $IMAGE
fi

# Succesfully registered (with http://localhost:8080)
echo "------"
docker logs cordite-test-${SALT} | grep -q "Succesfully registered"
echo "------"
if [ ! "$(docker ps -q -f name=cordite-test-${SALT})" ]; then
    echo "TEST-IMAGE-${IMAGE}: FAIL cordite-test has exited."
    docker logs cordite-test-${SALT}
    rm -f $(pwd)/test-doorman-network-root-truststore.jks
    docker rm -f cordite-test-${SALT}
    exit 1
else
    echo "TEST-IMAGE-${IMAGE}: SUCCESS : Succesfully registered with http://localhost:8080"
fi

# Node started up and registered
# docker logs -f cordite-test-${SALT} | grep -q "started up and registered in"
# if [ ! "$(docker ps -q -f name=cordite-test-${SALT})" ]; then
#     echo "TEST-IMAGE-${IMAGE}: FAIL cordite-test has exited."
#     docker logs cordite-test-${SALT}
#     rm -f $(pwd)/test-doorman-network-root-truststore.jks
#     docker rm -f cordite-test-${SALT}
#     exit 1
# else
#     echo "TEST-IMAGE-${IMAGE}:  SUCCESS : Node started up and registered"
#     echo "TEST-IMAGE-${IMAGE}:  SUCCESS : tear down"
#     rm -f $(pwd)/test-doorman-network-root-truststore.jks
#     docker rm -f cordite-test-${SALT}
#     exit 0
# fi
