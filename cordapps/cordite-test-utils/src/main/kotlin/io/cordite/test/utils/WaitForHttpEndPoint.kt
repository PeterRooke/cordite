/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.test.utils

import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import net.corda.core.utilities.loggerFor
import java.time.Duration

class WaitForHttpEndPoint(
  private val vertx: Vertx,
  private val host: String = "localhost",
  private val port: Int = 8080,
  private val path: String = "/",
  private val tls: Boolean = true,
  private val trustAll: Boolean = true,
  private val verifyHost: Boolean = false,
  private val maxAttempts: Int = 10,
  private val sleepBeforeRetry: Duration = Duration.ofSeconds(1)) {
  companion object {
    val log = loggerFor<WaitForHttpEndPoint>()

    fun waitForHttpEndPoint(vertx: Vertx,
                            host: String = "localhost",
                            port: Int = 8080,
                            path: String = "/",
                            tls: Boolean = true,
                            trustAll: Boolean = true,
                            verifyHost: Boolean = false,
                            sleepBeforeRetry: Duration = Duration.ofSeconds(3),
                            maxAttempts: Int = 10,
                            handler: Handler<AsyncResult<Unit>>) {
        WaitForHttpEndPoint(vertx, host, port, path, tls, trustAll, verifyHost, maxAttempts, sleepBeforeRetry)
          .execute(handler)
    }
  }

  fun execute(handler: Handler<AsyncResult<Unit>>) {
    val webClientOptions = WebClientOptions()
      .setDefaultHost(host)
      .setDefaultPort(port)
      .setTrustAll(trustAll)
      .setVerifyHost(verifyHost)
      .setSsl(tls)
    val client = WebClient.create(vertx, webClientOptions)
    client.retry(path = path, maxAttempts = maxAttempts, sleep = sleepBeforeRetry.toMillis()) {
      client.close()
      handler.handle(it)
    }
  }

  private fun WebClient.retry(path: String, maxAttempts: Int = 100, currentAttempt: Int = 1, sleep: Long = 1_000, handler: (AsyncResult<Unit>) -> Unit) {
    log.info("checking for end-point $host:$port$path'")
    this.get(path).send {
      if (it.succeeded()) {
        log.info("end point found $host:$port$path")
        handler(Future.succeededFuture(Unit))
      } else {
        if (currentAttempt >= maxAttempts) {
          handler(Future.failedFuture("failed to reach end point '$host:$port$path' after $maxAttempts attempts"))
        } else {
          log.info("connection failed. retrying in $sleep milliseconds")
          vertx.setTimer(sleep) {
            this.retry(path, maxAttempts, currentAttempt + 1, sleep, handler)
          }
        }
      }
    }
  }

}