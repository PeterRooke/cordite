/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
@file:Suppress("DEPRECATION")

package io.cordite.dgl.api.impl

import io.cordite.braid.core.http.*
import io.cordite.commons.distribution.impl.DataDistributionGroupService
import io.cordite.dgl.api.*
import io.cordite.dgl.contract.v1.account.AccountContract
import io.cordite.dgl.contract.v1.account.AccountState
import io.cordite.dgl.contract.v1.tag.Tag
import io.cordite.test.utils.BraidPortHelper
import io.cordite.test.utils.execute
import io.cordite.test.utils.h2.H2Server
import io.vertx.core.Vertx
import io.vertx.core.http.HttpClient
import io.vertx.core.http.HttpClientOptions
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.core.identity.CordaX500Name
import net.corda.core.internal.packageName
import net.corda.core.utilities.contextLogger
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkParameters
import net.corda.testing.node.StartedMockNode
import net.corda.testing.node.internal.cordappForClasses
import net.corda.testing.node.internal.findCordapp
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(VertxUnitRunner::class)
class LedgerRestBindingsTest {
  companion object {
    private val log = contextLogger()
    private lateinit var network: MockNetwork
    private lateinit var h2Server: H2Server
    private val braidPortHelper = BraidPortHelper()
    private val nodeNameA = CordaX500Name("nodeA", "London", "GB")
    private val nodeNameB = CordaX500Name("nodeB", "London", "GB")
    private lateinit var nodeA: StartedMockNode
    private lateinit var nodeB: StartedMockNode
    private lateinit var testNodeA: TestNode
    private lateinit var testNodeB: TestNode
    private val intFountain = IntFountain()
    private lateinit var defaultNotaryName: CordaX500Name
    private lateinit var nodeAClient: HttpClient
    private val clientVertx = Vertx.vertx()

    @JvmStatic
    @BeforeClass
    fun beforeClass() {
      braidPortHelper.setSystemPropertiesFor(nodeNameA, nodeNameB)
      val cordapps = setOf(
        findCordapp(LedgerApi::class.packageName),
        findCordapp(AccountContract::class.packageName),
        findCordapp(DataDistributionGroupService::class.packageName),
        cordappForClasses(LedgerTestBraidServer::class.java)
      )

      network = MockNetwork(MockNetworkParameters(
        cordappsForAllNodes = cordapps,
        networkParameters = testNetworkParameters(minimumPlatformVersion = 4)
      ))

      nodeA = network.createPartyNode(nodeNameA)
      nodeB = network.createPartyNode(nodeNameB)
      network.runNetwork()
      testNodeA = TestNode(nodeA, braidPortHelper)
      testNodeB = TestNode(nodeB, braidPortHelper)
      h2Server = H2Server(network, listOf(nodeA, nodeB))
      defaultNotaryName = network.defaultNotaryIdentity.name
      nodeAClient = clientVertx.createHttpClient(HttpClientOptions()
        .setDefaultPort(braidPortHelper.portForNode(testNodeA.node))
        .setSsl(true)
        .setTrustAll(true)
        .setVerifyHost(false)
      )
    }

    @JvmStatic
    @AfterClass
    fun afterClass() {
      nodeAClient.close()
      clientVertx.close()
      testNodeA.shutdown()
      testNodeB.shutdown()
      h2Server.stop()
      network.stopNodes()
    }
  }

  @Test
  fun `that we can create an account and its tags and alter the tag`(context: TestContext) {
    val accountId = "account-${intFountain.next()}"
    val tagCategory = "category"
    val tagValue = "value"
    val tags = listOf(Tag(tagCategory, tagValue))
    val accounts = createAccount(CreateAccountsPayload(notary = defaultNotaryName.toString(), requests = listOf(CreateAccountRequest(accountId, tags))), context)
    context.assertEquals(1, accounts.size, "should have created an account")
    context.assertEquals(accountId, accounts.first().address.accountId, "account name should match")
    context.assertEquals(1, accounts.first().tags.count { it.category == tagCategory && it.value == tagValue }, "must contain the tag")
    val newValue = "new value"
    val updatedAccount = setTag(accountId, tagCategory, newValue, context)
    context.assertEquals(newValue, updatedAccount.tags.first { it.category == tagCategory}.value, "tag must exist with the right value")
  }

  private fun getAccounts(pageSize: Int, page: Int, context: TestContext): List<AccountState> {
    return execute(network) {
      nodeAClient.futureGet(path = "/rest/ledger/accounts", queryParams = mapOf("page-size" to pageSize.toString(), "page-number" to page.toString()))
        .flatMap { response ->
          context.assertEquals(200, response.statusCode(), response.statusMessage())
          response.bodyAsJsonList<AccountState>()
        }
    }
  }

  private fun createAccount(payload: CreateAccountsPayload, context: TestContext): Set<AccountState> {
    return execute(network) {
      nodeAClient.futurePost(path = "/rest/ledger/accounts", body = payload)
        .flatMap { response ->
          context.assertEquals(200, response.statusCode(), response.statusMessage())
          response.bodyAsJsonSet<AccountState>()
        }
    }
  }

  private fun setTag(accountId: String, category: String, value: String, context: TestContext): AccountState {
    return execute(network) {
      nodeAClient.futurePut(
        path = "/rest/ledger/accounts/$accountId/tags/$category",
        queryParams = mapOf("value" to value, "notary" to defaultNotaryName.toString()))
        .flatMap { response ->
          context.assertEquals(200, response.statusCode(), response.statusMessage())
          response.bodyAsJsonObject<AccountState>()
        }
    }
  }
}

