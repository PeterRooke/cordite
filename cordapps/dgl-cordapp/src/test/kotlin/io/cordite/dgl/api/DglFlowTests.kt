/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api

import io.cordite.commons.distribution.impl.DataDistributionGroupService
import io.cordite.dgl.api.flows.account.CreateAccountFlow
import io.cordite.dgl.contract.v1.account.AccountContract
import io.cordite.dgl.contract.v1.tag.Tag
import io.cordite.dgl.contract.v1.tag.WellKnownTagCategories
import io.cordite.dgl.contract.v1.tag.WellKnownTagValues
import io.cordite.dgl.contract.v1.token.TokenTransactionSummary
import io.cordite.dgl.contract.v1.token.TokenTypeState
import io.cordite.dgl.contract.v1.token.toBigDecimal
import io.cordite.dgl.contract.v1.token.toBigDecimalAmount
import io.cordite.test.utils.BraidPortHelper
import io.cordite.test.utils.execute
import io.cordite.test.utils.h2.H2Server
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.internal.packageName
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.toFuture
import net.corda.core.utilities.getOrThrow
import net.corda.core.utilities.loggerFor
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkParameters
import net.corda.testing.node.StartedMockNode
import net.corda.testing.node.internal.cordappForClasses
import net.corda.testing.node.internal.findCordapp
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.floor
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue


@RunWith(VertxUnitRunner::class)
class DglFlowTests {
  companion object {
    private val log = loggerFor<DglFlowTests>()
    private lateinit var network: MockNetwork
    private lateinit var ledgerNodeA: LedgerApi
    private lateinit var ledgerNodeB: LedgerApi
    private lateinit var ledgerNodeC: LedgerApi
    private val nodeNameA = CordaX500Name("nodeA", "London", "GB")
    private val nodeNameB = CordaX500Name("nodeB", "London", "GB")
    private val nodeNameC = CordaX500Name("nodeC", "New York", "US")
    private lateinit var h2Server: H2Server
    private val braidPortHelper = BraidPortHelper()
    private lateinit var testNodeA: TestNode
    private lateinit var testNodeB: TestNode
    private lateinit var testNodeC: TestNode
    private val intFountain = IntFountain()
    private lateinit var defaultNotaryName: CordaX500Name
    private lateinit var nodeA: StartedMockNode
    private lateinit var nodeB: StartedMockNode
    private lateinit var nodeC: StartedMockNode

    @JvmStatic
    @BeforeClass
    fun beforeClass() {
      braidPortHelper.setSystemPropertiesFor(nodeNameA, nodeNameB, nodeNameC)
      val cordapps = setOf(
        findCordapp(LedgerApi::class.packageName),
        findCordapp(AccountContract::class.packageName),
        findCordapp(DataDistributionGroupService::class.packageName),
        cordappForClasses(LedgerTestBraidServer::class.java)
      )

      network = MockNetwork(MockNetworkParameters(cordappsForAllNodes = cordapps, networkParameters = testNetworkParameters(minimumPlatformVersion = 4)))

      nodeA = network.createPartyNode(nodeNameA)
      nodeB = network.createPartyNode(nodeNameB)
      nodeC = network.createPartyNode(nodeNameC)
      network.runNetwork()
      testNodeA = TestNode(nodeA, braidPortHelper)
      testNodeB = TestNode(nodeB, braidPortHelper)
      testNodeC = TestNode(nodeC, braidPortHelper)
      ledgerNodeA = testNodeA.ledgerService
      ledgerNodeB = testNodeB.ledgerService
      ledgerNodeC = testNodeC.ledgerService
      h2Server = H2Server(network, listOf(nodeA, nodeB, nodeC))
      defaultNotaryName = network.defaultNotaryIdentity.name
    }

    @JvmStatic
    @AfterClass
    fun afterClass() {
      testNodeA.shutdown()
      testNodeB.shutdown()
      testNodeC.shutdown()
      h2Server.stop()
      network.stopNodes()
    }
  }

  private val xkcdSymbol = "XKCD${intFountain.next()}"
  private val corditeSymbl = "XCD${intFountain.next()}"
  private val accountId1 = "account-${intFountain.next()}"
  private val accountId2 = "account-${intFountain.next()}"
  private val accountId3 = "account-${intFountain.next()}"
  private val accountId4 = "account-${intFountain.next()}"
  private val swiftCodeTag = Tag(WellKnownTagCategories.SWIFT_CODE, "AACCGB21" + intFountain.next())

  private val cashAccountLabel = "CASH_ACCOUNT"
  private val britishAccounts = "BRIT_ACCOUNTS"
  private val europeanAccounts = "EURO_ACCOUNTS"
  private val allAccounts = "ALL_ACCOUNTS"

  private val allAccountsTag = Tag(cashAccountLabel, allAccounts)
  private val britishAccountsTag = Tag(cashAccountLabel, britishAccounts)
  private val europeanAccountsTag = Tag(cashAccountLabel, europeanAccounts)

  @Test
  fun `should fail to create token type with exponent outside of the allowed range 0 to 20`() {
    assertThatExceptionOfType(RuntimeException::class.java).isThrownBy {
      network.execute<TokenTypeState> {
        ledgerNodeA.createTokenType(xkcdSymbol, TokenTypeState.AMOUNT_MAX_EXPONENT + 1, defaultNotaryName)
      }
    }.withMessageContaining("exponent must be within the range (0..${TokenTypeState.AMOUNT_MAX_EXPONENT})")

    assertThatExceptionOfType(RuntimeException::class.java).isThrownBy {
      network.execute<TokenTypeState> { ledgerNodeA.createTokenType(xkcdSymbol, -2, defaultNotaryName) }
    }.withMessageContaining("exponent must be within the range (0..${TokenTypeState.AMOUNT_MAX_EXPONENT})")
  }

  @Test
  fun `create account and check we can find it using its tag`() {
    network.execute { ledgerNodeA.createAccount(accountId1, defaultNotaryName) }
    val accountV2 = execute(network) { ledgerNodeA.setAccountTag(accountId1, swiftCodeTag, defaultNotaryName) }
    assertEquals(accountV2, execute(network) { ledgerNodeA.findAccountsByTag(swiftCodeTag) }.first())
    network.execute { ledgerNodeA.removeAccountTag(accountId1, swiftCodeTag.category, defaultNotaryName) }
    assertEquals(0, execute(network) { ledgerNodeA.findAccountsByTag(swiftCodeTag) }.count())
  }

  @Test
  fun `create token and check that it exists`() {
    val token = execute(network) { ledgerNodeA.createTokenType(xkcdSymbol, 2, defaultNotaryName) }
    val tokens = execute(network) { ledgerNodeA.listTokenTypes() }
    assertTrue(tokens.contains(token))
  }

  @Test
  fun `issue tokens`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    val corditeTokenType = ledgerNodeA.createTokenType(corditeSymbl)
    ledgerNodeA.createAccounts()

    val amount = BigDecimal("100.00")
    val fountain = intFountain::next

    ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}")

    // issue a bunch more tokens
    ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}")
    ledgerNodeA.issueToken(accountId1, amount, corditeSymbl, "issuance ${fountain()}")
    ledgerNodeA.issueToken(accountId1, amount, corditeSymbl, "issuance ${fountain()}")
    ledgerNodeA.issueToken(accountId2, amount, xkcdSymbol, "issuance ${fountain()}")
    ledgerNodeA.issueToken(accountId2, amount, xkcdSymbol, "issuance ${fountain()}")
    ledgerNodeA.issueToken(accountId2, amount, corditeSymbl, "issuance ${fountain()}")
    ledgerNodeA.issueToken(accountId2, amount, corditeSymbl, "issuance ${fountain()}")
    ledgerNodeA.issueToken(accountId2, amount, corditeSymbl, "issuance ${fountain()}")

    val balances1 = execute(network) { ledgerNodeA.balanceForAccount(accountId1) }.map { it.amountType to it.quantity }.toMap()
    val balances2 = execute(network) { ledgerNodeA.balanceForAccount(accountId2) }.map { it.amountType to it.quantity }.toMap()

    assertEquals(BigDecimal("200.00"), balances1.getValue(corditeTokenType.descriptor))
    assertEquals(BigDecimal("200.00"), balances1.getValue(xkcdTokenType.descriptor))
    assertEquals(BigDecimal("300.00"), balances2.getValue(corditeTokenType.descriptor))
    assertEquals(BigDecimal("200.00"), balances2.getValue(xkcdTokenType.descriptor))
  }

  @Test
  fun `bulk issue tokens`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    val corditeTokenType = ledgerNodeA.createTokenType(corditeSymbl)
    ledgerNodeA.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next

    execute(network) { ledgerNodeA.bulkIssueTokens(listOf(accountId1), amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    execute(network) { ledgerNodeA.bulkIssueTokens(listOf(accountId1, accountId2), amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    execute(network) { ledgerNodeA.bulkIssueTokens(listOf(accountId1, accountId2), amount, corditeSymbl, "issuance ${fountain()}", defaultNotaryName) }

    val balances1 = execute(network) { ledgerNodeA.balanceForAccount(accountId1) }.map { it.amountType to it.quantity }.toMap()
    val balances2 = execute(network) { ledgerNodeA.balanceForAccount(accountId2) }.map { it.amountType to it.quantity }.toMap()

    assertEquals(BigDecimal("100.00"), balances1.getValue(corditeTokenType.descriptor))
    assertEquals(BigDecimal("200.00"), balances1.getValue(xkcdTokenType.descriptor))
    assertEquals(BigDecimal("100.00"), balances2.getValue(corditeTokenType.descriptor))
    assertEquals(BigDecimal("100.00"), balances2.getValue(xkcdTokenType.descriptor))
  }

  @Test
  fun `calling bulk issue tokens with no accounts causes exception`() {
    ledgerNodeA.createTokenType(xkcdSymbol)
    assertThatExceptionOfType(RuntimeException::class.java).isThrownBy {
      network.execute<SecureHash> { ledgerNodeA.bulkIssueTokens(emptyList(), "100.00", xkcdSymbol, "issuance ${intFountain::next}", defaultNotaryName) }
    }
  }

  @Test
  fun `issuing tokens with account URI parses URI into account id and issues to account`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next

    assertNotNull(execute(network) {
      ledgerNodeA.issueToken("$accountId1@O=nodeA,L=London,C=GB", amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName)
    })
    val balances = execute(network) { ledgerNodeA.balanceForAccount(accountId1) }.map { it.amountType to it.quantity }.toMap()
    assertEquals(BigDecimal("100.00"), balances.getValue(xkcdTokenType.descriptor))
  }

  @Test
  fun `issuing tokens with account URI not owned by node throws exception`() {
    ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next

    assertThatExceptionOfType(RuntimeException::class.java).isThrownBy {
      ledgerNodeA.issueToken(accountId1 at testNodeB, amount.toBigDecimal(), xkcdSymbol, "issuance ${fountain()}")
    }
  }

  @Test
  fun `issuing tokens with account id issues to specified account`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next

    assertNotNull(execute(network) {
      ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName)
    })
    val balances = execute(network) { ledgerNodeA.balanceForAccount(accountId1) }.map { it.amountType to it.quantity }.toMap()
    assertEquals(BigDecimal("100.00"), balances.getValue(xkcdTokenType.descriptor))
  }

  // This used to fail in Corda 3.x as documented here https://gitlab.com/cordite/cordite/issues/194
  @Test
  fun `that we can get well known tag categories`() {
    val categories = ledgerNodeA.wellKnownTagCategories()
    val values = ledgerNodeA.wellKnownTagValues()

    val categoryCount = WellKnownTagCategories::class.java.fields.filter { it.type == String::class.java }.count()
    val valueCount = WellKnownTagValues::class.java.fields.filter { it.type == String::class.java }.count()
    assertEquals(categoryCount, categories.count())
    assertEquals(valueCount, values.count())
  }

  @Test
  fun `issue tokens and transfer between internal accounts`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      ledgerNodeA.transferAccountToAccount(
        "5.00",
        xkcdTokenType.descriptor.uri,
        accountId1 at testNodeA,
        accountId2 at testNodeA,
        "transfer", defaultNotaryName)
    }

    assertEquals(BigDecimal("95.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("5.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      ledgerNodeA.transferAccountToAccount(
        "5.00",
        xkcdTokenType.descriptor.uri,
        accountId1 at testNodeA,
        accountId2 at testNodeA,
        "transfer", defaultNotaryName)
    }

    assertEquals(BigDecimal("90.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("10.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))
  }

  @Test
  fun `issue tokens and transfer between accounts on distinct parties`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      ledgerNodeA.transferAccountToAccount(
        "5.00",
        xkcdTokenType.descriptor.uri,
        accountId1 at testNodeA,
        accountId2 at testNodeB,
        "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("95.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("5.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      ledgerNodeA.transferAccountToAccount(
        "5.00",
        xkcdTokenType.descriptor.uri,
        accountId1 at testNodeA,
        accountId2 at testNodeB,
        "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("90.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("10.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))
    val result = ledgerNodeA.transactionsForAccount(accountId1, PageSpecification())
    assertEquals(3, result.size)
  }

  @Test
  fun `issue tokens and multi transfer between accounts on distinct parties one to many`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      ledgerNodeA.transferAccountToAccount(
        "25.00",
        xkcdTokenType.descriptor.uri,
        accountId1 at testNodeA,
        accountId2 at testNodeA,
        "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("75.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("25.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeB.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      val toAccounts = mapOf(
        "10.00" to (accountId1 at testNodeB),
        "30.00" to (accountId2 at testNodeB)
      )
      ledgerNodeA.transferAccountsToAccounts(
        xkcdTokenType.descriptor.uri,
        mapOf(
          "40.00" to (accountId1 at testNodeA)
        ),
        toAccounts,
        "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("35.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("25.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))
    assertEquals(BigDecimal("10.00"), ledgerNodeB.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("30.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))
    val result = ledgerNodeA.transactionsForAccount(accountId1, PageSpecification())
    assertEquals(3, result.size)
  }

  @Test
  fun `issue tokens and multi transfer between accounts on distinct parties many to one`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      ledgerNodeA.transferAccountToAccount(
        "25.00",
        xkcdTokenType.descriptor.uri,
        accountId1 at testNodeA,
        accountId2 at testNodeA,
        "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("75.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("25.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeB.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      val from = mapOf(
        "25.01" to (accountId1 at testNodeA),
        "20.01" to (accountId2 at testNodeA)
      )
      val to = mapOf(
        "45.02" to (accountId1 at testNodeB)
      )
      ledgerNodeA.transferAccountsToAccounts(xkcdTokenType.descriptor.uri, from, to, "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("49.99"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("4.99"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))
    assertEquals(BigDecimal("45.02"), ledgerNodeB.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))
    val result = ledgerNodeA.transactionsForAccount(accountId1, PageSpecification())
    assertEquals(3, result.size)
  }

  @Test
  fun `issue tokens and multi transfer between accounts on distinct parties many to many`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      ledgerNodeA.transferAccountToAccount(
        "25.00",
        xkcdTokenType.descriptor.uri,
        accountId1 at testNodeA,
        accountId2 at testNodeA,
        "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("75.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("25.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeB.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      val from = mapOf(
        "25.00" to (accountId1 at testNodeA),
        "20.00" to (accountId2 at testNodeA)
      )
      val to = mapOf(
        "10.00" to (accountId1 at testNodeB),
        "35.00" to (accountId2 at testNodeB)
      )
      ledgerNodeA.transferAccountsToAccounts(
        xkcdTokenType.descriptor.uri, from, to, "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("50.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("5.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))
    assertEquals(BigDecimal("10.00"), ledgerNodeB.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("35.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))
    val result = ledgerNodeA.transactionsForAccount(accountId1, PageSpecification())
    assertEquals(3, result.size)
  }

  @Test
  fun `issue tokens and multi transfer between accounts on distinct parties many to many balances just enough`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      ledgerNodeA.transferAccountToAccount(
        "25.00",
        xkcdTokenType.descriptor.uri,
        accountId1 at testNodeA,
        accountId2 at testNodeA,
        "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("75.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("25.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeB.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      val from = mapOf(
        "75.00" to (accountId1 at testNodeA),
        "25.00" to (accountId2 at testNodeA)
      )
      val to = mapOf(
        "60.00" to (accountId1 at testNodeB),
        "40.00" to (accountId2 at testNodeB)
      )
      ledgerNodeA.transferAccountsToAccounts(
        xkcdTokenType.descriptor.uri, from, to, "transfer ${fountain()}", defaultNotaryName)
    }

    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))
    assertEquals(BigDecimal("60.00"), ledgerNodeB.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("40.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))
    val result = ledgerNodeA.transactionsForAccount(accountId1, PageSpecification())
    assertEquals(3, result.size)
  }

  @Test
  fun `transaction id set on TokenTransactionSummary State when retrieving transactions for an account`() {
    ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val fountain = intFountain::next
    val hash = execute(network) {
      ledgerNodeA.issueToken(
        accountId1,
        "100.00",
        xkcdSymbol,
        "issuance ${fountain()}",
        defaultNotaryName
      )
    }
    val result = ledgerNodeA.transactionsForAccount(accountId1, PageSpecification())
    assertEquals(1, result.size)
    assertEquals(hash, result.first().transactionId)
  }

  @Test(expected = RuntimeException::class)
  fun `issue tokens and attempt to multi transfer between accounts to multiple recipients`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()
    ledgerNodeC.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      val from = mapOf(
        "75.00" to (accountId1 at testNodeA),
        "25.00" to (accountId2 at testNodeA)
      )
      val to = mapOf(
        "60.00" to (accountId1 at testNodeB),
        "40.00" to (accountId2 at testNodeB)
      )
      ledgerNodeA.transferAccountsToAccounts(
        xkcdTokenType.descriptor.uri, from, to, "transfer ${fountain()}", defaultNotaryName)
    }
  }

  @Test(expected = RuntimeException::class)
  fun `issue tokens and attempt to multi transfer when tokens are insufficient`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()
    ledgerNodeC.createAccounts()

    val amount = "10.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      val from = mapOf(
        "75.00" to (accountId1 at testNodeA),
        "25.00" to (accountId2 at testNodeA)
      )
      val to = mapOf(
        "60.00" to (accountId1 at testNodeB),
        "40.00" to (accountId2 at testNodeB)
      )
      ledgerNodeA.transferAccountsToAccounts(
        xkcdTokenType.descriptor.uri, from, to, "transfer ${fountain()}", defaultNotaryName)
    }
  }


  @Test(expected = RuntimeException::class)
  fun `issue tokens and attempt to multi transfer with negative amounts`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      val from = mapOf(
        "75.00" to (accountId1 at testNodeA),
        "25.00" to (accountId2 at testNodeA)
      )
      val to = mapOf(
        "110.00" to (accountId1 at testNodeB),
        "-10.00" to (accountId2 at testNodeB)
      )
      ledgerNodeA.transferAccountsToAccounts(
        xkcdTokenType.descriptor.uri, from, to, "transfer ${fountain()}", defaultNotaryName)
    }
  }

  @Test(expected = RuntimeException::class)
  fun `issue tokens and attempt to multi transfer with zero amount`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    execute(network) {
      val from = mapOf(
        "75.00" to (accountId1 at testNodeA),
        "25.00" to (accountId2 at testNodeA)
      )
      val to = mapOf(
        "100.00" to (accountId1 at testNodeB),
        "0.00" to (accountId1 at testNodeB)
      )
      ledgerNodeA.transferAccountsToAccounts(
        xkcdTokenType.descriptor.uri, from, to, "transfer ${fountain()}", defaultNotaryName)
    }
  }

  @Test
  fun `that we can query the balances of an account without having created tokens`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
  }

  @Test
  fun `create accounts and get aggregated balance using its tags`() {
    execute(network) { ledgerNodeA.createAccount(accountId1, defaultNotaryName) }
    execute(network) { ledgerNodeA.createAccount(accountId2, defaultNotaryName) }
    execute(network) { ledgerNodeA.createAccount(accountId3, defaultNotaryName) }
    execute(network) { ledgerNodeA.createAccount(accountId4, defaultNotaryName) }

    execute(network) { ledgerNodeA.setAccountTag(accountId1, allAccountsTag, defaultNotaryName) }
    execute(network) { ledgerNodeA.setAccountTag(accountId2, allAccountsTag, defaultNotaryName) }
    execute(network) { ledgerNodeA.setAccountTag(accountId3, allAccountsTag, defaultNotaryName) }
    execute(network) { ledgerNodeA.setAccountTag(accountId4, allAccountsTag, defaultNotaryName) }

    execute(network) { ledgerNodeA.setAccountTag(accountId1, britishAccountsTag, defaultNotaryName) }
    execute(network) { ledgerNodeA.setAccountTag(accountId2, britishAccountsTag, defaultNotaryName) }
    execute(network) { ledgerNodeA.setAccountTag(accountId3, europeanAccountsTag, defaultNotaryName) }
    execute(network) { ledgerNodeA.setAccountTag(accountId4, europeanAccountsTag, defaultNotaryName) }

    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)

    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, "25.00", xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    execute(network) { ledgerNodeA.issueToken(accountId2, "50.00", xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    execute(network) { ledgerNodeA.issueToken(accountId3, "75.00", xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    execute(network) { ledgerNodeA.issueToken(accountId3, "100.00", xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }

//    h2Server.block()
    assertEquals(BigDecimal("250.00"), ledgerNodeA.balanceForAccountTag(allAccountsTag, xkcdTokenType))
    assertEquals(BigDecimal("75.00"), ledgerNodeA.balanceForAccountTag(britishAccountsTag, xkcdTokenType))
    assertEquals(BigDecimal("175.00"), ledgerNodeA.balanceForAccountTag(europeanAccountsTag, xkcdTokenType))
  }

  @Test
  fun `collect coins when sufficient balance available`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val smallChange = 1
    val fountain = intFountain::next
    val coins = 100
    repeat(coins) {
      ledgerNodeA.issueToken(accountId1, smallChange.toBigDecimal(), xkcdSymbol, "issuance ${fountain()}")
    }
    val balance1 = ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType)
    assertEquals(0, smallChange.toBigDecimal().multiply(coins).compareTo(balance1))

    val transfer = smallChange.toBigDecimal().multiply(coins - 1).minus(smallChange.toBigDecimal().divide(2))
    val expectedRemainder = balance1.minus(transfer)

    execute(network) {
      ledgerNodeA.transferAccountToAccount(transfer.toString(), xkcdTokenType.descriptor.uri, accountId1, accountId2, "transfer ${fountain()}", defaultNotaryName)
    }
    val balance2 = ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType)
    assertTrue(expectedRemainder.compareTo(balance2) == 0)

    val balance3 = ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType)
    assertTrue(transfer.compareTo(balance3) == 0)
  }

  @Test
  fun `collect coins when insufficient balance available should fail but still allow future requests that are within the balance`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val smallChange = 1
    val fountain = intFountain::next
    val coins = 100
    repeat(coins) {
      ledgerNodeA.issueToken(accountId1, smallChange.toBigDecimal(), xkcdSymbol, "issuance ${fountain()}")
    }
    val balance1 = ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType)
    assertTrue(smallChange.toBigDecimal().multiply(coins).compareTo(balance1) == 0)

    val transfer1 = smallChange.toBigDecimal().multiply(coins).plus(smallChange.toBigDecimal().divide(2))

    try {
      execute(network) {
        ledgerNodeA.transferAccountToAccount(transfer1.toString(), xkcdTokenType.descriptor.uri, accountId1, accountId2, "transfer ${fountain()}", defaultNotaryName)
      }
      throw IllegalStateException("transfer should have raised an exception because of insufficient funds")
    } catch (err: IllegalStateException) {
      throw err
    } catch (err: Throwable) {
      println("expected exception caught")
      println(err)
    }

    val transfer2 = smallChange.toBigDecimal().multiply(coins - 1).minus(smallChange.toBigDecimal().divide(2))
    val expectedRemainder = balance1.minus(transfer2)

    execute(network) {
      ledgerNodeA.transferAccountToAccount(transfer2.toString(), xkcdTokenType.descriptor.uri, accountId1, accountId2, "transfer ${fountain()}", defaultNotaryName)
    }
    val balance2 = ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType)
    assertTrue(expectedRemainder.compareTo(balance2) == 0)

    val balance3 = ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType)
    assertTrue(transfer2.compareTo(balance3) == 0)
  }

  @Test(expected = CreateAccountFlow.Exception::class)
  fun `that calling CreateAccountFlow with zero requests raises an appropriate exception`() {
    val future = nodeA.startFlow(CreateAccountFlow(listOf(), network.defaultNotaryIdentity))
    network.runNetwork()
    future.getOrThrow()
  }

  @Test
  fun `that transferring to an unknown remote account fails`() {
    try {
      val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
      ledgerNodeA.createAccounts()
      val amount = "100.00"
      val fountain = intFountain::next
      execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
      execute(network) {
        ledgerNodeA.transferAccountToAccount(
          "5.00",
          xkcdTokenType.descriptor.uri,
          accountId1 at testNodeA,
          "unknown-account" at testNodeB,
          "transfer", defaultNotaryName)
      }
      throw Exception("transfer should have failed")
    } catch (ex: RuntimeException) {
      // all good
    }
  }

  @Test
  fun `that we can get the balance for an empty account`() {
    ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val result = execute(network) { ledgerNodeA.balanceForAccount(accountId1) }
    assertTrue(result.isEmpty())
  }

  @Test
  fun `that transferring to an unknown local account fails`() {
    try {
      val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
      ledgerNodeA.createAccounts()
      val amount = "100.00"
      val fountain = intFountain::next
      execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
      execute(network) {
        ledgerNodeA.transferAccountToAccount(
          "5.00",
          xkcdTokenType.descriptor.uri,
          accountId1 at testNodeA,
          "unknown-account" at testNodeA,
          "transfer", defaultNotaryName)
      }
      throw Exception("transfer should have failed")
    } catch (ex: RuntimeException) {
      // all good
    }
  }

  @Test
  fun `we can listen to account updates`(context: TestContext) {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    val notionalString = "5.00"
    val notional = notionalString.toBigDecimal(xkcdTokenType)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()

    val amount = "100.00"
    val fountain = intFountain::next
    execute(network) { ledgerNodeA.issueToken(accountId1, amount, xkcdSymbol, "issuance ${fountain()}", defaultNotaryName) }
    assertEquals(BigDecimal(amount), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("0.00"), ledgerNodeA.balanceForAccount(accountId2, xkcdTokenType))

    val async = context.async()

    ledgerNodeB.listenForTransactions(listOf(accountId2)).subscribe { tokenTransactionSummary: TokenTransactionSummary.State ->
      log.info("return transaction summary $tokenTransactionSummary")
      val amounts = tokenTransactionSummary.amounts.map { it.amount.quantity }
      context.assertTrue(amounts.contains(notional))
      context.assertTrue(amounts.contains(notional.negate()))
      async.complete()
    }

    execute(network) {
      ledgerNodeA.transferAccountToAccount(
        notionalString,
        xkcdTokenType.descriptor.uri,
        accountId1 at testNodeA,
        accountId2 at testNodeB,
        "transfer ${fountain()}", defaultNotaryName)
    }
    assertEquals(BigDecimal("95.00"), ledgerNodeA.balanceForAccount(accountId1, xkcdTokenType))
    assertEquals(BigDecimal("5.00"), ledgerNodeB.balanceForAccount(accountId2, xkcdTokenType))
  }

  @Test
  fun `transaction summary returned when listening to transactions`() {
    val future =
      ledgerNodeA.listenForTransactions(listOf(accountId1)).toFuture()
    ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val fountain = intFountain::next
    execute(network) {
      ledgerNodeA.issueToken(
        accountId1,
        "100.00",
        xkcdSymbol,
        "issuance ${fountain()}",
        defaultNotaryName
      )
    }
    assertNotNull(future.get())
  }

  @Test
  fun `Only retrieve transaction summaries of chosen accounts when listening to transactions`() {
    val future =
      ledgerNodeA.listenForTransactions(listOf(accountId1)).toFuture()
    ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val fountain = intFountain::next
    execute(network) {
      ledgerNodeA.issueToken(
        accountId2,
        "100.00",
        xkcdSymbol,
        "issuance ${fountain()}",
        defaultNotaryName
      )
    }
    execute(network) {
      ledgerNodeA.issueToken(
        accountId1,
        "100.00",
        xkcdSymbol,
        "issuance ${fountain()}",
        defaultNotaryName
      )
    }
    assertNotNull(future.get())
  }

  @Test
  fun `should not blow up when number of transactions goes over the 200 default page size`() {
    ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val fountain = intFountain::next
    repeat(205) {
      execute(network) {
        ledgerNodeA.issueToken(
          accountId2,
          "100.00",
          xkcdSymbol,
          "issuance ${fountain()}}",
          defaultNotaryName
        )
      }
      Unit
    }
    val future = ledgerNodeA.listenForTransactions(listOf(accountId2)).toFuture()
    execute(network) {
      ledgerNodeA.issueToken(
        accountId2,
        "100.00",
        xkcdSymbol,
        "issuance ${fountain()}}",
        defaultNotaryName
      )
    }
    future.get() // this blows up with Please specify a `PageSpecification` as there are more results [201] than the default page size [200]
  }

  @Test
  fun `transaction id set on transaction summary when listening to transactions`() {
    val future =
      ledgerNodeA.listenForTransactions(listOf(accountId1)).toFuture()
    ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    val fountain = intFountain::next
    val hash = execute(network) {
      ledgerNodeA.issueToken(
        accountId1,
        "100.00",
        xkcdSymbol,
        "issuance ${fountain()}",
        defaultNotaryName
      )
    }
    assertEquals(hash, future.get().transactionId!!)
  }

  @Test
  fun `that we can create a token type, issue tokens and transfer to another party and then transfer back`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()
    val fountain = intFountain::next
    ledgerNodeA.issueToken(accountId1, "100.00".toBigDecimal(), xkcdSymbol, "issuance ${fountain()}")
    ledgerNodeA.transferAccountToAccount(
      "50.00".toBigDecimal(),
      xkcdTokenType,
      accountId1 at testNodeA,
      accountId1 at testNodeB,
      "transfer ${fountain()}"
    )
    assertTrue { network.execute { ledgerNodeA.balanceForAccount(accountId1) }.contains("50.00".toBigDecimalAmount(xkcdTokenType)) }
    assertTrue { network.execute { ledgerNodeB.balanceForAccount(accountId1) }.contains("50.00".toBigDecimalAmount(xkcdTokenType)) }

    ledgerNodeB.transferAccountToAccount(
      "50.00".toBigDecimal(),
      xkcdTokenType,
      accountId1 at testNodeB,
      accountId1 at testNodeA,
      "transfer ${fountain()}"
    )
    assertTrue { network.execute { ledgerNodeA.balanceForAccount(accountId1) }.contains("100.00".toBigDecimalAmount(xkcdTokenType)) }
    assertTrue { network.execute { ledgerNodeB.balanceForAccount(accountId1) }.none { it.amountType == xkcdTokenType.descriptor} }
  }

  @Test
  fun `that we can create a token type, issue tokens and transfer, and updates to the token type is seen on the holder of the token`() {
    val xkcdTokenType = ledgerNodeA.createTokenType(xkcdSymbol)
    ledgerNodeA.createAccounts()
    ledgerNodeB.createAccounts()
    val fountain = intFountain::next
    ledgerNodeA.issueToken(accountId1, "100.00".toBigDecimal(), xkcdSymbol, "issuance ${fountain()}")
    ledgerNodeA.transferAccountToAccount(
      "50.00".toBigDecimal(),
      xkcdTokenType,
      accountId1 at testNodeA,
      accountId1 at testNodeB,
      "transfer ${fountain()}"
    )
    val balances = network.execute { ledgerNodeB.balanceForAccount(accountId1) }
    assertTrue(balances.contains("50.00".toBigDecimalAmount(xkcdTokenType)))

    val newDescription = "updated description for XKCD"
    val newSettlementInstructions = listOf(
      mapOf("type" to "SWIFT", "BIC" to "12345")
    )
    network.execute {
      ledgerNodeA.updateTokenType(xkcdTokenType.toUpdateTokenTypeRequest(defaultNotaryName).copy(
        description = newDescription,
        settlements = newSettlementInstructions))
    }
    val types = testNodeB.node.services.vaultService.queryBy(TokenTypeState::class.java).states
    val newType = types.map { it.state.data }.single {
      it.symbol == xkcdSymbol
    }
    assertEquals(newDescription, newType.description)
    assertEquals(newSettlementInstructions, newType.settlements)
  }

  private fun LedgerApi.transferAccountToAccount(
    amount: BigDecimal,
    tokenTypeState: TokenTypeState,
    from: String,
    to: String,
    description: String
  ): SecureHash {
    return network.execute {
      this.transferAccountToAccount(
        amount.toString(),
        tokenTypeState.descriptor.uri,
        from,
        to,
        description,
        defaultNotaryName
      )
    }
  }

  private fun LedgerApi.createAccounts() {
    execute(network) {
      createAccounts(CreateAccountRequests(listOf(
        CreateAccountRequest(accountId1, listOf(swiftCodeTag)),
        CreateAccountRequest(accountId2)
      )), defaultNotaryName)
    }
  }

  private fun LedgerApi.createTokenType(symbol: String): TokenTypeState {
    return execute(network) {
      this.createTokenTypeV2(CreateTokenTypeRequest(
        symbol = symbol,
        exponent = 2,
        description = "token type for $symbol",
        metaData = createRandomMetaData(),
        notary = defaultNotaryName
      ))
    }
  }

  private fun createRandomMetaData(): Any {
    return when (floor(Math.random() * 3.0).toInt()) {
      0 -> createRandomString()
      1 -> createRandomList()
      else -> createRandomObject()
    }
  }

  private val charPool: CharArray = (('a'..'z') + ('A'..'Z') + ('0'..'9')).toCharArray()

  private fun createRandomString(): String {
    return (1..5).map {
      val index = floor(Math.random() * charPool.size).toInt()
      charPool[index]
    }.joinToString("")
  }

  private fun createRandomObject(): Map<String, String> {
    return mapOf("foo" to createRandomString(), "bar" to createRandomString())
  }

  private fun createRandomList(): List<String> {
    return listOf(createRandomString(), createRandomString())
  }

  private fun LedgerApi.balanceForAccount(account: String, tokenType: TokenTypeState): BigDecimal {
    return try {
      val balances = execute(network) { balanceForAccount(account) }.also { log.info("got balances for $account, $it") }.map { it.amountType to it.quantity }.toMap()
      balances[tokenType.descriptor] ?: "0".toBigDecimal(tokenType)
    } catch (err: Throwable) {
      log.error("failed to get balance for $account and $tokenType", err)
      throw err
    }
  }

  private fun LedgerApi.balanceForAccountTag(tag: Tag, tokenType: TokenTypeState): BigDecimal {
    return execute(network) { balanceForAccountTag(tag) }
      .filter { it.amountType == tokenType.descriptor }
      .map { it.quantity }
      .singleOrNull()
      ?: "0".toBigDecimal(tokenType)
  }

  private fun LedgerApi.issueToken(account: String, amount: BigDecimal, symbol: String, description: String) {
    execute(network) { this.issueToken(account, amount.toString(), symbol, description, defaultNotaryName) }
  }
}

class IntFountain {
  private val atomicInt = AtomicInteger(1)
  fun next() = atomicInt.getAndIncrement()
}

fun Int.toBigDecimal(): BigDecimal {
  return BigDecimal(this.toLong())
}

fun Double.toBigDecimal(): BigDecimal {
  return BigDecimal(this)
}

fun BigDecimal.multiply(value: Int): BigDecimal {
  return this.multiply(value.toBigDecimal())
}

fun BigDecimal.minus(value: Int): BigDecimal {
  return this.minus(value.toBigDecimal())
}

fun BigDecimal.minus(value: Double): BigDecimal {
  return this.minus(value.toBigDecimal())
}

fun BigDecimal.divide(value: Int): BigDecimal {
  return this.divide(value.toBigDecimal())
}

infix fun String.at(node: TestNode): String {
  return "$this@${node.node.info.legalIdentities.first().name}"
}