/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.impl

import io.cordite.braid.corda.BraidConfig
import io.cordite.braid.core.annotation.MethodDescription
import io.cordite.commons.braid.BraidCordaService
import io.cordite.commons.database.executeCaseInsensitiveQuery
import io.cordite.commons.utils.toVertxFuture
import io.cordite.commons.utils.transaction
import io.cordite.dgl.api.*
import io.cordite.dgl.api.flows.account.*
import io.cordite.dgl.api.flows.token.flows.CreateTokenTypeFlow
import io.cordite.dgl.api.flows.token.flows.IssueTokensFlow
import io.cordite.dgl.api.flows.token.flows.MultiAccountTokenTransferFlow
import io.cordite.dgl.api.flows.token.flows.UpdateTokenTypeFlow
import io.cordite.dgl.api.flows.tokentypes.findTokenType
import io.cordite.dgl.api.flows.tokentypes.findTokenTypesIssuesByMe
import io.cordite.dgl.api.flows.tokentypes.listAllTokenTypes
import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.account.AccountAddress.Companion.createAccountAddress
import io.cordite.dgl.contract.v1.account.AccountState
import io.cordite.dgl.contract.v1.tag.Tag
import io.cordite.dgl.contract.v1.tag.WellKnownTagCategories
import io.cordite.dgl.contract.v1.tag.WellKnownTagValues
import io.cordite.dgl.contract.v1.token.*
import io.swagger.annotations.ApiParam
import io.vertx.core.Future
import io.vertx.core.Future.succeededFuture
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.StateRef
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.vault.DEFAULT_PAGE_NUM
import net.corda.core.node.services.vault.DEFAULT_PAGE_SIZE
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.utilities.loggerFor
import rx.Observable
import java.math.BigDecimal
import javax.ws.rs.QueryParam

open class LedgerApiImpl(private val services: AppServiceHub, private val sync: () -> Unit = {}) : LedgerApi, BraidCordaService {
  // unlike swift, we don't seem to be able to specify constructors in interfaces
  constructor(services: AppServiceHub) : this(services, {})

  companion object {
    val log = loggerFor<LedgerApiImpl>()
  }

  override fun configureWith(config: BraidConfig): BraidConfig {
    return LedgerRestBindings.bind(config, this)
      .withFlow(CreateTokenTypeFlow::class.java)
      .withFlow(IssueTokensFlow::class.java)
      .withFlow(MultiAccountTokenTransferFlow::class.java)
      .withService("ledger", this)
  }

  override fun wellKnownTagCategories(): List<String> {
    val fields = WellKnownTagCategories::class.java.fields
    return fields
      .filter { it.type == String::class.java }
      .map { it.get(WellKnownTagCategories).toString() }
  }

  override fun wellKnownTagValues(): List<String> {
    val fields = WellKnownTagValues::class.java.fields
    return fields
      .filter { it.type == String::class.java }
      .map { it.get(WellKnownTagCategories).toString() }
  }

  override fun createTokenType(symbol: String, exponent: Int, notary: CordaX500Name): Future<TokenTypeState> {
    return createTokenTypeV2(CreateTokenTypeRequest(symbol = symbol, exponent = exponent, notary = notary, metaData = null))
  }

  @MethodDescription(description = "Create a token type that can then be used to issue tokens. this is a v2 API that takes additional parameters")
  override fun createTokenTypeV2(request: CreateTokenTypeRequest): Future<TokenTypeState> {
    val flow = CreateTokenTypeFlow(request)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture().map { it.tx.outputStates.first() as TokenTypeState }
  }

  @MethodDescription(description = "Retrieve a list of token types given a pageSize (greather than 0) and page (greater than 0)", returnType = TokenTypeState::class)
  override fun listTokenTypesPaged(
    @ApiParam("page number - must be greater than zero", defaultValue = DEFAULT_PAGE_NUM.toString())
    @QueryParam("page-number")
    page: Int,
    @ApiParam("page number - must be greater than zero", defaultValue = DEFAULT_PAGE_SIZE.toString())
    @QueryParam("page-size")
    pageSize: Int
  ): Future<List<TokenTypeState>> {
    return succeededFuture(services.listAllTokenTypes(page, pageSize))
  }

  override fun createAccount(accountId: String, notary: CordaX500Name): Future<AccountState> {
    return createAccount(accountId, findNotary(notary))
  }

  override fun createAccounts(request: CreateAccountRequests, notary: CordaX500Name): Future<Set<AccountState>> {
    val notaryParty = findNotary(notary)
    return createAccounts(request.accountRequests, notaryParty)
  }

  private fun createAccount(accountId: String, notaryParty: Party): Future<AccountState> {
    return createAccounts(listOf(CreateAccountRequest(accountId)), notaryParty).map { it.first() }
  }

  private fun createAccounts(requests: List<CreateAccountRequest>, notaryParty: Party): Future<Set<AccountState>> {
    val mappedRequests = requests.map { CreateAccountFlow.Request(it.accountId, it.tags) }
    val createFlow = CreateAccountFlow(mappedRequests, notaryParty)
    val createAccountFuture = services.startFlow(createFlow).returnValue
    sync()
    return createAccountFuture.toVertxFuture().map { it.tx.outputStates.map { it as AccountState }.toSet() }
  }

  override fun setAccountTag(accountId: String, tag: Tag, notary: CordaX500Name): Future<AccountState> {
    val updateFlow = SetAccountTagFlow(accountId, listOf(tag), findNotary(notary))
    val future = services.startFlow(updateFlow).returnValue
    sync()
    return future.toVertxFuture().map { it.tx.outputStates.first() as AccountState }
  }

  override fun removeAccountTag(accountId: String, category: String, notary: CordaX500Name): Future<AccountState> {
    val updateFlow = RemoveAccountTagFlow(accountId, category, findNotary(notary))
    val future = services.startFlow(updateFlow).returnValue
    sync()
    return future.toVertxFuture().map { it.tx.outputStates.first() as AccountState }
  }

  override fun setAccountTags(accountId: String, tags: List<Tag>, notary: CordaX500Name): Future<AccountState> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun removeAccountTags(accountId: String, categories: List<String>, notary: CordaX500Name): Future<AccountState> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }


  override fun findAccountsByTag(tag: Tag): Future<Set<AccountState>> {
    val flow = FindAccountsFlow(tag)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture()
  }

  override fun getAccount(accountId: String): Future<AccountState> {
    val flow = GetAccountFlow(accountId)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture().map { it.state.data }
  }

  override fun listAccounts(): Future<List<AccountState>> {
    log.trace("listAccounts")
    val flow = ListAllAccountsFlow()
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture()
  }

  override fun listAccountsPaged(paging: PageSpecification): Future<List<AccountState>> {
    log.trace("listAccountsPaged")
    val flow = ListAllAccountsFlow(paging.pageNumber, paging.pageSize)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture()
  }

  override fun bulkIssueTokens(accountIds: List<String>, amount: String, symbol: String, description: String, notary: CordaX500Name): Future<SecureHash> {
    val tokenTypeStateAndRef = services.findTokenTypesIssuesByMe(symbol)
    val issuer = services.myInfo.legalIdentities.first()
    val tokens = generateIssuedTokens(services, accountIds, amount, tokenTypeStateAndRef, issuer)
    val notaryParty = findNotary(notary)
    val flow = IssueTokensFlow(tokens = tokens, notary = notaryParty, description = description)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture().map { it.id }
  }

  override fun updateTokenType(request: UpdateTokenTypeRequest): Future<TokenTypeState> {
    val result = services.startFlow(UpdateTokenTypeFlow(request)).returnValue.toVertxFuture()
    return result.map { it.tx.outputStates.first() as TokenTypeState }
  }

  private fun generateIssuedTokens(
    services: ServiceHub,
    accountIds: List<String>,
    amount: String,
    tokenTypeRefState: StateAndRef<TokenTypeState>,
    issuer: Party
  ): List<TokenState> {
    return accountIds.asSequence()
      .map(this::parseAccountId)
      .map { TokenContract.generateIssuance(services, amount, tokenTypeRefState, it, issuer) }
      .toList()
  }

  private fun parseAccountId(accountId: String): String {
    val parsed = try {
      accountId.parseAccountAddress()
    } catch (e: RuntimeException) {
      null
    }
    return when (parsed) {
      null -> accountId
      else -> {
        val identities = services.myInfo.legalIdentities
        require(identities.asSequence().map(Party::name).contains(parsed.party)) {
          "account is not owned by ${identities.first()}"
        }
        parsed.accountId
      }
    }
  }

  override fun issueToken(
    accountId: String,
    amount: String,
    symbol: String,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash> = bulkIssueTokens(listOf(accountId), amount, symbol, description, notary)

  override fun balanceForAccount(accountId: String): Future<Set<BigDecimalAmount<TokenDescriptor>>> {
    return succeededFuture(services.getBalances(accountId))
  }

  data class BigDecimalAmountTokenDescriptor(val quantity: BigDecimal, val amountType: TokenDescriptor)

  override fun balanceForAccountTag(tag: Tag): Future<Set<BigDecimalAmount<TokenDescriptor>>> {
    return succeededFuture(services.getBalancesForTag(tag))
  }

  @Deprecated(message = "this is part of the old API", replaceWith = ReplaceWith("transferAccountToAccount"))
  override fun transferToken(
    amount: String,
    tokenTypeUri: String,
    fromAccount: String,
    toAccount: String,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash> {
    return transferAccountToAccount(amount, tokenTypeUri, fromAccount, toAccount, description, notary)
  }

  override fun transferAccountToAccount(
    amount: String,
    tokenTypeUri: String,
    fromAccount: String,
    toAccount: String,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash> {
    val from = mapOf(amount to fromAccount)
    val to = mapOf(amount to toAccount)
    return transferAccountsToAccounts(tokenTypeUri, from, to, description, notary)
  }

  override fun transferAccountsToAccounts(
    tokenTypeUri: String,
    amountFromAccountMap: Map<String, String>,
    amountToAccountMap: Map<String, String>,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash> {
    val tokenType = readTokenType(tokenTypeUri).state.data
    val parsedFrom = amountFromAccountMap.map { Pair(services.parseAndResolveAccountAddress(it.value), it.key.toBigDecimalAmount(tokenType)) }
    val parsedTo = amountToAccountMap.map { Pair(services.parseAndResolveAccountAddress(it.value), it.key.toBigDecimalAmount(tokenType)) }
    val notaryParty = findNotary(notary)
    val flow = MultiAccountTokenTransferFlow(parsedFrom, parsedTo, description, notaryParty)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture().map { it.id }
  }

  override fun transactionsForAccount(
    accountId: String,
    paging: PageSpecification
  ): List<TokenTransactionSummary.State> {
    val accountAddress = services.parseAndResolveAccountAddress(accountId)
    val stmt = """
SELECT TRANSACTION_ID, OUTPUT_INDEX from CORDITE_TOKEN_TRANSACTION_AMOUNT
WHERE ACCOUNT_ID_URI in ('${accountAddress.uri}')
ORDER BY TRANSACTION_TIME DESC
      """

    log.info("using account address: $accountAddress for $accountId")

    return services.transaction {
      val stateRefs = services.jdbcSession().executeCaseInsensitiveQuery(stmt)
          .map {
            val sh = SecureHash.parse(it.getString("TRANSACTION_ID"))
            val i = it.getInt("OUTPUT_INDEX")
            StateRef(sh, i)
          }.toList().toBlocking().first()

      // TODO: figure out why postgres blows up if you pass in an empty list...
      if (stateRefs.size == 0) {
        emptyList()
      } else {
        services.vaultService.queryBy(
            contractStateType = TokenTransactionSummary.State::class.java,
            criteria = QueryCriteria.VaultQueryCriteria(stateRefs = stateRefs),
            paging = paging
        ).states
            .map { it.state.data.copy(transactionId = it.ref.txhash) }
            .sortedByDescending { it.transactionTime }
      }
    }
  }


  override fun listenForTransactions(
    accountIds: List<String>
  ): Observable<TokenTransactionSummary.State> {
    val accountAddresses = accountIds
      .map { it.trim() }
      .filter { it.isNotEmpty() }
      .map { parseFromAccount(it) }

    return services.vaultService
      .trackBy(
        TokenTransactionSummary.State::class.java,
        QueryCriteria.VaultQueryCriteria(),
        PageSpecification(1, 1)
      )
      .updates
      .map { update ->
        update.produced.single {
          // Intellij says that the `single` check below is "useless"
          // it is needed to filter out other states from the `produced` list
          @Suppress("USELESS_IS_CHECK")
          it.state.data is TokenTransactionSummary.State
        }
          .let { it.state.data.copy(transactionId = it.ref.txhash) }
      }.let { txStateObservable ->
        if (accountAddresses.isNotEmpty()) {
          txStateObservable.filter { state ->
            state.amounts.any { accountAddresses.contains(it.accountAddress) }
          }
        } else {
          txStateObservable
        }
      }
      .doOnError {
        log.error("listen for account failed", it)
      }
      .doOnNext {
        log.info("item received {}", it)
      }
  }

  private fun readTokenType(str: String): StateAndRef<TokenTypeState> {
    val parts = str.split(TokenDescriptor.SEPARATOR)
    return if (parts.size > 1) {
      // this is a uri to a token type
      val descriptor = TokenDescriptor.parse(str)
      services.findTokenType(descriptor.symbol, descriptor.issuerName)
        ?: error("unknown token type $descriptor")
    } else {
      // this is a reference to a local token type
      services.findTokenTypesIssuesByMe(str)
    }
  }

  private fun parseFromAccount(fromAccount: String): AccountAddress {
    return try {
      val tag = Tag.parse(fromAccount)
      services.findAccountsWithTag(tag).first().address
    } catch (_: Throwable) {
      createAccountAddress(fromAccount, services.myInfo.legalIdentities.first().name)
    }
  }

  private fun findNotary(notary: CordaX500Name): Party {
    return services.networkMapCache.getNotary(notary) ?: throw RuntimeException("could not find notary $notary")
  }
}
