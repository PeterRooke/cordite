/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.distribution.dataDistribution
import io.cordite.dgl.api.CreateTokenTypeRequest
import io.cordite.dgl.api.flows.crud.CrudCreateFlow
import io.cordite.dgl.contract.v1.token.TokenTypeContract
import io.cordite.dgl.contract.v1.token.TokenTypeState
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.flows.StartableByService
import net.corda.core.transactions.SignedTransaction

@InitiatingFlow
@StartableByRPC
@StartableByService
class CreateTokenTypeFlow(
  private val request: CreateTokenTypeRequest
) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val state = TokenTypeState(
      symbol = request.symbol,
      exponent = request.exponent,
      description = request.description,
      issuer = ourIdentity,
      metaData = request.metaData,
      settlements = request.settlements
    )
    return subFlow(CrudCreateFlow(
      TokenTypeState::class.java,
      listOf(state),
      TokenTypeContract.CONTRACT_ID,
      serviceHub.networkMapCache.getNotary(request.notary) ?: error("could not find notary ${request.notary}")
    )).also {
      serviceHub.dataDistribution().create(state.linearId, state.descriptor.uri)
    }
  }
}