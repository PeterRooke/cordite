/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.api.flows.account.accountExists
import io.cordite.dgl.api.flows.account.verifyAccountsExist
import io.cordite.dgl.api.flows.token.flows.TokenTransactionSummaryFunctions.addTokenTransactionSummary
import io.cordite.dgl.api.flows.token.selection.AbstractTokenSelection
import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.token.*
import io.cordite.dgl.contract.v1.token.TokenTransactionSummary.NettedAccountAmount
import net.corda.confidential.IdentitySyncFlow
import net.corda.core.contracts.ContractState
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.TransactionState
import net.corda.core.flows.CollectSignatureFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.FlowSession
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.loggerFor
import net.corda.core.utilities.unwrap
import java.math.BigDecimal
import java.security.PublicKey

class TransferTokenRecipientFunctions {
  companion object {
    fun checkTokenMoveTransaction(stx: SignedTransaction, serviceHub: ServiceHub) {
      // verify that we are receiving funds for a known account
      // Verify that we know who all the participants in the transaction are
      val states: Iterable<ContractState> = stx.tx.inputs.map { serviceHub.loadState(it).data } + stx.tx.outputs.map { it.data }
      states.forEach { state ->
        state.participants.forEach { anon ->
          require(serviceHub.identityService.wellKnownPartyFromAnonymous(anon) != null) {
            "Transaction state $state involves unknown participant $anon"
          }
        }
      }
      // TODO: check that the totals of input and output cash are equal https://gitlab.com/cordite/cordite/issues/289
    }
  }
}

class TransferTokenSenderFunctions {
  companion object {
    private val logger = loggerFor<TransferTokenRecipientFunctions>()

    @Suspendable
    fun prepareMultiTokenMoveWithSummary(
      txb: TransactionBuilder,
      from: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>,
      to: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>,
      serviceHub: ServiceHub,
      ourIdentity: Party,
      description: String
    ): List<PublicKey> {
      val recipients = to.asSequence().map { findRecipient(serviceHub, it.first) }.toList().distinct()
      require(recipients.size == 1) { "there can be one and only one recipient in a multi token transfer" }
      val recipient = recipients.single()
      verifyAccounts(from.map { it.first }, to.map { it.first }, recipient, serviceHub)
      // gets account balances for token used in amount
      val inputsForMultipleAccounts = from.map { (accountAddress, amount) -> collectCoinsAndSoftLock(txb, accountAddress, amount, serviceHub, txb.notary as Party) }
      val inputs = inputsForMultipleAccounts.reduce { acc, set -> acc.union(set) }
      val outputs = computeOutputsForMultiMove(inputsForMultipleAccounts, from, to, recipient)
      return prepareTokenMove(txb, inputs, outputs).apply {
        val summaryList = generateSummaryList(from, to)
        txb.addTokenTransactionSummary(TokenContract.Command.Move(), ourIdentity, description, listOf(recipient), summaryList)
      }
    }

    @Suspendable
    private fun generateSummaryList(from: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>,
                                    to: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>):
      List<NettedAccountAmount> {

      val fromSummary = from.map {
        NettedAccountAmount(it.first, -it.second)
      }
      val toSummary = to.map {
        NettedAccountAmount(it.first, it.second)
      }
      return fromSummary + toSummary
    }


    @Suspendable
    private fun prepareTokenMove(txb: TransactionBuilder, inputs: Set<StateAndRef<TokenState>>, outputs: Set<TransactionState<TokenState>>): List<PublicKey> {
      val inputSigningKeys = inputs.map { it.state.data.owner.owningKey }.distinct()
      val outputSigningKeys = outputs.map { it.data.owner.owningKey }
      val signingKeys = (inputSigningKeys + outputSigningKeys).distinct()
      inputs.forEach { txb.addInputState(it) }
      outputs.forEach { txb.addOutputState(it) }
      txb.addCommand(TokenContract.Command.Move(), signingKeys)
      return inputSigningKeys
    }


    /**
     * Verify that a set [fromAccounts] and [toAccounts] are valid
     * Fails if the local accounts do not exist
     * Or if there is an overlap between from and to accounts (we cannot transfer within the same account)
     */
    @Suspendable
    private fun verifyAccounts(
      fromAccounts: List<AccountAddress>,
      toAccounts: List<AccountAddress>,
      recipient: Party,
      serviceHub: ServiceHub
    ) {
      toAccounts.intersect(fromAccounts).apply {
        if (isNotEmpty()) {
          throw RuntimeException("cannot transfer between the same accounts ${joinToString(",")}")
        }
      }
      serviceHub.verifyAccountsExist(fromAccounts)
      if (serviceHub.myInfo.isLegalIdentity(recipient)) {
        serviceHub.verifyAccountsExist(toAccounts)
      }
    }

    @Suspendable
    private fun computeOutputsForMultiMove(
      inputsForMultipleAccounts: List<Set<StateAndRef<TokenState>>>,
      from: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>,
      to: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>,
      recipient: Party
    ): Set<TransactionState<TokenState>> {
      val outputs = mutableSetOf<TransactionState<TokenState>>()
      val templateState = inputsForMultipleAccounts.first().first().state
      if (inputsForMultipleAccounts.size != from.size) {
        val msg = "input data sizes mismatch: balance list size (${inputsForMultipleAccounts.size}) does not match account list size (${from.size})"
        logger.error(msg)
        throw IllegalStateException(msg)
      }
      from.forEachIndexed { index, _ ->
        val addressAndAmount = from[index]
        outputs += createRemainder(addressAndAmount.first, inputsForMultipleAccounts[index], addressAndAmount.second)
      }

      to.forEach {
        outputs += deriveState(
          templateState,
          it.first.accountId,
          it.second,
          recipient)
      }

      return outputs
    }

    @Suspendable
    private fun deriveState(txs: TransactionState<TokenState>,
                            accountId: String,
                            amount: BigDecimalAmount<TokenDescriptor>,
                            owner: AbstractParty) =
      txs.copy(data = txs.data.copy(amount = amount, owner = owner, accountId = accountId))

    @Suspendable
    private fun createRemainder(
      fromAccount: AccountAddress,
      inputs: Set<StateAndRef<TokenState>>,
      amount: BigDecimalAmount<TokenDescriptor>
    ): Set<TransactionState<TokenState>> {
      val outputs = mutableSetOf<TransactionState<TokenState>>()
      val total = inputs.map { it.state.data.amount }.sum(amount.amountType)
      val remainder = total - amount
      val templateState = inputs.first().state
      when {
        remainder > BigDecimal.ZERO -> // calculate the 'change' returned
          outputs += deriveState(templateState,
            fromAccount.accountId,
            remainder,
            inputs.first().state.data.owner)
        remainder < BigDecimal.ZERO -> {
          val msg = "required $amount but only collected $total"
          logger.error(msg)
          throw IllegalStateException(msg)
        }
        else -> {
          logger.trace("no change required for $amount from $total")
        }
      }
      return outputs
    }

    @Suspendable
    private fun collectCoinsAndSoftLock(
      txb: TransactionBuilder,
      fromAccount: AccountAddress,
      amount: BigDecimalAmount<TokenDescriptor>,
      serviceHub: ServiceHub,
      notary: Party
    ): Set<StateAndRef<TokenState>> {
      val tokenSelectionAlgo = AbstractTokenSelection.getInstance { serviceHub.jdbcSession().metaData }
      return tokenSelectionAlgo.unconsumedTokenStatesForSpending(serviceHub, amount, fromAccount.accountId, notary, txb.lockId)
    }

    /**
     * collects token move signatures, returning the signed transaction and the list of sessions created
     */
    @Suspendable
    fun <T> FlowLogic<T>.collectTokenMoveSignatures(
      stx: SignedTransaction,
      serviceHub: ServiceHub,
      toAccounts: Set<AccountAddress>
    ): Pair<SignedTransaction, List<FlowSession>> {
      return toAccounts
        .asSequence()
        // lookup the recipients of the accounts
        .map { toAccount -> toAccount to findRecipient(serviceHub, toAccount) }
        // filter out accounts that this node owns
        .filter { (toAccount, recipient) ->
          when {
            !serviceHub.myInfo.isLegalIdentity(recipient) -> true
            !serviceHub.accountExists(toAccount) -> error("account does not exist on this node: $toAccount")
            else -> false
          }
        }
        .fold(stx to emptyList()) { (stx, sessions), (toAccount, recipient) ->
          val (newStx, session) = collectTokenMoveSignatureFromOtherParty(stx, toAccount, recipient)
          newStx to (sessions + session)
        }
    }

    /**
     * collect signatures from party that's not us
     */
    @Suspendable
    private fun <T> FlowLogic<T>.collectTokenMoveSignatureFromOtherParty(
      stx: SignedTransaction,
      toAccount: AccountAddress,
      recipient: Party
    ): Pair<SignedTransaction, FlowSession> {
      logger.debug("checking existence of remote account")
      val session = initiateFlow(recipient)
      val response = session.sendAndReceive<String>(toAccount).unwrap { it }
      logger.debug("account check response $response")
      subFlow(IdentitySyncFlow.Send(session, stx.tx))
      val sellerSignature = subFlow(CollectSignatureFlow(stx, session, session.counterparty.owningKey))
      return (stx + sellerSignature) to session
    }

    @Suspendable
    fun findRecipient(serviceHub: ServiceHub, recipientAccount: AccountAddress): Party {
      return serviceHub.networkMapCache
        .getNodeByLegalName(recipientAccount.party)
        ?.legalIdentities?.first()
        ?: error("cannot find recipient $recipientAccount")
    }
  }
}

object TokenTransactionSummaryFunctions {
  @Suspendable
  fun TransactionBuilder.addTokenTransactionSummary(
    command: TokenContract.Command,
    ourIdentity: Party,
    description: String,
    participants: List<AbstractParty>,
    amounts: List<NettedAccountAmount>) {
    val summary = TokenTransactionSummary.State(
      participants = (participants.toSet() + ourIdentity).toList(),
      command = command,
      description = description,
      amounts = amounts)
    addOutputState(summary, TokenTransactionSummary.CONTRACT_ID)
  }
}
