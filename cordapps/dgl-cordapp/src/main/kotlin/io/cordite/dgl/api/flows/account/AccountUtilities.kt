/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.account

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.database.executeCaseInsensitiveQuery
import io.cordite.commons.utils.transaction
import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.account.AccountContract
import io.cordite.dgl.contract.v1.account.AccountSchemaV1
import io.cordite.dgl.contract.v1.account.AccountState
import io.cordite.dgl.contract.v1.tag.Tag
import io.cordite.dgl.contract.v1.token.BigDecimalAmount
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import net.corda.core.contracts.StateRef
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.utilities.loggerFor
import rx.Observable

private val log = loggerFor<AccountContract>()

@Suspendable
fun ServiceHub.verifyAccountsExist(accounts: List<AccountAddress>) {
  require(accounts.isNotEmpty()) { "no accounts to verify" }
  val accountIds = accounts.map { it.accountId }
  val foundAccounts = vaultService.queryBy(AccountState::class.java, builder {
    QueryCriteria.VaultCustomQueryCriteria(AccountSchemaV1.PersistentAccount::accountId.`in`(accountIds))
  }).states.map { it.state.data.address.accountId }
  require(foundAccounts.size == accounts.size) { "could not locate ${accountIds.subtract(foundAccounts)}" }
}

fun ServiceHub.accountExists(account: AccountAddress): Boolean {
  val stmt = """SELECT COUNT(*) as AC_COUNT FROM CORDITE_ACCOUNT_ALIAS WHERE CATEGORY = 'DGL.ID' AND VALUE='$account'"""

  return transaction {
    val accountCount = jdbcSession().executeCaseInsensitiveQuery(stmt).map { it.getLong("AC_COUNT") }.toList().toBlocking().first().first()
    accountCount >= 1L
  }
}

fun ServiceHub.getBalances(accountId: String): Set<BigDecimalAmount<TokenDescriptor>> {
  return try {
    val stmt = """
SELECT SUM(TOKEN.AMOUNT) as TOTAL, TOKEN.SYMBOL AS SYMBOL, STATE_STATUS, TOKEN_TYPE.ISSUER AS ISSUER, TOKEN_TYPE.EXPONENT AS EXPONENT
FROM CORDITE_TOKEN as TOKEN
  JOIN VAULT_STATES AS STATES
    ON TOKEN.TRANSACTION_ID = STATES.TRANSACTION_ID
      AND TOKEN.OUTPUT_INDEX = STATES.OUTPUT_INDEX
      AND STATES.STATE_STATUS = 0
  LEFT JOIN CORDITE_TOKEN_TYPE AS TOKEN_TYPE
      ON TOKEN.SYMBOL = TOKEN_TYPE.SYMBOL
        AND TOKEN.ISSUER = TOKEN_TYPE.ISSUER
WHERE TOKEN.ACCOUNT_ID = '$accountId'
GROUP BY TOKEN.SYMBOL, STATES.STATE_STATUS, TOKEN_TYPE.ISSUER, TOKEN_TYPE.EXPONENT
 """
    transaction {
      jdbcSession().executeCaseInsensitiveQuery(stmt)
        .map { recordSet ->
          val exponent = recordSet.getInt("EXPONENT")
          val quantity = recordSet.getBigDecimal("TOTAL").setScale(exponent)
          val symbol = recordSet.getString("SYMBOL")
          val issuerName = CordaX500Name.parse(recordSet.getString("ISSUER"))
          BigDecimalAmount(quantity, TokenDescriptor(symbol, issuerName))
        }
        .onErrorResumeNext {
          log.error("balance query for $accountId failed", it)
          Observable.error(RuntimeException("failed to get balance from query", it))
        }
        .toBlocking().toIterable()
        .toSet()
    }
  } catch (err: Throwable) {
    log.error("balance query for $accountId failed", err)
    throw err
  }
}

fun ServiceHub.findAccountsWithTag(tag: Tag, paging: PageSpecification = PageSpecification()): Set<AccountState> {
  val stmt = """
        SELECT TRANSACTION_ID, OUTPUT_INDEX
        FROM CORDITE_ACCOUNT_ALIAS
        WHERE CATEGORY = '${tag.category}'
        AND VALUE = '${tag.value}'
        """
  return transaction {
    val stateRefs = jdbcSession().executeCaseInsensitiveQuery(stmt)
      .map {
        val sh = SecureHash.parse(it.getString("TRANSACTION_ID"))
        val i = it.getInt("OUTPUT_INDEX")
        StateRef(sh, i)
      }.toList().toBlocking().first()

    vaultService.queryBy(
      contractStateType = AccountState::class.java,
      criteria = QueryCriteria.VaultQueryCriteria(stateRefs = stateRefs),
      paging = paging
    ).states.map { it.state.data }.toSet()
  }
}

fun ServiceHub.getBalancesForTag(accountTag: Tag): Set<BigDecimalAmount<TokenDescriptor>> {
  val stmt = """
      SELECT SUM(TOKEN.AMOUNT) as TOTAL, TOKEN.SYMBOL AS SYMBOL, STATE_STATUS, TOKEN_TYPE.ISSUER AS ISSUER, TOKEN_TYPE.EXPONENT as EXPONENT
       FROM CORDITE_ACCOUNT_ALIAS as ALIAS
       JOIN CORDITE_ACCOUNT as ACCOUNT
        ON ALIAS.TRANSACTION_ID = ACCOUNT.TRANSACTION_ID
          AND ALIAS.OUTPUT_INDEX = ACCOUNT.OUTPUT_INDEX
       JOIN CORDITE_TOKEN as TOKEN
        ON TOKEN.ACCOUNT_ID = ACCOUNT.ACCOUNT
       JOIN VAULT_STATES AS STATES
        ON TOKEN.TRANSACTION_ID = STATES.TRANSACTION_ID
          AND TOKEN.OUTPUT_INDEX = STATES.OUTPUT_INDEX
          AND STATES.STATE_STATUS = 0
       LEFT JOIN CORDITE_TOKEN_TYPE AS TOKEN_TYPE
        ON TOKEN.SYMBOL = TOKEN_TYPE.SYMBOL
          AND TOKEN.ISSUER = TOKEN_TYPE.ISSUER
        WHERE ALIAS.CATEGORY = '${accountTag.category}'
        AND ALIAS.VALUE = '${accountTag.value}'
        GROUP BY TOKEN.SYMBOL, STATES.STATE_STATUS, TOKEN_TYPE.ISSUER
        """

  return transaction {
    jdbcSession().executeCaseInsensitiveQuery(stmt)
      .map {
        val exponent = it.getInt("EXPONENT")
        val quantity = it.getBigDecimal("TOTAL").setScale(exponent)
        val symbol = it.getString("SYMBOL")
        val issuerName = CordaX500Name.parse(it.getString("ISSUER"))
        BigDecimalAmount(quantity, TokenDescriptor(symbol, issuerName))
      }
      .toList()
      .toBlocking()
      .first()
      .toSet()
  }
}

