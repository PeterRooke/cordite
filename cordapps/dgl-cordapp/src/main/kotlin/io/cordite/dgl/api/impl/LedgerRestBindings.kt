/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.impl

import io.cordite.braid.corda.BraidConfig
import io.cordite.commons.braid.amendRestConfig
import io.cordite.dgl.api.CreateAccountRequest
import io.cordite.dgl.api.CreateAccountRequests
import io.cordite.dgl.api.CreateTokenTypeRequest
import io.cordite.dgl.api.UpdateTokenTypeRequest
import io.cordite.dgl.contract.v1.account.AccountState
import io.cordite.dgl.contract.v1.tag.Tag
import io.cordite.dgl.contract.v1.token.TokenTransactionSummary
import io.cordite.dgl.contract.v1.token.TokenTypeState
import io.swagger.annotations.ApiModelProperty
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import io.vertx.core.Future
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.node.services.vault.DEFAULT_PAGE_NUM
import net.corda.core.node.services.vault.DEFAULT_PAGE_SIZE
import net.corda.core.node.services.vault.PageSpecification
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam

class LedgerRestBindings(private val ledger: LedgerApiImpl) {
  companion object {
    fun bind(config: BraidConfig, ledger: LedgerApiImpl): BraidConfig {
      return LedgerRestBindings(ledger).bind(config)
    }
  }

  fun bind(config: BraidConfig): BraidConfig {
    // TODO: think braid config should blow up if we replace some things - e.g. paths - so not checking here - will replace with braid issue
    return config.amendRestConfig {
      withPaths {
        group("ledger token types") {
          protected {
            get("/ledger/token-types", ledger::listTokenTypesPaged)
            post("/ledger/token-types/:symbol", this@LedgerRestBindings::createTokenTypeRest)
            put("/ledger/token-types/:symbol", this@LedgerRestBindings::updateTokenTypeRest)
          }
        }
        group("ledger accounts") {
          protected {
            get("/ledger/accounts", this@LedgerRestBindings::listAccountsPagedRest)
            post("/ledger/accounts/:account", this@LedgerRestBindings::createAccountRest)
            post("/ledger/accounts", this@LedgerRestBindings::createAccountsRest)
            put("/ledger/accounts/:account/tags/:category", this@LedgerRestBindings::setAccountTagRest)
            put("/ledger/accounts/:account/tags", this@LedgerRestBindings::setAccountTagsRest)
            delete("/ledger/accounts/:account/tags/:category", this@LedgerRestBindings::removeAccountTagRest)
            delete("/ledger/accounts/:account/tags", this@LedgerRestBindings::removeAccountTagsRest)
          }
        }
        group("ledger transactions and balances") {
          protected {
            post("/ledger/accounts/:account/tokens/:symbol", this@LedgerRestBindings::issueTokenRest)
            post("/ledger/accounts/:debtorAccount/transfers/:creditorAccount", this@LedgerRestBindings::transferAccountToAccountRest)
            get("/ledger/accounts/:account/transactions", this@LedgerRestBindings::transactionsForAccountRest)
            get("/ledger/accounts/:account/balances", this@LedgerRestBindings::balanceForAccountRest)
            get("/ledger/tags/:tag/balances", this@LedgerRestBindings::balanceForAccountTagRest)
          }
        }
        group("ledger tags") {
          protected {
            get("/ledger/tags/well-known/categories", ledger::wellKnownTagCategories)
            get("/ledger/tags/well-known/values", ledger::wellKnownTagValues)
          }
        }
      }
    }
  }

  @ApiOperation(value = "Create a token type that can then be used to issue tokens")
  fun createTokenTypeRest(
    @ApiParam("the token type symbol") @PathParam("symbol") symbol: String,
    @ApiParam("the request payload") request: CreateTokenTypePayload
  ): Future<TokenTypeState> {
    return ledger.createTokenTypeV2(CreateTokenTypeRequest(
      symbol,
      request.exponent,
      request.description,
      request.metaData,
      request.settlements,
      CordaX500Name.parse(request.notary)
    ))
  }

  @ApiOperation("Update an existing token type. All nodes on the network that have or have owned tokens of this type will be informed of the change")
  fun updateTokenTypeRest(
    @ApiParam("the token type symbol") @PathParam("symbol") symbol: String,
    request: UpdateTokenTypePayload
  ): Future<TokenTypeState> {
    val payload = UpdateTokenTypeRequest(symbol, request.symbol, request.exponent, request.description, request.metaData, request.settlements, CordaX500Name.parse(request.notary))
    return ledger.updateTokenType(payload)
  }

  @ApiOperation("Retrieves a page of accounts")
  fun listAccountsPagedRest(
    @QueryParam("page-number")
    @ApiParam(value = "page number - must be greater than zero", defaultValue = 1.toString(), required = true)
    pageNumber: Int,
    @ApiParam(value = "max accounts per page", defaultValue = DEFAULT_PAGE_SIZE.toString(), required = true)
    @QueryParam("page-size") pageSize: Int
  ): Future<List<AccountState>> {
    return ledger.listAccountsPaged(PageSpecification(pageNumber, pageSize))
  }

  @ApiOperation(value = "Create an account, given its unique id for this node")
  fun createAccountRest(
    @ApiParam("the identifier for the account")
    @PathParam("account")
    accountId: String,
    @ApiParam(value = "the x500 name for the notary")
    @QueryParam("notary")
    notary: String
  ): Future<AccountState> {
    return ledger.createAccount(accountId, CordaX500Name.parse(notary))
  }

  @ApiOperation("Bulk create a set of accounts")
  fun createAccountsRest(payload: CreateAccountsPayload): Future<Set<AccountState>> {
    return ledger.createAccounts(CreateAccountRequests(payload.requests), CordaX500Name.parse(payload.notary))
  }

  @ApiOperation("Set a tag category and value against a given account")
  fun setAccountTagRest(@ApiParam("the selected account") @PathParam("account") accountId: String,
                        @ApiParam("the tag category - this is effectively the name of the category") @PathParam("category") category: String,
                        @ApiParam(value = "the tag value", required = true, example = "location") @QueryParam("value") value: String,
                        @ApiParam(value = "the x500 name for the notary", required = true) @QueryParam("notary") notary: String): Future<AccountState> {
    return ledger.setAccountTag(accountId, Tag(category, value), CordaX500Name.parse(notary))
  }

  @ApiOperation("Set a tag category and value against a given account")
  fun setAccountTagsRest(@ApiParam("the selected account") @PathParam("account") accountId: String,
                        @ApiParam("the tag category - this is effectively the name of the category") tags: List<Tag>,
                        @ApiParam(value = "the x500 name for the notary", required = true) @QueryParam("notary") notary: String): Future<AccountState> {
    return ledger.setAccountTags(accountId, tags, CordaX500Name.parse(notary))
  }

  @ApiOperation("Remove a tag from a given account")
  fun removeAccountTagRest(
    @ApiParam("the account which will be updated with thse removed tag") @PathParam("account") accountId: String,
    @ApiParam("the tag category") @PathParam("category") category: String,
    @ApiParam(value = "the x500 name of the notary", required = true) @QueryParam("notary") notary: String): Future<AccountState> {
    return ledger.removeAccountTag(accountId, category, CordaX500Name.parse(notary))
  }

  @ApiOperation("Remove a tag from a given account")
  fun removeAccountTagsRest(
    @ApiParam("the account which will be updated with the removed tag") @PathParam("account") accountId: String,
    @ApiParam("the tag category") categories: List<String>,
    @ApiParam(value = "the x500 name of the notary", required = true) @QueryParam("notary") notary: String): Future<AccountState> {
    return ledger.removeAccountTags(accountId, categories, CordaX500Name.parse(notary))
  }

  @ApiOperation("Issue tokens of pre declared token type symbol to an existing account")
  fun issueTokenRest(
    @ApiParam(value = "account id to issue token to", required = true, example = "USD")
    @PathParam("account")
    accountId: String,
    @ApiParam(example = "12.34")
    @QueryParam("amount")
    amount: String,
    @ApiParam(value = "must match an already created token type for this node", example = "USD")
    @PathParam("symbol")
    symbol: String,
    @ApiParam(value = "human readable description / reason for the issuance", required = true)
    @QueryParam("description")
    description: String,
    @ApiParam(value = "x500 name for the notary", required = true)
    @QueryParam("notary")
    notary: String
  ) = ledger.issueToken(accountId, amount, symbol, description, CordaX500Name.parse(notary))

  @ApiOperation("Transfer a sum of tokens from one account to another")
  fun transferAccountToAccountRest(
    @ApiParam("the debtor account id - must be on the local node") @PathParam("debtorAccount") debtorAccount: String,
    @ApiParam("the creditor account id - either a local node account id or a fully qualified account URI on the network") @PathParam("creditorAccount") creditorAccount: String,
    request: TransferAccountToAccountPayload): Future<SecureHash> {
    return with(request) {
      ledger.transferAccountToAccount(amount, tokenTypeURI, debtorAccount, creditorAccount, description, CordaX500Name.parse(notary))
    }
  }

  @ApiOperation("Retrieve a page of transactions for a given account, given the page number (greater than zero) and page size (also greater than zero)")
  fun transactionsForAccountRest(
    @ApiParam("account id") @PathParam("account")
    accountId: String,
    @ApiParam(value = "page number - must be greater than zero", defaultValue = DEFAULT_PAGE_NUM.toString())
    @QueryParam("page-number")
    pageNumber: Int,
    @ApiParam(value = "page size - must be greater than zero", defaultValue = DEFAULT_PAGE_SIZE.toString())
    @QueryParam("page-size")
    pageSize: Int): List<TokenTransactionSummary.State> {
    return ledger.transactionsForAccount(accountId, PageSpecification(pageNumber, pageSize))
  }

  fun balanceForAccountRest(@QueryParam("account") accountId: String): Future<List<LedgerApiImpl.BigDecimalAmountTokenDescriptor>> {
    return ledger.balanceForAccount(accountId).map { balances ->
      balances.map { LedgerApiImpl.BigDecimalAmountTokenDescriptor(it.quantity, it.amountType) }
    }
  }

  fun balanceForAccountTagRest(@ApiParam(
    value = "grouping tag for account in the form <code>[tag-category-name]:[tag-value]</code>",
    example = "location:US"
  ) tag: String): Future<List<LedgerApiImpl.BigDecimalAmountTokenDescriptor>> {
    val parsed = Tag.parse(tag)
    return ledger.balanceForAccountTag(parsed).map { balances ->
      balances.map { LedgerApiImpl.BigDecimalAmountTokenDescriptor(it.quantity, it.amountType) }
    }
  }
}

data class CreateAccountsPayload(
  @ApiModelProperty("a list of account creation payloads") val requests: List<CreateAccountRequest>,
  @ApiModelProperty("the x500 name for the notary") val notary: String
)

data class UpdateTokenTypePayload(
  @ApiModelProperty("the new symbol for this token type", example = "USD")
  val symbol: String,
  @ApiModelProperty("the new number of decimal places for this token type", example = "2")
  val exponent: Int,
  @ApiModelProperty("the new description for this token type")
  val description: String = "",
  @ApiModelProperty("the new meta-data for this token type")
  val metaData: Any? = null,
  @ApiModelProperty("the new list of settlement descriptors")
  val settlements: List<Any> = emptyList(),
  @ApiModelProperty("the x500 name for the notary")
  val notary: String
)

data class TransferAccountToAccountPayload(
  @ApiModelProperty("the amount of the token type to transfer")
  val amount: String,
  @ApiModelProperty("the fully qualified URI for the token type")
  val tokenTypeURI: String,
  @ApiModelProperty("a description or reason for the transaction")
  val description: String,
  @ApiModelProperty("the x500 name of the notary")
  val notary: String
)

data class CreateTokenTypePayload(
  @ApiModelProperty("number of decimal places for this token type", example = "2")
  val exponent: Int,
  @ApiModelProperty("description of this token type")
  val description: String = "",
  @ApiModelProperty("any additional meta data")
  val metaData: Any? = null,
  @ApiModelProperty("array of settlement routes - this is an open type to allow for customisation")
  val settlements: List<Any> = emptyList(),
  @ApiModelProperty("the x500 name for the notary")
  val notary: String
)