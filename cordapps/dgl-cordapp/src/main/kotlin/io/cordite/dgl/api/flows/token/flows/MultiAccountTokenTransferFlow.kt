/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.cordite.dgl.api.flows.token.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.distribution.dataDistribution
import io.cordite.dgl.api.flows.token.flows.TransferTokenSenderFunctions.Companion.collectTokenMoveSignatures
import io.cordite.dgl.api.flows.token.flows.TransferTokenSenderFunctions.Companion.prepareMultiTokenMoveWithSummary
import io.cordite.dgl.api.flows.tokentypes.findTokenType
import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.token.BigDecimalAmount
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import io.cordite.dgl.contract.v1.token.TokenState
import io.cordite.dgl.contract.v1.token.TokenTypeState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.math.BigDecimal

@InitiatingFlow
@StartableByRPC
@StartableByService
class MultiAccountTokenTransferFlow(
  private val from: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>,
  private val to: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>,
  private val description: String = "",
  private val notary: Party
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    validate()

    val txb = TransactionBuilder(notary = notary)
    val inputSigningKeys = prepareMultiTokenMoveWithSummary(txb, from, to, serviceHub, ourIdentity, description)
    val tx = serviceHub.signInitialTransaction(txb, inputSigningKeys)
    val (stx, sessions) = collectTokenMoveSignatures(tx, serviceHub, to.map { it.first }.toSet())
    val signedTx = subFlow(FinalityFlow(stx, sessions))
    updateDistributionGroups(txb)

    return signedTx
  }

  private fun validate() {
    checkAmounts(from)
    checkAmounts(to)
    checkInputsAndOutputsAreZeroSum(from, to)
  }

  @Suspendable
  private fun checkInputsAndOutputsAreZeroSum(from: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>, to: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>) {
    val totals = mutableMapOf<TokenDescriptor, BigDecimal>()
    from.fold(totals) { acc, (_, amount) ->
      val descriptor = amount.amountType
      val quantity = amount.quantity
      acc.compute(descriptor) { _, current ->
        when (current) {
          null -> -quantity
          else -> current - quantity
        }
      }
      acc
    }
    to.fold(totals) { acc, (_, amount) ->
      val descriptor = amount.amountType
      val quantity = amount.quantity
      acc.compute(descriptor) { _, current ->
        when (current) {
          null -> quantity
          else -> current + quantity
        }
      }
      acc
    }
    check(totals.values.all { it.compareTo(BigDecimal.ZERO) == 0 }) { "totals did not equal zero"}
  }

  @Suspendable
  private fun updateDistributionGroups(txb: TransactionBuilder) {
    val allTokenTypes = to.map { (_, amount) -> serviceHub.findTokenType(amount.amountType) }.filterNotNull().map { it.state.data }
    val ourTokenTypes = allTokenTypes.filter { serviceHub.myInfo.isLegalIdentity(it.issuer) }
    val otherTokenTypes = (allTokenTypes - ourTokenTypes)

    val outputTokens = txb.outputStates().map { it.data }.filterIsInstance<TokenState>()
    updateDistributionGroupsMaintainedByUs(outputTokens, ourTokenTypes.map { it.linearId to it }.toMap())
    updateDistributionGroupsMaintainedByOthers(outputTokens, otherTokenTypes.map { it.linearId to it }.toMap())
  }

  @Suspendable
  private fun updateDistributionGroupsMaintainedByUs(outputTokens: List<TokenState>, ourTokenTypes: Map<UniqueIdentifier, TokenTypeState>) {
    outputTokens
      .asSequence()
      .filter { token -> ourTokenTypes.containsKey(token.tokenTypePointer.pointer) && !serviceHub.myInfo.isLegalIdentity(token.owner as Party) }
      .map { token -> token.tokenTypePointer.pointer to token.owner as Party }
      .apply { serviceHub.dataDistribution().addMembers(this.toSet()) }
  }

  @Suspendable
  private fun updateDistributionGroupsMaintainedByOthers(outputTokens: List<TokenState>, otherTokenTypes: Map<UniqueIdentifier, TokenTypeState>) {
    data class UpdateMembership(val maintainer: Party, val groupId: UniqueIdentifier, val newMember: Party)
    outputTokens
      .asSequence()
      .filter { token -> otherTokenTypes.containsKey(token.tokenTypePointer.pointer) }
      .map { token -> token.tokenTypePointer.pointer to token.owner as Party }
      .map { (groupId, newMember) -> UpdateMembership(otherTokenTypes.getValue(groupId).issuer, groupId, newMember) }
      .groupBy({ it.maintainer }, { it.groupId to it.newMember })
      .forEach { (maintainer, updates) ->
        serviceHub.dataDistribution().requestAdditionOfMember(this, maintainer, updates.toSet())
      }
  }

  @Suspendable
  private fun checkAmounts(amounts: List<Pair<AccountAddress, BigDecimalAmount<TokenDescriptor>>>) {
    requireThat {
      "that there are balances to transfer" to amounts.isNotEmpty()
      "that amounts are greater than zero" to amounts.all { it.second.quantity > BigDecimal.ZERO }
    }
  }
}
