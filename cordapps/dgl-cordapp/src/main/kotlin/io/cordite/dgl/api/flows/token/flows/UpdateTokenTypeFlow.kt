/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.distribution.dataDistribution
import io.cordite.dgl.api.UpdateTokenTypeRequest
import io.cordite.dgl.api.flows.crud.CrudUpdateFlow
import io.cordite.dgl.api.flows.tokentypes.findTokenTypesIssuesByMe
import io.cordite.dgl.contract.v1.token.TokenTypeContract
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.flows.StartableByService
import net.corda.core.transactions.SignedTransaction

@InitiatingFlow
@StartableByRPC
@StartableByService
class UpdateTokenTypeFlow(private val request: UpdateTokenTypeRequest) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val notary = serviceHub.networkMapCache.getNotary(request.notary) ?: error("could not find notary ${request.notary}")
    val input = serviceHub.findTokenTypesIssuesByMe(request.currentSymbol)
    val newState = input.state.data.copy(
      symbol = request.symbol,
      exponent = request.exponent,
      description = request.description,
      metaData = request.metaData,
      settlements = request.settlements
    )
    return subFlow(CrudUpdateFlow(
      listOf(input),
      listOf(newState),
      TokenTypeContract.CONTRACT_ID,
      notary
    )).also {
      serviceHub.dataDistribution().updateDistributionGroup(this, newState.linearId)
    }
  }
}