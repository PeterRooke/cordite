/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@Suspendable
fun <T> FlowLogic<T>.execute(transactionBuilder: TransactionBuilder) : SignedTransaction {
  val signingKeys = transactionBuilder.commands().flatMap { it.signers }
  val ourKeys = serviceHub.keyManagementService.filterMyKeys(signingKeys)
  val pstx = serviceHub.signInitialTransaction(transactionBuilder, ourKeys)
  val counterparties = pstx.tx.outputs.flatMap { it.data.participants }.map { it as Party } - ourIdentity
  val sessions = counterparties.map { initiateFlow(it) }
  val finalStx = subFlow(CollectSignaturesFlow(pstx, sessions))
  return subFlow(FinalityFlow(finalStx, sessions))
}

@Suspendable
fun FlowLogic<*>.signAndFinalise(otherSession: FlowSession, checkTransaction: (SignedTransaction) -> Unit = {}) : SignedTransaction {
  val stx = subFlow(object : SignTransactionFlow(otherSession) {
    override fun checkTransaction(stx: SignedTransaction) {
      checkTransaction(stx)
    }
  })
  return subFlow(ReceiveFinalityFlow(otherSession, stx.id))
}
