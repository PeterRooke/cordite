/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api

import io.cordite.braid.core.annotation.MethodDescription
import io.cordite.braid.core.annotation.ServiceDescription
import io.cordite.dgl.contract.v1.account.AccountState
import io.cordite.dgl.contract.v1.tag.Tag
import io.cordite.dgl.contract.v1.token.BigDecimalAmount
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import io.cordite.dgl.contract.v1.token.TokenTransactionSummary
import io.cordite.dgl.contract.v1.token.TokenTypeState
import io.vertx.core.Future
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.node.services.vault.DEFAULT_PAGE_NUM
import net.corda.core.node.services.vault.DEFAULT_PAGE_SIZE
import net.corda.core.node.services.vault.PageSpecification
import rx.Observable

/**
 * Formal interface to interact with the ledger using Braid
 */
@ServiceDescription(name = "ledger", description = "API for accessing the distributed general ledger")
interface LedgerApi {
  /**
   * List of well-known tag categories
   * Emits list for [io.cordite.dgl.contract.v1.tag.WellKnownTagCategories]
   */
  @MethodDescription(description = "retrieve the list of well-know tag categories")
  fun wellKnownTagCategories(): List<String>

  /**
   * List of well-known tag values. Emits list for [io.cordite.dgl.contract.v1.tag.WellKnownTagValues]
   */
  @MethodDescription(description = "retrieve the list of well-known tag values")
  fun wellKnownTagValues(): List<String>

  /**
   * Create a token type that can be used for issuing tokens
   * @param symbol - typically a 3 or 4 uppercase code
   * @param exponent - the number of fractional decimal places in the range 0..[io.cordite.dgl.contract.v1.token.TokenTypeState.AMOUNT_MAX_EXPONENT]
   * @param notary - notary to be used when creating this token type
   */
  @MethodDescription(description = "create a token type that can then be used to issue tokens")
  fun createTokenType(
    symbol: String,
    exponent: Int,
    notary: CordaX500Name
  ): Future<TokenTypeState>

  /**
   * Create a token type that can be used for issuing tokens
   * @param request payload for token type definition
   */
  fun createTokenTypeV2(
    request: CreateTokenTypeRequest
  ): Future<TokenTypeState>

  /**
   * Update the definition of a token type
   * @param request - payload describing the state of the update
   */
  @MethodDescription(description = "Update the definition of a token type, given its symbol, with new values", returnType = TokenTypeState::class)
  fun updateTokenType(request: UpdateTokenTypeRequest) : Future<TokenTypeState>

  /**
   * Retrieve a list of token types given
   * @param pageSize the number of token types per page
   * @param page the request page. First page is 1
   */
  fun listTokenTypesPaged(
    page: Int = DEFAULT_PAGE_NUM,
    pageSize: Int = DEFAULT_PAGE_SIZE
  ): Future<List<TokenTypeState>>

  /**
   * Retrieve a list of up to [DEFAULT_PAGE_SIZE] token types
   */
  @MethodDescription(description = "retrieve $DEFAULT_PAGE_SIZE token types", returnType = TokenTypeState::class)
  fun listTokenTypes() = listTokenTypesPaged(DEFAULT_PAGE_NUM, DEFAULT_PAGE_SIZE)

  /**
   * Create an account given its [accountId]
   */
  @MethodDescription(description = "create an account given its accountId", returnType = AccountState::class)
  fun createAccount(accountId: String, notary: CordaX500Name): Future<AccountState>

  /**
   * Create a set of accounts as a list of [request]
   */
  @MethodDescription(description = "create a set of accounts given their accountIds", returnType = AccountState::class)
  fun createAccounts(request: CreateAccountRequests, notary: CordaX500Name): Future<Set<AccountState>>

  /**
   * Set a tag on an account
   */
  @MethodDescription(description = "set a tag on an account")
  fun setAccountTag(accountId: String, tag: Tag, notary: CordaX500Name): Future<AccountState>

  @MethodDescription(description = "set tags on an account")
  fun setAccountTags(accountId: String, tags: List<Tag>, notary: CordaX500Name) : Future<AccountState>

  @MethodDescription(description = "remove a tag on an account")
  fun removeAccountTag(accountId: String, category: String, notary: CordaX500Name): Future<AccountState>

  @MethodDescription(description = "remove tags on an account")
  fun removeAccountTags(accountId: String, categories: List<String>, notary: CordaX500Name): Future<AccountState>

  fun getAccount(accountId: String): Future<AccountState>

  fun findAccountsByTag(tag: Tag): Future<Set<AccountState>>

  fun listAccounts(): Future<List<AccountState>>

  fun listAccountsPaged(paging: PageSpecification): Future<List<AccountState>>

  fun bulkIssueTokens(
    accountIds: List<String>,
    amount: String,
    symbol: String,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash>

  fun issueToken(
    accountId: String,
    amount: String,
    symbol: String,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash>

  fun balanceForAccount(accountId: String): Future<Set<BigDecimalAmount<TokenDescriptor>>>

  fun balanceForAccountTag(tag: Tag): Future<Set<BigDecimalAmount<TokenDescriptor>>>

  @Deprecated(message = "this is part of the old API", replaceWith = ReplaceWith("transferAccountToAccount"))
  fun transferToken(
    amount: String,
    tokenTypeUri: String,
    fromAccount: String,
    toAccount: String,
    description: String,
    notary: CordaX500Name
  ) : Future<SecureHash>

  fun transferAccountToAccount(
    amount: String,
    tokenTypeUri: String,
    fromAccount: String,
    toAccount: String,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash>

  fun transferAccountsToAccounts(
    tokenTypeUri: String,
    amountFromAccountMap: Map<String, String>,
    amountToAccountMap: Map<String, String>,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash>

  fun transactionsForAccount(accountId: String, paging: PageSpecification): List<TokenTransactionSummary.State>

  @MethodDescription(returnType = TokenTransactionSummary.State::class, description = "listen for transactions against one or more accounts")
  fun listenForTransactions(accountIds: List<String>): Observable<TokenTransactionSummary.State>
}

data class CreateAccountRequest(val accountId: String, val tags: List<Tag> = emptyList())
data class CreateAccountRequests(val accountRequests: List<CreateAccountRequest>)

data class CreateTokenTypeRequest(
  val symbol: String,
  val exponent: Int,
  val description: String = "",
  val metaData: Any? = null,
  val settlements: List<Any> = emptyList(),
  val notary: CordaX500Name
)

data class UpdateTokenTypeRequest(
  val currentSymbol: String,
  val symbol: String,
  val exponent: Int,
  val description: String = "",
  val metaData: Any? = null,
  val settlements: List<Any> = emptyList(),
  val notary: CordaX500Name
)

fun TokenTypeState.toUpdateTokenTypeRequest(notary: CordaX500Name) : UpdateTokenTypeRequest {
  return UpdateTokenTypeRequest(
    currentSymbol = symbol,
    symbol = symbol,
    exponent = exponent,
    description = description,
    notary = notary
  )
}