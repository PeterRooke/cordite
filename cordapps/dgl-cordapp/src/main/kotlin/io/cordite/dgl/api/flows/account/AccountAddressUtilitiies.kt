/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.account

import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.account.AccountAddress.Companion.createAccountAddress
import io.cordite.dgl.contract.v1.tag.Tag
import net.corda.core.identity.CordaX500Name
import net.corda.core.node.ServiceHub

fun String.parseAccountAddress() : AccountAddress {
  val parts = this.split('@')
  if (parts.size != 2) {
    throw RuntimeException("address is malformed: $this")
  }
  return AccountAddress(accountId = parts[0], party = CordaX500Name.parse(parts[1]))
}

fun ServiceHub.parseAndResolveAccountAddress(address: String) : AccountAddress {
  return try {
    val tag = Tag.parse(address)
    findAccountsWithTag(tag).first().address
  } catch (_: Throwable) {
    return try {
      address.parseAccountAddress()
    } catch (err: Throwable) {
      createAccountAddress(address, myInfo.legalIdentities.first().name)
    }
  }
}
