'
'   Copyright 2018, Cordite Foundation.
'
'    Licensed under the Apache License, Version 2.0 (the "License");
'    you may not use this file except in compliance with the License.
'    You may obtain a copy of the License at
'
'      http://www.apache.org/licenses/LICENSE-2.0
'
'    Unless required by applicable law or agreed to in writing, software
'    distributed under the License is distributed on an "AS IS" BASIS,
'    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'    See the License for the specific language governing permissions and
'    limitations under the License.
'

@startuml
title Core Business-Agnostic Core

skinparam shadowing false
skinparam NoteBackgroundColor White

skinparam class {
	BackgroundColor White
	ArrowColor SeaGreen
  NoteBackgroundColor White
}

skinparam object {
	BackgroundColor White
	ArrowColor SeaGreen
  NoteBackgroundColor White
}

left to right direction

' --- BEGIN Requests payloads ---

interface Request {
  nonce: Long
}

interface Response {
  nonce: Long
}

class Authorised<Payload> {
}


AtomicRequest *--> "*" Request: has 2 or more
AtomicResponse *--> "*" Response: has 2 or more


interface Ledger {
  invoke(request: Authorised<Request>) : Response
}

Ledger ..> Request: consumes
Ledger ..> Response: responds with
Ledger ..> Authorised: authorised by
AtomicRequest --|> Request: is a
AtomicResponse --|> Response: is a


' --- BEGIN Authentication ---

class AuthenticatedUser {
  token: String
}

class User {
  userId: String
}

interface AuthService {
  authenticate(credentials): AuthenticatedUser
}

AuthService ..> AuthenticatedUser : generates

User "*" <..> "*" Business
User "*" <..> "*" Role
Role "*" <..> "*" Permission
Authorised *--> "1" Request: payload
Authorised *--> "1" AuthenticatedUser
AuthenticatedUser *--> User

CreateUserRequest --|> Request
CreateRoleRequest --|> Request
CreateBusinessRequest --|> Request

CreateUserResponse --|> Response
CreateRoleResponse --|> Response
CreateBusinessResponse --|> Response

CreateUserRequest <..> CreateUserResponse
CreateRoleRequest <..> CreateRoleResponse
CreateBusinessRequest <..> CreateBusinessResponse
@enduml