/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.cordapp

import io.cordite.commons.distribution.impl.DataDistributionGroup
import io.cordite.cordapp.braid.BraidServerCordaService
import io.cordite.cordapp.braid.BraidServerCordaService.Companion.CORDITE_TLS_ENABLED
import io.cordite.dgl.api.impl.LedgerApiImpl
import io.cordite.dgl.contract.v1.account.AccountContract
import net.corda.core.identity.CordaX500Name
import net.corda.core.internal.packageName
import net.corda.core.utilities.getOrThrow
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.driver.DriverParameters
import net.corda.testing.driver.driver
import net.corda.testing.node.TestCordapp
import net.corda.testing.node.User
import net.corda.testing.node.internal.findCordapp
import java.lang.System.setProperty

fun main(args: Array<String>) {
  // No permissions required as we are not invoking core.
  val user = User("user1", "test", permissions = setOf())
  setProperty(CORDITE_TLS_ENABLED, false.toString())
  driver(
    DriverParameters(
      isDebug = true,
      waitForAllNodesToFinish = true,
      startNodesInProcess = true,
      networkParameters = testNetworkParameters(minimumPlatformVersion = 4),
      cordappsForAllNodes = listOf(
        findCordapp(DataDistributionGroup::class.packageName),
        findCordapp(BraidServerCordaService::class.packageName),
        findCordapp(LedgerApiImpl::class.packageName),
        findCordapp(AccountContract::class.packageName)
      )
    )
  ) {
    val (_, _) = listOf(
      startNode(
        providedName = CordaX500Name(
          commonName = null,
          organisation = "Nielsen Group",
          organisationUnit = "Nielsen",
          country = "GB",
          locality = "London",
          state = null
        ), rpcUsers = listOf(user)
      )
      ,
      startNode(
        providedName = CordaX500Name(
          commonName = null,
          organisation = "Sable Group",
          organisationUnit = "Sable",
          country = "GB",
          locality = "London",
          state = null
        ), rpcUsers = listOf(user)
      )
    ).map { it.getOrThrow() }
  }
}

