/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.token

import net.corda.core.serialization.CordaSerializable
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Equivalent class to Corda's [net.corda.core.contracts.Amount] but using [BigDecimal] to represent higher
 * scales and precision.
 */
@CordaSerializable
data class BigDecimalAmount<T : Any>(val quantity: BigDecimal, val amountType: T) : Comparable<BigDecimalAmount<T>> {
  /**
   * Construct given a [String] [quantity] and [amountType]
   */
  constructor(quantity: String, amountType: T) : this(BigDecimal(quantity), amountType)
  /**
   * Construct given a [Int] [quantity] and [amountType]
   */
  constructor(quantity: Int, amountType: T) : this(BigDecimal(quantity), amountType)
  /**
   * Construct given a [Long] [quantity] and [amountType]
   */
  constructor(quantity: Long, amountType: T) : this(BigDecimal(quantity), amountType)
  /**
   * Construct given a [Double] [quantity] and [amountType]
   */
  constructor(quantity: Double, amountType: T) : this(BigDecimal(quantity), amountType)

  /**
   * Compare with another [BigDecimalAmount]
   */
  override operator fun compareTo(other: BigDecimalAmount<T>): Int {
    check(this.amountType == other.amountType) { "cannot compare amounts of different types" }
    return this.quantity.compareTo(other.quantity)
  }
}

/**
 * Compare the quantity to a [BigDecimal]
 */
inline operator fun <reified T : Any> BigDecimalAmount<T>.compareTo(rhs: BigDecimal): Int {
  return this.quantity.compareTo(rhs)
}

/**
 * Returns a [BigDecimalAmount] with the quantity negated
 */
inline operator fun <reified T : Any> BigDecimalAmount<T>.unaryMinus() = copy(quantity = -this.quantity)

/**
 * Returns [this] - [rhs]
 */
inline operator fun <reified T : Any> BigDecimalAmount<T>.minus(rhs: BigDecimalAmount<T>): BigDecimalAmount<T> {
  check(this.amountType == rhs.amountType) { "cannot subtract amounts of different type" }
  return this.copy(quantity = quantity - rhs.quantity)
}

/**
 * Returns this + rhs
 */
inline operator fun <reified T : Any> BigDecimalAmount<T>.plus(rhs: BigDecimalAmount<T>): BigDecimalAmount<T> {
  check(this.amountType == rhs.amountType) { "cannot add amounts of different type" }
  return this.copy(quantity = quantity + rhs.quantity)
}

/**
 * Returns the sum of an [Iterable] of [BigDecimalAmount]
 */
inline fun <reified T : Any> Iterable<BigDecimalAmount<T>>.sum(amountType: T): BigDecimalAmount<T> {
  return this.reduce { acc, item ->
    check(item.amountType == amountType) {
      """during summing up a set of ${BigDecimalAmount::class.simpleName} a token did"""
    }
    BigDecimalAmount(quantity = acc.quantity + item.quantity, amountType = amountType)
  }
}

/**
 * This method provides a token conserving divide mechanism.
 * @param partitions the number of amounts to divide the current quantity into.
 * @return 'partitions' separate Amount objects which sum to the same quantity as this Amount
 * and differ by no more than a single token in size.
 */
fun BigDecimal.splitEvenly(partitions: Int): List<BigDecimal> {
  val partitionsBD = partitions.toBigDecimal()
  require(partitions >= 1) { "Must split amount into one, or more pieces" }
  val commonTokensPerPartition = this.div(partitionsBD)
  val residualTokens = this - (commonTokensPerPartition * partitionsBD)
  val splitAmountPlusOne = commonTokensPerPartition + BigDecimal.ONE
  return (0 until partitions).map { if (it.toBigDecimal() < residualTokens) splitAmountPlusOne else commonTokensPerPartition }.toList()
}

/**
 * This method provides a token conserving divide mechanism.
 * @param partitions the number of amounts to divide the current quantity into.
 * @return 'partitions' separate Amount objects which sum to the same quantity as this Amount
 * and differ by no more than a single token in size, using a scale and rounding, the first n-1 will be the same, the last will contain the difference.
 */

fun BigDecimal.splitReasonablyEvently(partitions: Int, scale: Int): List<BigDecimal> {
  val partitionsBD = partitions.toBigDecimal()
  require(partitions >= 1) { "Must split amount into one, or more pieces" }
  val commonTokensPerPartition = this.divide(partitionsBD, scale, RoundingMode.DOWN)
  val residualTokens = this.minus(commonTokensPerPartition.multiply((partitions-1).toBigDecimal()))
  return (0 until partitions - 1).map { if (it < partitions - 2) commonTokensPerPartition else residualTokens }.toList()
}

/**
 * Convert a [String] to a [BigDecimal] given a [tokenType]
 */
fun String.toBigDecimal(tokenType: TokenTypeState) : BigDecimal {
  val dp = tokenType.exponent
  return BigDecimal(this).apply {
    check(scale() <= dp) { "amount must have an exponent less than or equal to that specified for token type"}
  }.setScale(dp, RoundingMode.HALF_UP)
}

/**
 * Convert a [String] to a [BigDecimalAmount] given a [tokenType]
 */
fun String.toBigDecimalAmount(tokenType: TokenTypeState) : BigDecimalAmount<TokenDescriptor> {
  return BigDecimalAmount(toBigDecimal(tokenType), tokenType.descriptor)
}
