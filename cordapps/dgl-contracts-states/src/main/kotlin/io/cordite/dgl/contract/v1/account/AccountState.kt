/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.account

import io.cordite.dgl.contract.v1.tag.Tag
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import java.util.*
import javax.persistence.*

@BelongsToContract(AccountContract::class)
data class AccountState(
  val address: AccountAddress,
  val tags: Set<Tag> = emptySet(),
  override val linearId: UniqueIdentifier = UniqueIdentifier(id = UUID.randomUUID(), externalId = address.accountId),
  override val participants: List<AbstractParty>) : LinearState, QueryableState {

  override fun generateMappedObject(schema: MappedSchema): PersistentState {
    val account = AccountSchemaV1.PersistentAccount(accountId = address.accountId, linearStateId = linearId.id)
    account.persistentAliases = tags.map { tag ->
      val alias = AccountSchemaV1.PersistentAlias(tag.category, tag.value)
      alias.persistentAccount = account
      alias
    }.toMutableSet()
    return account
  }

  override fun supportedSchemas(): Iterable<MappedSchema> = listOf(AccountSchemaV1)
}

object AccountSchema
object AccountSchemaV1 : MappedSchema(
  AccountSchema::class.java, 1,
  setOf(AccountSchemaV1.PersistentAccount::class.java, AccountSchemaV1.PersistentAlias::class.java)) {
  @Entity
  @Table(name = "CORDITE_ACCOUNT")
  class PersistentAccount(
    @Column(name = "linearStateId")
    val linearStateId: UUID,
    @Column(name = "account")
    val accountId: String
  ) : PersistentState() {
    @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
    @JoinColumns(JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"), JoinColumn(name = "output_index", referencedColumnName = "output_index"))
    @OrderColumn
    var persistentAliases: MutableSet<PersistentAlias> = mutableSetOf()
  }

  @Entity
  @Table(name = "CORDITE_ACCOUNT_ALIAS",
    indexes = [Index(name = "cordite_account_tag_index", columnList = "category,value", unique = false)])
  class PersistentAlias(
    @Column(name = "category", nullable = false)
    var category: String,
    @Column(name = "value", nullable = true)
    var value: String
  ) {
    @Id
    @GeneratedValue
    @Column(name = "child_id", unique = true, nullable = false)
    var childId: Int? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns(JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"), JoinColumn(name = "output_index", referencedColumnName = "output_index"))
    var persistentAccount: PersistentAccount? = null
  }
}