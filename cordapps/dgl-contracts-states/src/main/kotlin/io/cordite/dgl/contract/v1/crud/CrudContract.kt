/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.crud

import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction
import kotlin.reflect.KClass

abstract class CrudContract<T : LinearState>(private val stateClazz: KClass<T>): Contract {
  val contractClassName : ContractClassName = stateClazz.java.name
  override fun verify(tx: LedgerTransaction) {
    val command = tx.commands.requireSingleCommand<CrudCommands>()
    val groups = tx.groupStates(stateClazz.java) { it: T -> it.linearId }
    for ((inputs, outputs, _) in groups) {
      when (command.value) {
        is CrudCommands.Create -> requireThat {
          "there are no inputs" using (inputs.isEmpty())
          "there are one or more outputs" using (outputs.isNotEmpty())
        }
        is CrudCommands.Update -> {
          requireThat {
            "there are one or more inputs that match in size to outputs" using
                (inputs.isNotEmpty() && inputs.size == outputs.size)
          }
          requireThat {
            "that the identifiers in the inputs match the outputs" using
                (inputs.map { it.linearId.id }.toSet() == outputs.map { it.linearId.id }.toSet())
          }
        }
        is CrudCommands.Delete -> {
          requireThat { "there are one or more inputs" using (inputs.isNotEmpty())}
          requireThat { "there are zero outputs" using (outputs.isEmpty())}
        }
      }
    }
  }
}
