/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.account

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import net.corda.core.identity.CordaX500Name
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
@JsonIgnoreProperties(ignoreUnknown = true, allowGetters = true, value = ["uri"])
data class AccountAddress(val accountId: String, val party: CordaX500Name) {
  companion object {
    fun createAccountAddress(accountId: String, party: CordaX500Name): AccountAddress {
      return AccountAddress(accountId, party)
    }
  }
  val uri get() = "$accountId@$party"
  override fun toString(): String {
    return uri
  }
}