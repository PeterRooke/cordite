/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.token

import io.cordite.dgl.contract.v1.crud.CrudCommands
import net.corda.core.identity.CordaX500Name
import net.corda.core.internal.packageName
import net.corda.core.internal.packageName_
import net.corda.testing.core.TestIdentity
import net.corda.testing.node.MockServices
import net.corda.testing.node.ledger
import org.junit.Test

class TokenTypeContractTest {

    private val theNode = CordaX500Name("theNode", "Bristol", "GB")
    private val identity = TestIdentity(theNode)
    private val ledgerServices = MockServices(cordappPackages = listOf(this.javaClass.packageName_))

    /**
     * The bellow requirement is based on the fact that the Amount in Corda is stored as Long.
     * Exponent of 19 would make the maximum fraction token value to be 0.9,223,372,036,854,775,807.
     */
    @Test(expected = IllegalArgumentException::class)
    fun `valid token type request cannot have exponent larger than max exponent`() {
        TokenTypeState("GRG", TokenTypeState.AMOUNT_MAX_EXPONENT + 1, "", identity.party)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `valid token request cannot have negative exponent`() {
        TokenTypeState("GRG", -1, "", identity.party)
    }

    @Test
    fun `valid token request can have 0 exponent`() {
        ledgerServices.ledger {
            transaction {
                output(TokenTypeContract.CONTRACT_ID, TokenTypeState("GRG", 0, "", identity.party))
                command(identity.publicKey, CrudCommands.Create)
                verifies()
            }
        }
    }

    @Test
    fun `valid token request can have 19 exponent`() {
        ledgerServices.ledger {
            transaction {
                output(TokenTypeContract.CONTRACT_ID, TokenTypeState("GRG", 19, "", identity.party))
                command(identity.publicKey, CrudCommands.Create)
                verifies()
            }
        }
    }

    @Test
    fun `should create new valid token type`() {
        ledgerServices.ledger {
            transaction {
                output(TokenTypeContract.CONTRACT_ID, TokenTypeState("GRG", 19, "", identity.party))
                command(identity.publicKey, CrudCommands.Create)
                verifies()
            }
        }
    }
}