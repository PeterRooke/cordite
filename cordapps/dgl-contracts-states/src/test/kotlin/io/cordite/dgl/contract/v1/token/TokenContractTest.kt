/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.token

import io.cordite.dgl.contract.v1.crud.CrudCommands
import net.corda.core.identity.CordaX500Name
import net.corda.core.internal.packageName
import net.corda.core.internal.packageName_
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.core.TestIdentity
import net.corda.testing.node.MockServices
import net.corda.testing.node.ledger
import org.junit.Test

class TokenContractTest {
  private val firstName = CordaX500Name("theNode", "Bristol", "GB")
  private val firstIdentity = TestIdentity(firstName)
  private val secondName = CordaX500Name("theNode2", "London", "GB")
  private val secondIdentity = TestIdentity(secondName)
  private val ledgerServices = MockServices(
    listOf(this.javaClass.packageName_),
    firstIdentity,
    testNetworkParameters(minimumPlatformVersion = 4),
    secondIdentity
  )
  private val tokenTypeState = TokenTypeState("GRG", 2, "", firstIdentity.party)
  private val amount = "1".toBigDecimalAmount(tokenTypeState)

  @Test
  fun `valid Token request has issuer the same as TokenType issuer`() {
    val tokenState = TokenState(
      "accountId",
      amount,
      tokenTypeState.linearPointer(),
      firstIdentity.party
    )

    ledgerServices.ledger {
      transaction {
        output(TokenTypeContract.CONTRACT_ID, "token-type", tokenTypeState)
        command(firstIdentity.publicKey, CrudCommands.Create)
        verifies()
      }
      transaction {
        command(firstIdentity.publicKey, TokenContract.Command.Issue)
        output(TokenContract.CONTRACT_ID, tokenState)
        reference("token-type")
        verifies()
      }
    }
  }

  @Test
  fun `Token issuing party cannot be different to TokenType party`() {
    val tokenState = TokenState(
      "accountId",
      amount,
      tokenTypeState.linearPointer(),
      secondIdentity.party,
      firstIdentity.party
    )

    ledgerServices.ledger {
      transaction {
        output(TokenTypeContract.CONTRACT_ID, "token-type", tokenTypeState)
        command(firstIdentity.publicKey, CrudCommands.Create)
        verifies()
      }
      transaction {
        command(secondIdentity.publicKey, TokenContract.Command.Issue)
        output(TokenContract.CONTRACT_ID, tokenState)
        reference("token-type")
        failsWith("issuer of the token is the same as the TokenType owner")
      }
    }
  }

  @Test
  fun `issue token command should have no inputs`() {
    val tokenState = TokenState(
      "accountId",
      amount,
      tokenTypeState.linearPointer(),
      firstIdentity.party
    )

    ledgerServices.ledger {
      transaction {
        output(TokenTypeContract.CONTRACT_ID, "token-type", tokenTypeState)
        command(firstIdentity.publicKey, CrudCommands.Create)
        verifies()
      }

      transaction {
        command(firstIdentity.publicKey, TokenContract.Command.Issue)
        input(TokenContract.CONTRACT_ID, tokenState)
        reference("token-type")
        failsWith("There should be no inputs")
      }
    }
  }
}