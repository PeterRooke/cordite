/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import co.paralleluniverse.fibers.Suspendable
import io.cordite.metering.contract.MeteringTermsAndConditionsCommands
import io.cordite.metering.contract.MeteringTermsAndConditionsContract
import io.cordite.metering.contract.MeteringTermsAndConditionsState
import io.cordite.metering.contract.MeteringTermsAndConditionsStatus
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step

object AcceptMeteringTermsAndConditionsProposeFlow {

  private val log: Logger = LoggerFactory.getLogger(AcceptMeteringTermsAndConditionsProposeFlow.javaClass)

  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class TermsAndConditionsAccepter(val originalTermsAndConditionsLinearId: UniqueIdentifier) : FlowLogic<SignedTransaction>() {
    companion object {
      object BUILD_ACCEPT_METERING_TERMS_CONDITIONS_TRANSACTION : Step("Build the metering terms and conditions acceptance transaction")
      object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
      object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
      object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
        override fun childProgressTracker() = CollectSignaturesFlow.tracker()
      }
    }

    object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
      override fun childProgressTracker() = FinalityFlow.tracker()
    }

    override val progressTracker = ProgressTracker(
        BUILD_ACCEPT_METERING_TERMS_CONDITIONS_TRANSACTION,
        VERIFYING_TRANSACTION,
        SIGNING_TRANSACTION,
        GATHERING_SIGS,
        FINALISING_TRANSACTION
    )

    @Suspendable
    override fun call(): SignedTransaction {
      val termsAndConditions = getTermsAndConditionsState(originalTermsAndConditionsLinearId)

      val partiesToSign = termsAndConditions.state.data.participants
          .filter { it !in serviceHub.myInfo.legalIdentities }
          .map { serviceHub.identityService.requireWellKnownPartyFromAnonymous(it) }

      val txBuilder = buildTransaction(termsAndConditions, partiesToSign)
      val partiallySignedTx = verifyAndSignTransaction(txBuilder)


      val fullySignedTx = getSignatureFromReceiver(partiallySignedTx, partiesToSign.toList())
      val finalisedTx = finaliseTransaction(fullySignedTx)
      return finalisedTx
    }

    fun buildTransaction(termsAndConditions: StateAndRef<MeteringTermsAndConditionsState>, partiesToSign: List<Party>): TransactionBuilder {
      progressTracker.currentStep = BUILD_ACCEPT_METERING_TERMS_CONDITIONS_TRANSACTION

      val originalTermsAndConditionsProperties = termsAndConditions.state.data.meteringTermsAndConditionsProperties

      val acceptedTermsAndConditionsProperties = originalTermsAndConditionsProperties.copy(
          status = MeteringTermsAndConditionsStatus.ACCEPTED
      )

      val notary = originalTermsAndConditionsProperties.guardianNotaryParty

      val me = serviceHub.myInfo.legalIdentities.toSet()
      val newOwner = (partiesToSign - me).singleOrNull() ?: throw FlowException("Only should be one new owner")


      val termsAndConditionsStateToIssue = MeteringTermsAndConditionsState(acceptedTermsAndConditionsProperties, newOwner)
      val txCommand = Command(MeteringTermsAndConditionsCommands.Accept(), termsAndConditionsStateToIssue.participants.map { it.owningKey })

      return TransactionBuilder(notary)
          .addInputState(termsAndConditions)
          .addOutputState(termsAndConditionsStateToIssue, MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
          .addCommand(txCommand)
    }

    @Suspendable
    fun verifyAndSignTransaction(txBuilder: TransactionBuilder): SignedTransaction {
      progressTracker.currentStep = VERIFYING_TRANSACTION
      txBuilder.verify(serviceHub)
      progressTracker.currentStep = SIGNING_TRANSACTION
      val partiallySignedTx = serviceHub.signInitialTransaction(txBuilder)
      return partiallySignedTx
    }

    @Suspendable
    fun getSignatureFromReceiver(partiallySignedTx: SignedTransaction, parties: List<AbstractParty>): SignedTransaction {
      progressTracker.currentStep = GATHERING_SIGS

      val flowSessions = parties.map { initiateFlow(serviceHub.identityService.requireWellKnownPartyFromAnonymous(it)) }

      return subFlow(CollectSignaturesFlow(partiallySignedTx, flowSessions.toSet(), GATHERING_SIGS.childProgressTracker()))
    }

    @Suspendable
    fun finaliseTransaction(fullySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = FINALISING_TRANSACTION
      val finalisedTx = subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
      return finalisedTx
    }

    private fun getTermsAndConditionsState(linearId: UniqueIdentifier): StateAndRef<MeteringTermsAndConditionsState> {
      val criteria = QueryCriteria.LinearStateQueryCriteria(externalId = listOf(linearId.externalId!!))
      return serviceHub.vaultService.queryBy<MeteringTermsAndConditionsState>(criteria).states.singleOrNull()
          ?: throw FlowException("Query did not find exactly one state")
    }
  }

  @InitiatedBy(TermsAndConditionsAccepter::class)
  class TermsAndConditionsAccepterReceiver(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
      val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
        override fun checkTransaction(stx: SignedTransaction) = requireThat {
          val output = stx.tx.outputs.single().data
          "This must be an MeteringTermsAndConditions transaction." using (output is MeteringTermsAndConditionsState)
        }
      }
      return subFlow(signTransactionFlow)
    }
  }
}
