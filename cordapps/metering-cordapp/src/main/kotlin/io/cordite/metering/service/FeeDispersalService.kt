/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.CordaSerializable
import net.corda.core.serialization.SingletonSerializeAsToken
import net.corda.core.utilities.loggerFor
import net.corda.nodeapi.internal.addShutdownHook
import kotlin.concurrent.thread

/*
The Fee Dispersal services, takes the paid metering invoices and then distributes the funds
between the Metering Notaries, Guardian Notaries, and Dao Fund.
At the moment this is a very simple service that does this for each invoice
In the future this will aggregate and handle these things in bulk.
*/

@CordaService
@CordaSerializable
class FeeDispersalService(private val serviceHub: AppServiceHub) : SingletonSerializeAsToken() {

  private val log = loggerFor<FeeDispersalService>()
  private val myIdentity = serviceHub.myInfo.legalIdentities.first()
  private var running = true
  private lateinit var feeDispersalThread: Thread

  private val feeDispersalServiceConfig by lazy {
    FeeDispersalServiceConfig.loadConfig()
  }

  private val feeDisperser: FeeDisperser by lazy {
    FeeDisperser(serviceHub,feeDispersalServiceConfig)
  }

  init {
    log.info("instantiating Fee Dispersal Service $myIdentity")

    if (iShouldBeDispersingFees()) {
      log.info("starting Fee Dispersal Service $myIdentity")
      if (feeDispersalServiceConfig.daoName.isNullOrBlank()) {
        throw FeeDispersalServiceException("Missing daoName in fee dispersal service configuration")
      }
      if (feeDispersalServiceConfig.feeDispersalServicePartyName.isNullOrBlank()) {
        throw FeeDispersalServiceException("Missing feeDispersalServicePartyName in fee dispersal service configuration")
      }

      log.info("Started Fee Dispersal Service on $myIdentity")
      addShutdownHook { shutdown() }

      startFeeDispersal()
    }
  }

  private fun startFeeDispersal() {
    val feeDispersalRefreshInterval = feeDispersalServiceConfig.feeDispersalRefreshInterval

    log.info("Fee Dispersal Service Starting")

    feeDispersalThread = thread(start = true) {
      log.info("Fee Dispersal Service Running $myIdentity")
      while (running) {
        feeDisperser.disperseFeesForPaidInvoices()
        Thread.sleep(feeDispersalRefreshInterval)
      }
    }
  }

  private fun iShouldBeDispersingFees(): Boolean {
    if(feeDispersalServiceConfig.feeDispersalServicePartyName != myIdentity.name.organisation){
      log.info("Fee Dispersal Service will exit as this node is not configured to run fee dispersal config. my identity: ${ myIdentity.name.organisation}, config identity:${feeDispersalServiceConfig.feeDispersalServicePartyName}")
      return false
    }
    log.info("I have been designated as the fee disperser ${myIdentity.name.organisation}")
    return true
  }

  fun shutdown() {
    log.info("Fee Dispersal Service is now Shutting Down")
    running = false
    feeDispersalThread.join()
    log.info("Fee Dispersal Service has finished, thank the notaries for doing some awesome transaction verification")
  }
}