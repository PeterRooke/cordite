/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.contract.v1.token.TokenType
import io.cordite.metering.contract.*
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.security.PublicKey
import java.time.Instant

object DisputeMeteringInvoicesFlow {

  private val log: Logger = LoggerFactory.getLogger(DisputeMeteringInvoiceFlow.javaClass)

  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class MeteringInvoiceDisputer(val disputeMeteringInvoiceRequests: List<MeteringInvoiceFlowCommands.DisputeMeteringInvoiceRequest>) : FlowLogic<SignedTransaction>() {


    private lateinit var invoicedParty: Party
    private lateinit var invoiceNotary: Party
    private lateinit var notarisingNotary: Party
    private lateinit var daoParty: Party
    private lateinit var toAccount: String
    private lateinit var tokenType: TokenType.Descriptor

    companion object {
      object BUILD_METERING_INVOICE_TRANSACTION : Step("Build the metering invoice transaction")
      object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
      object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
      object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
        override fun childProgressTracker() = CollectSignaturesFlow.tracker()
      }
    }

    object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
      override fun childProgressTracker() = FinalityFlow.tracker()
    }

    override val progressTracker = ProgressTracker(
      BUILD_METERING_INVOICE_TRANSACTION,
      VERIFYING_TRANSACTION,
      SIGNING_TRANSACTION,
      GATHERING_SIGS,
      FINALISING_TRANSACTION
    )

    @Suspendable
    override fun call(): SignedTransaction {

      checkAndExtractInvoiceAttributes()
      val txBuilder = buildTransaction()
      val partiallySignedTx = verifyAndSignTransaction(txBuilder)
      val signatureParties = getSignatureParties()
      val fullySignedTx = getSignatureFromReceiver(partiallySignedTx,signatureParties.toList())
      val finalisedTx = finaliseTransaction(fullySignedTx)
      return finalisedTx
    }

    fun checkAndExtractInvoiceAttributes() {

      val checker = MeteringInvoiceConsistencyChecker(originalInvoices = FlowUtils.getOriginalMeteringInvoices(serviceHub, disputeMeteringInvoiceRequests.map { it.meteredTransactionId }))

      invoicedParty = checker.getInvoicedParty()
      invoiceNotary = checker.getInvoiceNotary()
      notarisingNotary = checker.getNotarisingNotary()
      daoParty = checker.getDaoParty()
      toAccount = checker.getPayAccount()
      tokenType = checker.getTokenType()
    }

    fun buildTransaction(): TransactionBuilder {
      progressTracker.currentStep = BUILD_METERING_INVOICE_TRANSACTION
      var txBuilder = TransactionBuilder(notarisingNotary)

      val signers = mutableListOf<PublicKey>()

      disputeMeteringInvoiceRequests.forEach {

        val originalMeteringInvoiceStateAndRef = FlowUtils.getOriginalMeteringInvoice(serviceHub, it.meteredTransactionId)
        val originalMeteringInvoiceProperties = originalMeteringInvoiceStateAndRef.state.data.meteringInvoiceProperties

        val meteringInvoiceProperties = MeteringInvoiceProperties(
            meteringState = MeteringState.IN_DISPUTE,
            invoicedParty = originalMeteringInvoiceProperties.invoicedParty,
            invoicingNotary = originalMeteringInvoiceProperties.invoicingNotary,
            daoParty = originalMeteringInvoiceProperties.daoParty,
            payAccountId = originalMeteringInvoiceProperties.payAccountId,
            amount = originalMeteringInvoiceProperties.amount,
            tokenDescriptor = originalMeteringInvoiceProperties.tokenDescriptor,
            meteredTransactionId = originalMeteringInvoiceProperties.meteredTransactionId,
            invoiceId = originalMeteringInvoiceProperties.invoiceId,
            reissueCount = originalMeteringInvoiceProperties.reissueCount,
            createdDateTime = Instant.now())

        val inputState = originalMeteringInvoiceStateAndRef
        txBuilder.addInputState(inputState)

        val meteringInvoiceStateToIssue = MeteringInvoiceState(meteringInvoiceProperties, invoiceNotary)
        val outputState = StateAndContract(meteringInvoiceStateToIssue, MeteringInvoiceContract.METERING_CONTRACT_ID)
        txBuilder.withItems(outputState)

        signers.addAll(meteringInvoiceStateToIssue.participants.map { it.owningKey })
      }

      val txCommand = Command(MeteringInvoiceCommands.Dispute(), signers.distinct())
      txBuilder.addCommand(txCommand)

      return txBuilder
    }


    // return the union set of DAOs and notaries appearing on the supplied invoices
    fun getSignatureParties() : Set<Party>{

      val signatureParties = mutableSetOf<Party>()

      disputeMeteringInvoiceRequests.forEach {
        val originalMeteringInvoiceStateAndRef = FlowUtils.getOriginalMeteringInvoice(serviceHub, it.meteredTransactionId)
        val originalMeteringInvoiceProperties = originalMeteringInvoiceStateAndRef.state.data.meteringInvoiceProperties
        signatureParties.addAll(setOf(originalMeteringInvoiceProperties.daoParty, originalMeteringInvoiceProperties.invoicingNotary))
      }
      return signatureParties
    }

    @Suspendable
    fun verifyAndSignTransaction(txBuilder: TransactionBuilder): SignedTransaction {
      progressTracker.currentStep = VERIFYING_TRANSACTION
      txBuilder.verify(serviceHub)
      progressTracker.currentStep = SIGNING_TRANSACTION
      val partiallySignedTx = serviceHub.signInitialTransaction(txBuilder)
      return partiallySignedTx
    }

    @Suspendable
    fun getSignatureFromReceiver(partiallySignedTx: SignedTransaction, parties : List<Party>): SignedTransaction {
      progressTracker.currentStep = GATHERING_SIGS

      val flowSessions = parties.map { initiateFlow(it) }

      val fullySignedTx = subFlow(CollectSignaturesFlow(partiallySignedTx, flowSessions.toSet(), GATHERING_SIGS.childProgressTracker()))
      return fullySignedTx
    }

    @Suspendable
    fun finaliseTransaction(fullySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = FINALISING_TRANSACTION
      val finalisedTx = subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
      return finalisedTx
    }
  }

  @InitiatedBy(MeteringInvoiceDisputer::class)
  class MeteringInvoiceDisputee(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
      val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
        override fun checkTransaction(stx: SignedTransaction) = requireThat {

          // for each output invoice state in the transaction, check the validity of the transaction
          stx.tx.outputs.filter { it.data is MeteringInvoiceState }.forEach {
            val output = it.data
            val meteringInvoiceState = output as MeteringInvoiceState
            "check that this this is a valid transaction we are being metered for" using (MeteringInvoiceFlowChecks.isTheMeteredTransactionValid(meteringInvoiceState, serviceHub, log))
          }

        }
      }
      return subFlow(signTransactionFlow)
    }
  }
}
