/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow


import io.cordite.dgl.contract.v1.token.TokenType
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

object MeteringInvoiceFlowCommands {
  @CordaSerializable
  data class IssueMeteringInvoiceRequest(val meteredTransactionId: String,
                                         val tokenDescriptor: TokenType.Descriptor,
                                         val amount: Int,
                                         val invoicedParty: Party,
                                         val invoicingNotary: Party,
                                         val daoParty: Party,
                                         val meteringPolicingNotary: Party,
                                         val payAccountId: String)
  @CordaSerializable
  data class ReIssueMeteringInvoiceRequest(val meteredTransactionId: String,
                                           val tokenDescriptor: TokenType.Descriptor,
                                           val amount: Int)
  @CordaSerializable
  data class DisputeMeteringInvoiceRequest(val meteredTransactionId: String)

  @CordaSerializable
  data class PayMeteringInvoiceRequest(val meteredTransactionId: String, val fromAccount: String)

  @CordaSerializable
  data class DisperseFundsForMeteringInvoiceRequest(val meteredTransactionId: String)
}
