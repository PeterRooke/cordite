/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import io.cordite.dgl.contract.v1.token.TokenType
import io.cordite.metering.contract.MeteringPerTransactionBillingType
import io.cordite.metering.contract.MeteringTermsAndConditionsProperties
import io.cordite.metering.contract.MeteringTermsAndConditionsState
import io.cordite.metering.contract.MeteringTermsAndConditionsStatus
import io.cordite.metering.flow.IssueMeteringTermsAndConditionsFlow
import io.cordite.metering.schema.MeteringTermsAndConditionsSchemaV1
import net.corda.core.concurrent.CordaFuture
import net.corda.core.contracts.StateAndRef
import net.corda.core.identity.Party
import net.corda.core.internal.concurrent.asCordaFuture
import net.corda.core.internal.concurrent.map
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.utilities.loggerFor
import java.time.Instant
import java.util.concurrent.CompletableFuture.completedFuture

class MeteringTermsAndConditionsLoader(private val serviceHub: AppServiceHub, private val termsAndConditionsDefaults: MeteringTermsAndConditionsDefaults) {

  private val log = loggerFor<MeteringTermsAndConditionsLoader>()

  init {
    log.info("Metering Terms And Conditions State Loader Created")
  }

  fun getTermsAndConditions(meteredParty: Party): StateAndRef<MeteringTermsAndConditionsState>? {
    if (meteredParty in serviceHub.myInfo.legalIdentities) {
      throw IllegalArgumentException("Cannot create terms and conditions if we are the metered party")
    }

    log.debug("trying to get terms and conditions for '$meteredParty'")
    return builder {
      val partyCriteria = MeteringTermsAndConditionsSchemaV1.PersistentMeteringTermsAndConditions::meteredParty.equal(meteredParty.name.toString())
      val customCriteria = QueryCriteria.VaultCustomQueryCriteria(partyCriteria)
      val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED)
      val fullCriteria = generalCriteria.and(customCriteria)
      serviceHub.vaultService.queryBy<MeteringTermsAndConditionsState>(fullCriteria).states
    }.map { it }.singleOrNull()
  }

  // Returns a CordaFuture mostly to let tests work but also it's good practice.
  fun getOrCreateTermsAndConditions(meteredParty: Party): CordaFuture<StateAndRef<MeteringTermsAndConditionsState>> {
    val tAndCState = getTermsAndConditions(meteredParty)

    return if(tAndCState != null){
      log.info("Terms and conditions for $meteredParty: $tAndCState")
      completedFuture(tAndCState).asCordaFuture()
    } else {
      if (serviceHub.identityService.wellKnownPartyFromX500Name(termsAndConditionsDefaults.guardianNotaryName) == null) {
        throw IllegalArgumentException("Couldn't find guardian notary.")
      } else if (serviceHub.identityService.wellKnownPartyFromX500Name(termsAndConditionsDefaults.payPartyName) == null) {
        throw IllegalArgumentException("Couldn't find pay party.")
      }

      val defaultTermsAndConditions = if (meteredParty.name in termsAndConditionsDefaults.authorizedForZeroTC) {
        makeDefaultNotaryTermsAndConditions(termsAndConditionsDefaults, meteredParty)
      } else {
        makeDefaultTermsAndConditions(termsAndConditionsDefaults, meteredParty)
      }

      return if (meteredParty.name in termsAndConditionsDefaults.authorizedForZeroTC && defaultTermsAndConditions.meteredParty == defaultTermsAndConditions.guardianNotaryParty) {
        val flowOut = serviceHub.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(defaultTermsAndConditions, doNotSend = true))
        flowOut.returnValue.map { it.coreTransaction.outRef<MeteringTermsAndConditionsState>(0) }
      } else {
        val flowOut = serviceHub.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(defaultTermsAndConditions))
        flowOut.returnValue.map { it.coreTransaction.outRef<MeteringTermsAndConditionsState>(0) }
      }
    }
  }

  private fun makeDefaultNotaryTermsAndConditions(config: MeteringTermsAndConditionsDefaults, meteredParty: Party): MeteringTermsAndConditionsProperties {
    return MeteringTermsAndConditionsProperties(
        meteringParty = serviceHub.myInfo.legalIdentities.first(),
        meteredParty = meteredParty,
        billingToken = TokenType.Descriptor.parse(config.billingTokenTypeUri),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = config.transactionCostForNotaries,
            transactionCreditLimit = config.transactionCreditLimitForNotaries,
            freeTransactions = config.freeTransactionsForNotaries
        ),
        payParty = serviceHub.identityService.wellKnownPartyFromX500Name(config.payPartyName)!!,
        guardianNotaryParty = serviceHub.identityService.wellKnownPartyFromX500Name(config.guardianNotaryName)!!,
        payAccountId = config.payAccount,
        createdDateTime = Instant.now(),
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
  }

  private fun makeDefaultTermsAndConditions(config: MeteringTermsAndConditionsDefaults, meteredParty: Party): MeteringTermsAndConditionsProperties {
    return MeteringTermsAndConditionsProperties(
        meteringParty = serviceHub.myInfo.legalIdentities.first(),
        meteredParty = meteredParty,
        billingToken = TokenType.Descriptor.parse(config.billingTokenTypeUri),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = config.transactionCost,
            transactionCreditLimit = config.transactionCreditLimit,
            freeTransactions = config.freeTransactions
        ),
        payParty = serviceHub.identityService.wellKnownPartyFromX500Name(config.payPartyName)!!,
        guardianNotaryParty = serviceHub.identityService.wellKnownPartyFromX500Name(config.guardianNotaryName)!!,
        payAccountId = config.payAccount,
        createdDateTime = Instant.now(),
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
  }
}