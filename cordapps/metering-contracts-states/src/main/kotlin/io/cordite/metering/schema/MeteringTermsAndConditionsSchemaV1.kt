/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.schema

import io.cordite.metering.contract.MeteringTermsAndConditionsStatus
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Index
import javax.persistence.Table

object MeteringTermsAndConditionsSchema

object MeteringTermsAndConditionsSchemaV1 : MappedSchema(schemaFamily = MeteringTermsAndConditionsSchema.javaClass, version = 1, mappedTypes = listOf(PersistentMeteringTermsAndConditions::class.java)) {

  @Entity
  @Table(name = "Cordite_Metering_Terms_And_Conditions",
      indexes = arrayOf(Index(name = "cordite_metering_metering_terms_and_conditions_id", columnList = "metering_terms_and_conditions_id", unique = false)))
  class PersistentMeteringTermsAndConditions(
    @Column(name = "metering_terms_and_conditions_id", nullable = false)
    var meteringTermsAndConditionsId: String,
    @Column(name = "metering_party", nullable = false)
    var meteringParty: String,
    @Column(name = "metered_parties", nullable = false)
    var meteredParty: String,
    @Column(name = "billing_token", nullable = false)
    var billingToken: String,
    @Column(name = "billing_type", nullable = false)
    var billingType: String, //TODO this is a place holder we need a KV sub table for this (probably)
    @Column(name = "pay_party", nullable = false)
    var payParty: String,
    @Column(name = "pay_account_id", nullable = false)
    var payAccountId: String,
    @Column(name = "guardian_notary_party", nullable = false)
    var guardianNotaryParty: String,
    @Column(name = "created_date_time", nullable = false)
    var createdDateTime: Instant?,
    @Column(name = "metering_terms_and_conditions_status", nullable = false)
    var meteringTermsAndConditionsStatus: MeteringTermsAndConditionsStatus
  ) : PersistentState() {
    constructor() : this("", "", "", "", "", "", "", "", null, MeteringTermsAndConditionsStatus.PROPOSED)
  }
}

/** TODO add subtable for billing type
@CordaSerializable
data class MeteringPerTransactionBillingType : MeteringBillingType (
val transactionCost : Amount<TokenType.Descriptor>,
val transactionCreditLimit : Int,
val freeTransactions : Int)
 */

