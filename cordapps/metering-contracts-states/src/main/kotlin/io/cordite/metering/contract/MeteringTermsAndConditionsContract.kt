/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.contract

import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class MeteringTermsAndConditionsContract : Contract {

  companion object {
    val METERING_T_AND_C_CONTRACT_ID: ContractClassName = MeteringTermsAndConditionsContract::class.java.canonicalName
  }

  override fun verify(tx: LedgerTransaction) {
    val command = tx.commands.requireSingleCommand<MeteringTermsAndConditionsCommands>()

    val otherInputs: List<ContractState> = tx.filterInputs { it !is MeteringTermsAndConditionsState }
    val otherOutputs: List<ContractState> = tx.filterOutputs { it !is MeteringTermsAndConditionsState }
    requireThat {
      "there should only be metering terms and conditions states in this transaction's inputs" using (otherInputs.count() == 0)
      "there should only be metering terms and conditions states in this transaction's outputs" using (otherOutputs.count() == 0)
    }

    val meteringTermsAndConditionsStateGroup = tx.groupStates { it: MeteringTermsAndConditionsState -> it.meteringTermsAndConditionsProperties.meteringTermsAndConditionsId }

    for ((inputs, outputs, _) in meteringTermsAndConditionsStateGroup) {
      when (command.value) {
        is MeteringTermsAndConditionsCommands.Issue -> {
          requireThat {
            "there should be no inputs when issuing" using (inputs.count() == 0)
            "there should be one output when issuing" using (outputs.count() == 1)

            val meteringTermsAndConditionsState = outputs.single()
            "the output status must be issued" using (meteringTermsAndConditionsState.meteringTermsAndConditionsProperties.status == MeteringTermsAndConditionsStatus.ISSUED)
            checkOwnerIsANegotiatingParty(meteringTermsAndConditionsState)
            "all signers must sign" using (command.signingParties.toSet().subtract(meteringTermsAndConditionsState.participants).isEmpty())
          }
        }
        is MeteringTermsAndConditionsCommands.Propose -> {
          requireThat {
            "there should be one input when proposing" using (inputs.count() == 1)
            "there should be one output when proposing" using (outputs.count() == 1)

            val inputMeteringTermsAndConditionsState = inputs.single()
            val outputMeteringTermsAndConditionsState = outputs.single()

            checkAllFieldsAreTheSameExceptTheStateAndBillingType(inputMeteringTermsAndConditionsState, outputMeteringTermsAndConditionsState)

            "the input status must be issued" using (inputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties.status == MeteringTermsAndConditionsStatus.ISSUED)
            "the output status must be proposed" using (outputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties.status == MeteringTermsAndConditionsStatus.PROPOSED)

            checkOwnerIsANegotiatingParty(inputMeteringTermsAndConditionsState)
            checkOwnerIsANegotiatingParty(outputMeteringTermsAndConditionsState)
            "all signers must sign" using (command.signingParties.toSet().subtract(outputMeteringTermsAndConditionsState.participants).isEmpty())
          }
        }
        is MeteringTermsAndConditionsCommands.Accept -> {
          requireThat {
            "there should be one input when accepting" using (inputs.count() == 1)
            "there should be one output when accepting" using (outputs.count() == 1)

            val inputMeteringTermsAndConditionsState = inputs.single()
            val outputMeteringTermsAndConditionsState = outputs.single()

            checkAllFieldsAreTheSameExceptTheState(inputMeteringTermsAndConditionsState, outputMeteringTermsAndConditionsState)

            "the input status must be proposed" using (inputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties.status == MeteringTermsAndConditionsStatus.PROPOSED)
            "the output status must be accepted" using (outputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties.status == MeteringTermsAndConditionsStatus.ACCEPTED)

            checkOwnerIsANegotiatingParty(inputMeteringTermsAndConditionsState)
            checkOwnerIsANegotiatingParty(outputMeteringTermsAndConditionsState)
            "all signers must sign" using (command.signingParties.toSet().subtract(outputMeteringTermsAndConditionsState.participants).isEmpty())
          }
        }
        is MeteringTermsAndConditionsCommands.Reject -> {
          requireThat {
            "there should be one input when rejecting" using (inputs.count() == 1)
            "there should be one output when rejecting" using (outputs.count() == 1)
            val inputMeteringTermsAndConditionsState = inputs.single()
            val outputMeteringTermsAndConditionsState = outputs.single()

            checkAllFieldsAreTheSameExceptTheState(inputMeteringTermsAndConditionsState, outputMeteringTermsAndConditionsState)
            "the input status must be proposed" using (inputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties.status == MeteringTermsAndConditionsStatus.PROPOSED)
            "the output status must be rejected" using (outputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties.status == MeteringTermsAndConditionsStatus.REJECTED)

            checkOwnerIsANegotiatingParty(inputMeteringTermsAndConditionsState)
            checkOwnerIsANegotiatingParty(outputMeteringTermsAndConditionsState)

            "all signers must sign" using (command.signingParties.toSet().subtract(outputMeteringTermsAndConditionsState.participants).isEmpty())
          }
        }
      }
    }
  }

  private fun checkAllFieldsAreTheSameExceptTheState(inputMeteringTermsAndConditionsState: MeteringTermsAndConditionsState, outputMeteringTermsAndConditionsState: MeteringTermsAndConditionsState) {
    val input = inputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties
    val output = outputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties

    requireThat {
      "billing token must remain the same" using (input.billingToken == output.billingToken)
      "billing type must remain the same" using (input.billingType == output.billingType)
      "created date time remain the same" using (input.createdDateTime == output.createdDateTime)
      "guardian notary party must remain the same" using (input.guardianNotaryParty == output.guardianNotaryParty)
      "metered party must remain the same" using (input.meteredParty == output.meteredParty)
      "metering notary party must remain the same" using (input.meteringParty == output.meteringParty)
      "metering T&C id must remain the same" using (input.meteringTermsAndConditionsId == output.meteringTermsAndConditionsId)
      "pay account id must remain the same" using (input.payAccountId == output.payAccountId)
      "pay party must remain the same" using (input.payParty == output.payParty)
    }
  }

  private fun checkAllFieldsAreTheSameExceptTheStateAndBillingType(inputMeteringTermsAndConditionsState: MeteringTermsAndConditionsState, outputMeteringTermsAndConditionsState: MeteringTermsAndConditionsState) {
    val input = inputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties
    val output = outputMeteringTermsAndConditionsState.meteringTermsAndConditionsProperties

    requireThat {
      "billing token must remain the same" using (input.billingToken == output.billingToken)
      "created date time remain the same" using (input.createdDateTime == output.createdDateTime)
      "guardian notary party must remain the same" using (input.guardianNotaryParty == output.guardianNotaryParty)
      "metered party must remain the same" using (input.meteredParty == output.meteredParty)
      "metering notary party must remain the same" using (input.meteringParty == output.meteringParty)
      "metering T&C id must remain the same" using (input.meteringTermsAndConditionsId == output.meteringTermsAndConditionsId)
      "pay account id must remain the same" using (input.payAccountId == output.payAccountId)
      "pay party must remain the same" using (input.payParty == output.payParty)
    }
  }

  private fun checkOwnerIsANegotiatingParty(meteringTermsAndConditionsState: MeteringTermsAndConditionsState) {
    val properties = meteringTermsAndConditionsState.meteringTermsAndConditionsProperties
    val parties = setOf(properties.meteredParty, properties.meteringParty).filterNotNull()

    requireThat {
      "owner must be one of [meteredParty, meteringParty]" using (meteringTermsAndConditionsState.owner in parties)
    }
  }
}



