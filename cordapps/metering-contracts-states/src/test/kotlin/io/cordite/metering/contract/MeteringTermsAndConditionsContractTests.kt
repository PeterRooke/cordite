/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.contract

import io.cordite.metering.tesutils.TestUtils.Companion.TestTokenDescriptor
import io.cordite.test.utils.ledgerServices
import net.corda.core.crypto.generateKeyPair
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.testing.contracts.DummyState
import net.corda.testing.node.ledger
import org.junit.Ignore
import org.junit.Test
import java.time.Instant

@Ignore
class MeteringTermsAndConditionsContractTests {

  private val corditeSocietyKeys = generateKeyPair()
  private val corditeSocietyParty = Party(CordaX500Name("Cordite Society Limited", "Cordite Society Limited", "Ableton", "GB"), corditeSocietyKeys.public)

  private val meteringNotaryKeys = generateKeyPair()
  private val meteringNotaryParty = Party(CordaX500Name("MeteringNotary", "MeteringNotary", "Sector 1", "GB"), meteringNotaryKeys.public)

  private val bogusMeteringNotaryKeys = generateKeyPair()
  private val bogusMeteringNotaryParty = Party(CordaX500Name("Bogus MeteringNotary", "Bogus MeteringNotary", "Sector 1", "GB"), bogusMeteringNotaryKeys.public)

  private val bogusMeteredPartyKeys = generateKeyPair()
  private val bogusMeteredParty = Party(CordaX500Name("Non Non Bogus Metered Party", "Non Non Bogus Metered Party", "Sector 1", "GB"), bogusMeteredPartyKeys.public)

  private val bogusPayPartyKeys = generateKeyPair()
  private val bogusPayParty = Party(CordaX500Name("Bill and Ted are not dead", "Bill and Ted are not dead", "the matrix", "GB"), bogusPayPartyKeys.public)

  private val corditeBusinessNodeKeys = generateKeyPair()
  private val corditeBusinessNodeParty = Party(CordaX500Name("Flourish and Blotts", "Florish and Blotts", "Sector 1", "GB"), corditeBusinessNodeKeys.public)

  private val guardianNotaryKeys = generateKeyPair()
  private val guardianNotaryParty = Party(CordaX500Name("Guardian Notary", "Guardian Notary", "Sector 2", "GB"), guardianNotaryKeys.public)

  private val bogusGuardianNotaryKeys = generateKeyPair()
  private val bogusGuardianNotaryParty = Party(CordaX500Name("Bogus Guardian Notary", "Bogus Guardian Notary", "Sector 2", "GB"), bogusGuardianNotaryKeys.public)

  private val payAccountName = "metering-and-csl-fund"

  private val testTokenType = TestTokenDescriptor(corditeSocietyParty)

  private val meteringPerTransactionBillingType = MeteringPerTransactionBillingType(
      freeTransactions = 10,
      transactionCost = 100,
      transactionCreditLimit = 20
  )

  private val issuedMeteringTermsAndConditionsProperties1 = MeteringTermsAndConditionsProperties(
      meteringTermsAndConditionsId = "ID1",
      billingToken = testTokenType,
      billingType = meteringPerTransactionBillingType,
      createdDateTime = Instant.now(),
      guardianNotaryParty = guardianNotaryParty,
      meteredParty = corditeBusinessNodeParty,
      meteringParty = meteringNotaryParty,
      payAccountId = payAccountName,
      payParty = meteringNotaryParty,
      status = MeteringTermsAndConditionsStatus.ISSUED)
  private val issuedMeteringTermsAndConditionsProperties2 = issuedMeteringTermsAndConditionsProperties1.copy(
      payAccountId = "not bogus"
  )
  private val proposedMeteringTermsAndConditionProperties1 = issuedMeteringTermsAndConditionsProperties1.copy(
      status = MeteringTermsAndConditionsStatus.PROPOSED
  )
  private val dodgyProposedMeteringTermsAndConditionProperties1 = issuedMeteringTermsAndConditionsProperties1.copy(
      status = MeteringTermsAndConditionsStatus.PROPOSED,
      payAccountId = "bogus!"
  )

  private val issuedMeteringTermsAndConditionState1 = MeteringTermsAndConditionsState(issuedMeteringTermsAndConditionsProperties1, corditeBusinessNodeParty)
  private val proposedMeteringTermsAndConditionsState1 = MeteringTermsAndConditionsState(proposedMeteringTermsAndConditionProperties1, corditeBusinessNodeParty)
  private val dodgyProposedMeteringTermsAndConditionsState1 = MeteringTermsAndConditionsState(dodgyProposedMeteringTermsAndConditionProperties1, corditeBusinessNodeParty)

  private val issuedMeteringTermsAndConditionState2 = MeteringTermsAndConditionsState(issuedMeteringTermsAndConditionsProperties2, corditeBusinessNodeParty)

  private val dodgyIssuedMeteringTermsAndConditionsProperties = issuedMeteringTermsAndConditionsProperties1.copy(
      status = MeteringTermsAndConditionsStatus.REJECTED)

  private val dodgyIssuedMeteringTermsAndConditionsState = MeteringTermsAndConditionsState(dodgyIssuedMeteringTermsAndConditionsProperties, corditeBusinessNodeParty)

  private val acceptedMeteringTermsAndConditionsProperties1 = issuedMeteringTermsAndConditionsProperties1.copy(
      status = MeteringTermsAndConditionsStatus.ACCEPTED)

  private val acceptedMeteringTermsAndConditionState1 = MeteringTermsAndConditionsState(acceptedMeteringTermsAndConditionsProperties1, meteringNotaryParty)

  private val rejectedMeteringTermsAndConditionsProperties1 = acceptedMeteringTermsAndConditionsProperties1.copy(
      status = MeteringTermsAndConditionsStatus.REJECTED)

  private val rejectedMeteringTermsAndConditionState1 = MeteringTermsAndConditionsState(rejectedMeteringTermsAndConditionsProperties1, meteringNotaryParty)

  private val stolenIssuedMeteringTermsAndConditionState = MeteringTermsAndConditionsState(issuedMeteringTermsAndConditionsProperties1, bogusMeteredParty)
  private val stolenAcceptedMeteringTermsAndConditionState1 = acceptedMeteringTermsAndConditionState1.copy(
      owner = bogusMeteredParty)
  private val stolenRejectedMeteringTermsAndConditionState1 = rejectedMeteringTermsAndConditionState1.copy(
      owner = bogusMeteredParty)

  @Test
  fun `you can issue metering terms and conditions`() {
    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }
    }
  }

  @Test
  fun `you can't issue metering terms and conditions with the wrong state`() {
    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, dodgyIssuedMeteringTermsAndConditionsState)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.fails()
      }
    }
  }

  @Test
  fun `you can propose metering terms and conditions`() {
    val issuedInputLabel = "issuedT&C1"

    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, proposedMeteringTermsAndConditionsState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.verifies()
      }
    }
  }

  @Test
  fun `you can't propose metering terms and conditions without an input`() {
    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, proposedMeteringTermsAndConditionsState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.failsWith("there should be one input when proposing")
      }
    }
  }

  @Test
  fun `you can't propose metering terms and conditions without an output`() {
    val issuedInputLabel = "issuedT&C1"

    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.failsWith("there should be one output when proposing")
      }
    }
  }

  @Test
  fun `you can't propose metering terms and conditions with multiple outputs`() {
    val issuedInputLabel = "issuedT&C1"

    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, proposedMeteringTermsAndConditionsState1)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, proposedMeteringTermsAndConditionsState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.failsWith("there should be one output when proposing")
      }
    }
  }

  @Test
  fun `you can't propose metering terms and conditions with multiple inputs`() {
    val issuedInputLabel1 = "issuedT&C1"
    val issuedInputLabel2 = "issuedT&C2"

    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel1, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel2, issuedMeteringTermsAndConditionState2)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel1)
        input(issuedInputLabel2)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, proposedMeteringTermsAndConditionsState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.failsWith("there should be one input when proposing")
      }
    }
  }

  @Test
  fun `you can't propose metering terms and conditions with an output that isn't a proposal`() {
    val issuedInputLabel = "issuedT&C1"

    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.failsWith("the output status must be proposed")
      }
    }
  }

  @Test
  fun `you can't propose metering terms and conditions with an output that isn't the same as the input`() {
    val issuedInputLabel = "issuedT&C1"

    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, dodgyProposedMeteringTermsAndConditionsState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.failsWith("pay account id must remain the same")
      }
    }
  }

  @Test
  fun `you can't propose metering terms and conditions with an input that isn't an issuance`() {
    val issuedInputLabel = "issuedT&C1"
    val proposedInputLabel = "proposedT&C1"
    val acceptedInputLabel = "acceptedT&C1"

    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, proposedInputLabel, proposedMeteringTermsAndConditionsState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(proposedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, acceptedInputLabel, acceptedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Accept())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(acceptedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, proposedMeteringTermsAndConditionsState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.failsWith("the input status must be issued")
      }
    }
  }

  @Test
  fun `you can accept metering terms and conditions`() {
    ledgerServices.ledger {
      val issuedInputLabel = "issuedT&C1"
      val proposedInputLabel = "proposedT&C1"

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, proposedInputLabel, proposedMeteringTermsAndConditionsState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(proposedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, acceptedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Accept())
        this.verifies()
      }
    }
  }

  @Test
  fun `you can reject metering terms and conditions if you feel like it`() {
    ledgerServices.ledger {
      val issuedInputLabel = "issuedT&C1"
      val proposedInputLabel = "proposedT&C1"

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, proposedInputLabel, proposedMeteringTermsAndConditionsState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Propose())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(proposedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, rejectedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Reject())
        this.verifies()
      }
    }
  }

  @Test
  fun `you cant change the billing type when accepting or rejecting terms and conditions`() {
    val differentMeteringPerTransactionBillingType = MeteringPerTransactionBillingType(
        freeTransactions = 8,
        transactionCost = 23,
        transactionCreditLimit = 42)

    val billingTypeChangedAcceptTanC = issuedMeteringTermsAndConditionsProperties1.copy(
        billingType = differentMeteringPerTransactionBillingType,
        status = MeteringTermsAndConditionsStatus.ACCEPTED
    )
    val billingTypeChangedAcceptState = MeteringTermsAndConditionsState(billingTypeChangedAcceptTanC, meteringNotaryParty)

    val billingTypeChangedRejectTanC = issuedMeteringTermsAndConditionsProperties1.copy(
        billingType = differentMeteringPerTransactionBillingType,
        status = MeteringTermsAndConditionsStatus.REJECTED
    )
    val billingTypeChangedRejectState = MeteringTermsAndConditionsState(billingTypeChangedRejectTanC, meteringNotaryParty)

    val failureMessage = "Contract verification failed: Failed requirement: billing type must remain the same"

    checkThatChangingFieldsFails(billingTypeChangedAcceptState, billingTypeChangedRejectState, failureMessage)
  }

  @Test
  fun `you cant change the id  when accepting or rejecting terms and conditions`() {
    val idChangedAcceptTandC = issuedMeteringTermsAndConditionsProperties1.copy(
        meteringTermsAndConditionsId = "ID2",
        status = MeteringTermsAndConditionsStatus.ACCEPTED)

    val idChangedAcceptState = MeteringTermsAndConditionsState(idChangedAcceptTandC, meteringNotaryParty)

    val idChangedRejectTandC = idChangedAcceptTandC.copy(
        status = MeteringTermsAndConditionsStatus.REJECTED)
    val idChangedRejectState = MeteringTermsAndConditionsState(idChangedRejectTandC, meteringNotaryParty)

    //Note: IDs are grouped in the contract so the test fails on inputs/outputs
    val failureMessage = "Contract verification failed: Failed requirement: there should be one output"

    checkThatChangingFieldsFails(idChangedAcceptState, idChangedRejectState, failureMessage)
  }

  @Test
  fun `you cant change the billing token when accepting or rejecting terms and conditions`() {
    val changedTestTokenType = TestTokenDescriptor(meteringNotaryParty)

    val tokenTypeChangedAcceptTandC = issuedMeteringTermsAndConditionsProperties1.copy(
        billingToken = changedTestTokenType,
        status = MeteringTermsAndConditionsStatus.ACCEPTED)

    val tokenTypeChangedAcceptState = MeteringTermsAndConditionsState(tokenTypeChangedAcceptTandC, meteringNotaryParty)

    val tokenTypeChangedRejectTandC = tokenTypeChangedAcceptTandC.copy(
        status = MeteringTermsAndConditionsStatus.REJECTED)

    val tokenTypeChangedRejectState = MeteringTermsAndConditionsState(tokenTypeChangedRejectTandC, meteringNotaryParty)

    val failureMessage = "Contract verification failed: Failed requirement: billing token must remain the same"

    checkThatChangingFieldsFails(tokenTypeChangedAcceptState, tokenTypeChangedRejectState, failureMessage)
  }

  @Test
  fun `you cant change the created date time when accepting or rejecting terms and conditions`() {
    val changedDateTime = Instant.MAX

    val dateTimeChangedAcceptTandC = issuedMeteringTermsAndConditionsProperties1.copy(
        createdDateTime = changedDateTime,
        status = MeteringTermsAndConditionsStatus.ACCEPTED)
    val dateTimeChangedAcceptState = MeteringTermsAndConditionsState(dateTimeChangedAcceptTandC, meteringNotaryParty)

    val dateTimeChangedRejectTandC = dateTimeChangedAcceptTandC.copy(
        status = MeteringTermsAndConditionsStatus.REJECTED)

    val dateTimeChangedRejectState = MeteringTermsAndConditionsState(dateTimeChangedRejectTandC, meteringNotaryParty)

    val failureMessage = "Contract verification failed: Failed requirement: created date time remain the same"

    checkThatChangingFieldsFails(dateTimeChangedAcceptState, dateTimeChangedRejectState, failureMessage)
  }

  @Test
  fun `you cant change the guardian notary party when accepting or rejecting terms and conditions`() {
    val changedGuardian = bogusGuardianNotaryParty

    val guardianChangedAcceptTandC = issuedMeteringTermsAndConditionsProperties1.copy(
        guardianNotaryParty = changedGuardian,
        status = MeteringTermsAndConditionsStatus.ACCEPTED)
    val guardianChangedAcceptState = MeteringTermsAndConditionsState(guardianChangedAcceptTandC, meteringNotaryParty)

    val guardianChangedRejectTandC = guardianChangedAcceptTandC.copy(
        status = MeteringTermsAndConditionsStatus.REJECTED)

    val guardianChangedRejectState = MeteringTermsAndConditionsState(guardianChangedRejectTandC, meteringNotaryParty)

    val failureMessage = "Contract verification failed: Failed requirement: guardian notary party must remain the same"

    checkThatChangingFieldsFails(guardianChangedAcceptState, guardianChangedRejectState, failureMessage)
  }

  @Test
  fun `you cant change the metered party when accepting or rejecting terms and conditions`() {
    val changedMeteredParty = bogusMeteredParty

    val meteredPartyChangedAcceptTandC = issuedMeteringTermsAndConditionsProperties1.copy(
        meteredParty = changedMeteredParty,
        status = MeteringTermsAndConditionsStatus.ACCEPTED)
    val meteredNotaryChangedAcceptState = MeteringTermsAndConditionsState(meteredPartyChangedAcceptTandC, meteringNotaryParty)

    val meteredPartyChangedRejectTandC = meteredPartyChangedAcceptTandC.copy(
        status = MeteringTermsAndConditionsStatus.REJECTED)

    val meteredPartyChangedRejectState = MeteringTermsAndConditionsState(meteredPartyChangedRejectTandC, meteringNotaryParty)

    val failureMessage = "Contract verification failed: Failed requirement: metered party must remain the same, contract"

    checkThatChangingFieldsFails(meteredNotaryChangedAcceptState, meteredPartyChangedRejectState, failureMessage)
  }

  @Test
  fun `you cant change the metering party when accepting or rejecting terms and conditions`() {
    val changedMeteringNotary = bogusMeteringNotaryParty

    val meteringNotaryChangedAcceptTandC = issuedMeteringTermsAndConditionsProperties1.copy(
        meteringParty = changedMeteringNotary,
        status = MeteringTermsAndConditionsStatus.ACCEPTED)
    val meteringNotaryChangedAcceptState = MeteringTermsAndConditionsState(meteringNotaryChangedAcceptTandC, meteringNotaryParty)

    val meteringNotaryChangedRejectTandC = meteringNotaryChangedAcceptTandC.copy(
        status = MeteringTermsAndConditionsStatus.REJECTED)

    val meteringNotaryChangedRejectState = MeteringTermsAndConditionsState(meteringNotaryChangedRejectTandC, meteringNotaryParty)

    val failureMessage = "Contract verification failed: Failed requirement: metering notary party must remain the same"

    checkThatChangingFieldsFails(meteringNotaryChangedAcceptState, meteringNotaryChangedRejectState, failureMessage)
  }

  @Test
  fun `you cant change the pay account id when accepting or rejecting terms and conditions`() {
    val changedPayAccountId = "dont-pay-me-here"

    val payAccountIdChangedAcceptTandC = issuedMeteringTermsAndConditionsProperties1.copy(
        payAccountId = changedPayAccountId,
        status = MeteringTermsAndConditionsStatus.ACCEPTED)
    val payAccountIdChangedAcceptState = MeteringTermsAndConditionsState(payAccountIdChangedAcceptTandC, meteringNotaryParty)

    val payAccountIdChangedRejectTandC = payAccountIdChangedAcceptTandC.copy(
        status = MeteringTermsAndConditionsStatus.REJECTED)

    val payAccountIdChangedRejectState = MeteringTermsAndConditionsState(payAccountIdChangedRejectTandC, meteringNotaryParty)

    val failureMessage = "Contract verification failed: Failed requirement: pay account id must remain the same"

    checkThatChangingFieldsFails(payAccountIdChangedAcceptState, payAccountIdChangedRejectState, failureMessage)
  }

  @Test
  fun `you cant change the pay party when accepting or rejecting terms and conditions`() {
    val changedPayParty = bogusPayParty

    val payPartyChangedAcceptTandC = issuedMeteringTermsAndConditionsProperties1.copy(
        payParty = changedPayParty,
        status = MeteringTermsAndConditionsStatus.ACCEPTED)
    val payAccountIdChangedAcceptState = MeteringTermsAndConditionsState(payPartyChangedAcceptTandC, meteringNotaryParty)

    val payPartyChangedRejectTandC = payPartyChangedAcceptTandC.copy(
        status = MeteringTermsAndConditionsStatus.REJECTED)

    val payAccountIdChangedRejectState = MeteringTermsAndConditionsState(payPartyChangedRejectTandC, meteringNotaryParty)

    val failureMessage = "Contract verification failed: Failed requirement: pay party must remain the same"

    checkThatChangingFieldsFails(payAccountIdChangedAcceptState, payAccountIdChangedRejectState, failureMessage)
  }

  @Test
  fun `you cant issue terms and conditions if you have an input state`() {
    ledgerServices.ledger {
      val issuedInputLabel = "issuedT&C1"

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, acceptedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.`fails with`("Contract verification failed: Failed requirement: there should be no inputs when issuing")
      }
    }
  }

  @Test
  fun `you cant issue terms and conditions with more than one output`() {
    ledgerServices.ledger {

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, "1", issuedMeteringTermsAndConditionState1)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, "2", issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.`fails with`("Contract verification failed: Failed requirement: there should be one output when issuing")
      }
    }
  }

  @Test
  fun `there should only be one input when accepting a proposal`() {
    val issuedInputLabel1 = "issuedT&C1"
    val issuedInputLabel2 = "issuedT&C2"

    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel1, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel2, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }


      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel1)
        input(issuedInputLabel2)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, acceptedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Accept())
        this.`fails with`("Contract verification failed: Failed requirement: there should be one input when accepting")
      }
    }
  }

  @Test
  fun `there should only be one input when rejecting a proposal`() {
    val issuedInputLabel1 = "issuedT&C1"
    val issuedInputLabel2 = "issuedT&C2"

    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel1, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel2, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel1)
        input(issuedInputLabel2)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, rejectedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Reject())
        this.`fails with`("Contract verification failed: Failed requirement: there should be one input when rejecting")
      }
    }
  }

  private fun checkThatChangingFieldsFails(tokenTypeChangedAcceptState: MeteringTermsAndConditionsState, tokenTypeChangedRejectState: MeteringTermsAndConditionsState, failureMessage: String) {
    ledgerServices.ledger {
      val issuedInputLabel = "issuedT&C1"

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, tokenTypeChangedAcceptState)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Accept())
        this.`fails with`(failureMessage)
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, tokenTypeChangedRejectState)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Reject())
        this.`fails with`(failureMessage)
      }
    }
  }

  @Test
  fun `you cant issue valid terms with extra outputs`() {
    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedMeteringTermsAndConditionState1)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, DummyState(0))
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.fails()
      }
    }
  }

  @Test
  fun `you cant issue valid terms with extra inputs`() {
    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedMeteringTermsAndConditionState1)
        input(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, DummyState(0))
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.fails()
      }
    }
  }

  @Test
  fun `you cant issue terms with a non-party owner`() {
    ledgerServices.ledger {
      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, stolenIssuedMeteringTermsAndConditionState)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.fails()
      }
    }
  }

  @Test
  fun `you cant accept terms with a non-party owner output`() {
    ledgerServices.ledger {
      val issuedInputLabel = "issuedT&C1"

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, stolenAcceptedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Accept())
        this.fails()
      }
    }
  }

  @Test
  fun `you can't reject metering terms and conditions with a non-party owner output`() {
    ledgerServices.ledger {
      val issuedInputLabel = "issuedT&C1"

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, issuedInputLabel, issuedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
        input(issuedInputLabel)
        output(MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID, stolenRejectedMeteringTermsAndConditionState1)
        command(meteringNotaryParty.owningKey, MeteringTermsAndConditionsCommands.Reject())
        this.fails()
      }
    }
  }
}
