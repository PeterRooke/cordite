/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.braid

import io.cordite.braid.corda.BraidConfig
import io.github.classgraph.ClassGraph
import io.vertx.core.Vertx
import net.corda.core.node.AppServiceHub
import net.corda.node.internal.cordapp.CordappProviderImpl

// TODO: if this works, move to Braid - also make braid config blow up if overriding happens
/**
 * Use this class to make a class:
 *   * feel like a CordaService (but note that it must not be one)
 *   * a class that BraidServerCordaService will serve up in braid
 *
 * NB your class must have a single argument constructor that takes and AppServiceHub
 */
interface BraidCordaService {

  companion object {
    fun startAllBraidServicesAndUpdateConfig(config: BraidConfig, serviceHub: AppServiceHub): BraidConfig {
      val braidServices = createAllBraidServices(serviceHub)
      val configWithVertx = config.ensureHasVertx()
      return braidServices.fold(configWithVertx) { acc, service ->
        service.configureWith(acc)
      }
    }

    private fun BraidConfig.ensureHasVertx(): BraidConfig {
      return when (vertx) {
        null -> withVertx(Vertx.vertx())
        else -> this
      }
    }

    private fun createAllBraidServices(serviceHub: AppServiceHub): List<BraidCordaService> {
      val classes = findAllBraidServiceClasses(serviceHub)
      return classes.map {
        it.getConstructor(AppServiceHub::class.java).newInstance(serviceHub) as BraidCordaService
      }
    }

    private fun findAllBraidServiceClasses(serviceHub: AppServiceHub): List<Class<out Any>> {
      val cpi = serviceHub.cordappProvider as CordappProviderImpl
      return cpi.cordapps.flatMap {
        val cl = cpi.getAppContext(it).classLoader
        val res = ClassGraph().enableClassInfo().addClassLoader(cl).overrideClasspath(it.jarPath).scan()
        res.getClassesImplementing(BraidCordaService::class.qualifiedName).names
      }.map {
        Class.forName(it)
      }
    }
  }

  /**
   * Used to customise the braid config - usually you'll just register yourself as a service
   */
  fun configureWith(config: BraidConfig): BraidConfig
}

