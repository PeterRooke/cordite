/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.distribution.impl


import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.serialization.CordaSerializable
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

object DataDistributionGroupMemberSchema

object DataDistributionGroupMemberSchemaV1 : MappedSchema(
  schemaFamily = DataDistributionGroupMemberSchema.javaClass,
  version = 1,
  mappedTypes = listOf(DataDistributionGroupMember::class.java)
)

@CordaSerializable
@Entity
@Table(name = "ddg_record", indexes = [Index(name = "cordite_ddg_member_record_idx", columnList = "linear_id")])
class DataDistributionGroupMember(

  @Id
  @GeneratedValue
  var id: Long,

  @Column(name = "linear_id", nullable = false)
  @Type(type = "uuid-char")
  var linearId: UUID,

  @Column(name = "party", nullable = false)
  var party: Party

) {
  constructor(linearId: UUID, party: Party) : this(0, linearId, party)
}


object DataDistributionGroupSchema

object DataDistributionGroupSchemaV1 : MappedSchema(
  schemaFamily = DataDistributionGroupSchema.javaClass,
  version = 1,
  mappedTypes = listOf(DataDistributionGroup::class.java)
)

@CordaSerializable
@Entity
@Table(name = "ddg", indexes = [Index(name = "cordite_ddg_record_idx", columnList = "linear_id")])
class DataDistributionGroup(
  @Id
  @Column(name = "linear_id", nullable = false)
  @Type(type = "uuid-char")
  var linearId: UUID,

  @Column(name = "description", nullable = false)
  val description: String
)
