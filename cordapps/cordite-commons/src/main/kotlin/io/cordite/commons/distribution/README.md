<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Package `io.cordite.commons.distribution`

# Data Distribution Groups

## 1. Introduction

This package implements a simple design that is inspired from the [Corda technical whitepaper](https://www.corda.net/content/corda-technical-whitepaper.pdf#section.12). 

Key features and constraints:

1. Each DDG has exactly one maintainer.
2. Each DDG has zero or more members that have subscribed to changes to DDG state.
3. Each DDG has exactly one associated `ContractState` that implements `LinearState`. 
4. Updates to this state are transmitted to the DDG members _explicitly_. The initiator has to explicitly invoke the notification after the state has been updated.
5. Guarantees for delivery of updates are bounded by Corda's guarantees of message delivery.
6. Members cannot request for latest version of states. This feature, while simple to implement exposes a potential DDoS attack vector.

## 2. API Interface

The primary interface is [`DataDistributionGroupAPI`](DataDistributionGroupAPI.kt).

Internally, the interface is implemented as a `@CordaService` within the class [`DataDistributionGroupService`](impl/DistributionServiceImpl.kt).

A reference to this interface can be acquired using the `ServiceHub.dataDistribution()` extension method:

```kotlin
val api = serviceHub.dataDistribution()
```

## 3. Static Structure

The DDG API allows one to create structures like this:

```mermaid
graph LR;
ddg[Data Distribution Group]
ddgm["Group Member (Corda Party)"]
id[UniqueIdentifier]
state["Some DDG State : LinearState"]
lp[LinearPointer]
other[OtherAppState]

ddg-->|has zero or more|ddgm
ddg-. has a unique id .->id
id-. which references the DDG state .->state
other-- has a -->lp
lp-. which references .->id
```

## 4. Sequences

There are several use-cases possible with the DDG API. Here are the most common.

### 4.1. Sequence: Creating a DDG

```mermaid
sequenceDiagram
  participant na as NodeA Cordapp
  participant naddg as NodeA DDG API
  na-->>na: create some LinearState called state
  Note left of na: DDG id points to linear state.
  na-->>naddg: create(state.linearId(), description)
```

```kotlin
// Kotlin
api.create(state.linearId(), "a short description")
```

### 4.2. Sequence: Adding a member to a DDG

```mermaid
sequenceDiagram
  participant na as NodeA Cordapp
  participant naddg as NodeA DDG API
  na-->>naddg: addMember(linearId, party)
```

```kotlin
// Kotlin
api.addMember(state.linearId, party)
```

### 4.3: Sequence: Update DDG members with latest state for the group

```mermaid
sequenceDiagram
  participant a as Corda NodeA
  participant na as NodeA Cordapp
  participant naddg as NodeA DDG API
  participant b as Corda NodeB
  a-->>na: some app FlowLogic
  na-->>naddg: FlowLogic.updateDistributionGroup(linearId)
  naddg-->>b: transfers transaction data for latest state version
```

```kotlin
// Kotlin - within FlowLogic implementation
this.updateDistributionGroup(state.linearId)
```