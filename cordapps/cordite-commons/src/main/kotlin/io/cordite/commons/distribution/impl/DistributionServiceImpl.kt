/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.distribution.impl

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.distribution.DataDistributionGroupAPI
import io.cordite.commons.distribution.flows.AddMembersToDistributionFlow
import io.cordite.commons.distribution.flows.UpdateDistributionGroupFlow
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken
import net.corda.core.utilities.contextLogger
import net.corda.core.utilities.getOrThrow
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Path
import kotlin.reflect.KProperty1


@Suppress("unused") // this is a top level service, instantiated by Corda
@CordaService
class DataDistributionGroupService(private val serviceHub: AppServiceHub): SingletonSerializeAsToken(), DataDistributionGroupAPI {
  companion object {
    private val logger = contextLogger()
  }

  init {
    logger.info("Data DataDistributionGroup Group service started")
  }

  @Suspendable
  override fun create(groupId: UniqueIdentifier, description: String) {
    if (exists(groupId)) {
      error("distribution already exists $groupId")
    }
    serviceHub.withEntityManager {
      persist(DataDistributionGroup(groupId.id, description))
    }
  }

  @Suspendable
  override fun exists(groupId: UniqueIdentifier) = getDistribution(groupId) != null

  @Suspendable
  fun getDistribution(distributionId: UniqueIdentifier): DataDistributionGroup? {
    return serviceHub.withEntityManager {
      val query: CriteriaQuery<DataDistributionGroup> = criteriaBuilder.createQuery(DataDistributionGroup::class.java)
      query.apply {
        val root = from(DataDistributionGroup::class.java)
        where(criteriaBuilder.equal(root.get(DataDistributionGroup::linearId), distributionId.id))
        select(root)
      }
      createQuery(query).resultList
    }.singleOrNull()
  }

  @Suspendable
  override fun addMember(groupId: UniqueIdentifier, member: Party) : Boolean {
    return addMembers(groupId to member).isNotEmpty()
  }

  @Suspendable
  override fun addMembers(vararg members: Pair<UniqueIdentifier, Party>): Set<Pair<UniqueIdentifier, Party>> {
    return addMembers(members.toSet())
  }

  @Suspendable
  override fun addMembers(members: Set<Pair<UniqueIdentifier, Party>>): Set<Pair<UniqueIdentifier, Party>> {
    val idAndParties = filterNewParties(members)
    if (idAndParties.isNotEmpty()) {
      addPartiesToDistributionList(idAndParties)
    }
    return idAndParties
  }

  @Suspendable
  override fun addMembers(groupId: UniqueIdentifier, members: Set<Party>) : Set<Party> {
    return if (exists(groupId)) {
      val newMembers = members.filter { hasMember(groupId, it) }.toSet()
      serviceHub.withEntityManager {
        newMembers.forEach { member ->
          persist(DataDistributionGroupMember(groupId.id, member))
        }
      }
      newMembers
    } else {
      emptySet()
    }
  }

  @Suspendable
  override fun removeMembers(vararg members: Pair<UniqueIdentifier, Party>) {
    removeMembers(members.toSet())
  }

  @Suspendable
  override fun removeMembers(members:Set<Pair<UniqueIdentifier, Party>>) {
    removePartiesFromDistributionList(members)
  }

  @Suspendable
  override fun getMembers(groupId: UniqueIdentifier): Set<Party> {
    if (!exists(groupId)) error("distribution not found $groupId")
    return serviceHub.withEntityManager {
      val query: CriteriaQuery<DataDistributionGroupMember> = criteriaBuilder.createQuery(DataDistributionGroupMember::class.java)
      query.apply {
        val root = from(DataDistributionGroupMember::class.java)
        where(criteriaBuilder.equal(root.get(DataDistributionGroupMember::linearId), groupId.id))
        select(root)
      }
      createQuery(query).resultList.map { it.party }.toSet()
    }
  }

  @Suspendable
  override fun hasMember(groupId: UniqueIdentifier, member: Party): Boolean {
    return getMemberRecord(groupId, member) != null
  }

  @Suspendable
  override fun requestAdditionOfMember(flow: FlowLogic<*>, maintainer: Party, members: Set<Pair<UniqueIdentifier, Party>>) {
    val filtered = members.filter { (_, party) -> party != maintainer }
    if (filtered.isNotEmpty()) {
      flow.subFlow(AddMembersToDistributionFlow(maintainer, members))
    }
  }

  @Suspendable
  fun getMemberRecord(distributionId: UniqueIdentifier, party: Party): DataDistributionGroupMember? {
    return serviceHub.withEntityManager {
      val query: CriteriaQuery<DataDistributionGroupMember> = criteriaBuilder.createQuery(DataDistributionGroupMember::class.java)
      query.apply {
        val root = from(DataDistributionGroupMember::class.java)
        val linearIdEq = criteriaBuilder.equal(root.get(DataDistributionGroupMember::linearId), distributionId.id)
        val partyEq = criteriaBuilder.equal(root.get(DataDistributionGroupMember::party), party)
        where(criteriaBuilder.and(linearIdEq, partyEq))
        select(root)
      }
      createQuery(query).resultList
    }.singleOrNull()
  }

  @Suspendable
  private fun filterNewParties(members: Set<Pair<UniqueIdentifier, Party>>): Set<Pair<UniqueIdentifier, Party>> {
    return members.filter { (uid, party) -> !hasMember(uid, party) }.toSet()
  }

  @Suspendable
  private fun addPartiesToDistributionList(idAndParties: Collection<Pair<UniqueIdentifier, Party>>) {
    serviceHub.withEntityManager {
      idAndParties.forEach { (uid, party) ->
        if (!hasMember(uid, party)) {
          persist(DataDistributionGroupMember(uid.id, party))
        }
      }
    }
  }

  @Suspendable
  private fun removePartiesFromDistributionList(idAndParties: Set<Pair<UniqueIdentifier, Party>>) {
    serviceHub.withEntityManager {
      // slow but easy TODO: optimise
      idAndParties
        .asSequence()
        .map { (uid, party) -> DataDistributionGroupMember(uid.id, party) }
        .forEach { remove(it) }
    }
  }

  @Suspendable
  fun updateDistributionGroup(distributionId: UniqueIdentifier) {
    serviceHub.startFlow(UpdateDistributionGroupFlow(distributionId)).returnValue.getOrThrow()
  }

  @Suspendable
  override fun updateDistributionGroup(flow: FlowLogic<*>, groupId: UniqueIdentifier) {
    flow.subFlow(UpdateDistributionGroupFlow(groupId))
  }

  private inline fun <reified X, reified T> Path<X>.get(property: KProperty1<X, T>): Path<T> {
    return this.get<T>(property.name)
  }
}
