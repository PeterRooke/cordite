/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.braid

import io.cordite.braid.corda.BraidConfig
import io.cordite.braid.core.rest.RestConfig
import io.swagger.models.Contact
import io.vertx.core.Handler
import net.corda.core.node.AppServiceHub
import net.corda.core.utilities.loggerFor

fun BraidConfig.acquireCorditeRestConfig() : RestConfig {
  return this.restConfig ?: RestConfig()
    .withContact(Contact().name("LAB577").url("https://lab577.io").email("devops@lab577.io"))
    .withServiceName("Swagger API")
    .withDescription("""|<h4><a href="/rest">REST API</a></h4>
            |<b>Please note:</b> The protected parts of this API require JWT authentication.
            |To activate, execute the <code>login</code> method.
            |Then copy the returned JWT token and insert it into the <i>Authorize</i> swagger dialog box as
            |<code>Bearer &lt;token&gt;</code>
          """.trimMargin().replace("\n", ""))
    .withApiPath("/rest")
    .withSwaggerPath("/swagger")
}

fun BraidConfig.amendRestConfig(fn: RestConfig.() -> RestConfig) : BraidConfig {
  val newRestConfig = acquireCorditeRestConfig().fn()
  return this.withRestConfig(newRestConfig)
}

fun BraidConfig.bootstrapAndLog(serviceHub: AppServiceHub) {
  val log = loggerFor<BraidConfig>()
  bootstrapBraid(serviceHub, Handler {
    when {
      it.failed() -> log.error("failed to start braid", it.cause())
      else -> {
        val protocol = when (httpServerOptions.isSsl) { true -> "https" false -> "http"}
        val host = httpServerOptions.host
        val swagger = restConfig!!.swaggerPath
        val rest = restConfig!!.apiPath
        println("Swagger: $protocol://$host:$port$swagger")
        println("REST endpoint: $protocol://$host:$port$rest")
      }
    }
  })
}