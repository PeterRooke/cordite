/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.braid.rest

import io.cordite.braid.corda.BraidConfig
import io.cordite.commons.braid.BraidCordaService
import io.cordite.commons.braid.amendRestConfig
import io.swagger.annotations.ApiOperation
import net.corda.core.node.AppServiceHub

class StatusBinding(@Suppress("UNUSED_PARAMETER") appServiceHub: AppServiceHub) : BraidCordaService {

  override fun configureWith(config: BraidConfig) = config.amendRestConfig {
    withPaths {
      group("status") {
        unprotected {
          get("/status/version", ::version)
          get("/status/live", ::live)
          get("/status/ready", ::ready)
        }
      }
    }
  }

  @ApiOperation("Get deployed version")
  fun version() = System.getenv("CORDITE_VERSION") ?: "master"

  @ApiOperation("Is DASL live?")
  fun live() = "yes"

  @ApiOperation("Is DASL ready?")
  fun ready() = "yes"

}