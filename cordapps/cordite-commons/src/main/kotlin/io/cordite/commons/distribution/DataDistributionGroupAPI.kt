/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.distribution

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.distribution.flows.UpdateDistributionGroupFlow
import io.cordite.commons.distribution.impl.DataDistributionGroupService
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub

/**
 * Get the global distribution service
 */
@Suspendable
fun ServiceHub.dataDistribution() : DataDistributionGroupAPI {
  return cordaService(DataDistributionGroupService::class.java)
}

/**
 * Update the distribution group with the latest version of a value
 */
@Suspendable
fun FlowLogic<*>.updateDistributionGroup(groupId: UniqueIdentifier) {
  subFlow(UpdateDistributionGroupFlow(groupId))
}

/**
 * Provide a means of creating simple distribution groups
 *
 * Structure is as follows:
 *
 *   +-------------------------+       +----------------------+
 *   |                         |     * |                      |
 *   | Data Distribution Group +------>+ Group Member [Party] |
 *   |                         |       |                      |
 *   +-------------------------+       +----------------------+
 *               |
 *               | UniqueIdentifier
 *               v
 *   +-------------------------+       +----------------------+
 *   |                         |       |                      |
 *   |     LinearState         +<|-----+   Some App State     |
 *   |                         |       |                      |
 *   +-------------------------+       +----------------------+
 *
 *
 */
interface DataDistributionGroupAPI {
  /**
   * Create a distribution group with [groupId] and [description]
   */
  @Suspendable
  fun create(groupId: UniqueIdentifier, description: String)

  /**
   * True iff a distribution group with id [groupId] exists
   */
  @Suspendable
  fun exists(groupId: UniqueIdentifier): Boolean

  /**
   * Add a new member to a distribution list held locally.
   * @return true iff a new record was added.
   */
  @Suspendable
  fun addMember(groupId: UniqueIdentifier, member: Party): Boolean

  /**
   * Add many members to many groups.
   * @return set of actual [Pair<UniqueIdentifier, Party>] that were added
   */
  @Suspendable
  fun addMembers(vararg members: Pair<UniqueIdentifier, Party>): Set<Pair<UniqueIdentifier, Party>>

  /**
   * Add many members to many groups.
   * @return set of actual [Pair<UniqueIdentifier, Party>] that were added
   */
  @Suspendable
  fun addMembers(members: Set<Pair<UniqueIdentifier, Party>>): Set<Pair<UniqueIdentifier, Party>>

  /**
   * Add zero or more parties to a group.
   * @return the set of actual parties added
   */
  @Suspendable
  fun addMembers(groupId: UniqueIdentifier, members: Set<Party>) : Set<Party>

  /**
   * Remove [members] from a set of respective groups
   */
  @Suspendable
  fun removeMembers(vararg members: Pair<UniqueIdentifier, Party>)

  /**
   * Remove [members] from a set of respective groups
   */
  @Suspendable
  fun removeMembers(members: Set<Pair<UniqueIdentifier, Party>>)

  /**
   * @return the set of members for distribution [groupId]
   */
  @Suspendable
  fun getMembers(groupId: UniqueIdentifier): Set<Party>

  /**
   * @return true iff distribution [groupId] contains [member]
   */
  @Suspendable
  fun hasMember(groupId: UniqueIdentifier, member: Party): Boolean

  /**
   * Update all members of distribution [groupId] with the latest version of the [net.corda.core.contracts.LinearState]
   * with the same id
   */
  @Suspendable
  fun updateDistributionGroup(flow: FlowLogic<*>, groupId: UniqueIdentifier)

  @Suspendable
  fun requestAdditionOfMember(flow: FlowLogic<*>, maintainer: Party, members: Set<Pair<UniqueIdentifier, Party>>)
}
