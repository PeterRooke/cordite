/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.braid.auth

import io.cordite.commons.utils.contextLogger
import io.vertx.core.AsyncResult
import io.vertx.core.Future.failedFuture
import io.vertx.core.Future.succeededFuture
import io.vertx.core.Handler
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.auth.User

class BasicAuthProvider(private val username : String, private val password : String) : AuthProvider {
  companion object {

    private val log = contextLogger()
    private const val USER = "username"
    private const val PASSWORD = "password"
  }
  override fun authenticate(authInfo: JsonObject, resultHandler: Handler<AsyncResult<User>>) {
    try {
      val u = authInfo.getString(USER) ?: error("no $USER field")
      val p = authInfo.getString(PASSWORD) ?: error("no $PASSWORD field")
      if (u != username || p != password) error("authentication failed")
      log.info("authentication successful")
      val basicAuthUser = BasicAuthUser(username).apply {
        @Suppress("DEPRECATION")
        setAuthProvider(this@BasicAuthProvider)
      }
      resultHandler.handle(succeededFuture(basicAuthUser))
    } catch (err: Throwable) {
      log.error("failed to login with credentials $authInfo")
      resultHandler.handle(failedFuture(err))
    }
  }
}

class BasicAuthUser(private val user: String) : User {
  private var authProvider : AuthProvider? = null

  override fun clearCache(): User {
    return this
  }

  override fun setAuthProvider(authProvider: AuthProvider?) {
    this.authProvider = authProvider
  }

  override fun isAuthorized(authority: String?, resultHandler: Handler<AsyncResult<Boolean>>?): User {
    resultHandler?.handle(succeededFuture(true))
    return this
  }

  override fun principal(): JsonObject {
    return JsonObject().put("user", user)
  }
}