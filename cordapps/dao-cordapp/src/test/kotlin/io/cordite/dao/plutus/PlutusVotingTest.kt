/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.plutus

import io.cordite.dao.core.DaoState
import io.cordite.dao.proposal.ProposalState
import io.cordite.dao.voting.NOT_QUORUM
import io.cordite.dao.voting.Vote
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import io.cordite.test.utils.*
import net.corda.core.crypto.entropyToKeyPair
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger
import java.security.KeyPair

class PlutusVotingTest {

  private val plutusProposalVotingStrategy = PlutusProposalVotingStrategy()
  private val plutusProposal = PlutusProposal(TokenDescriptor("XDC", proposerName), 40)
  private val daoState = DaoState("dao", setOf(proposerParty, newMemberParty, anotherMemberParty))
  private val successfulVoters = setOf(proposerParty, anotherMemberParty)

  @Test
  fun `should return not quorum if not enough people vote`() {
    val proposalState = ProposalState(plutusProposal, proposerParty, emptySet(), daoState.members, daoState.daoKey)
    val voteResult = plutusProposalVotingStrategy.voteStatus(proposalState, daoState)
    Assert.assertEquals("with no support should be not quorum", NOT_QUORUM, voteResult)
  }

  @Test
  fun `should return up if enough people vote up`() {
    val proposalState = ProposalState(plutusProposal, proposerParty, successfulVoters.map { Vote(it, UP) }.toSet(), daoState.members, daoState.daoKey)
    val voteResult = plutusProposalVotingStrategy.voteStatus(proposalState, daoState)
    Assert.assertEquals("with over 50% UP should be UP", UP_RESULT, voteResult)
  }

  @Test
  fun `should return down if enough people vote down`() {
    val proposalState = ProposalState(plutusProposal, proposerParty, successfulVoters.map { Vote(it, DOWN) }.toSet(), daoState.members, daoState.daoKey)
    val voteResult = plutusProposalVotingStrategy.voteStatus(proposalState, daoState)
    Assert.assertEquals("with over 50% DOWN should be DOWN", DOWN_RESULT, voteResult)
  }

}