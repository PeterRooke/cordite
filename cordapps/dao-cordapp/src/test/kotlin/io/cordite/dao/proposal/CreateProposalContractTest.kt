/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import io.cordite.dao.core.DAO_CONTRACT_ID
import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MEMBERSHIP_CONTRACT_ID
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membershipState
import io.cordite.dao.proposal.ProposalContract
import io.cordite.dao.proposalState
import io.cordite.test.utils.*
import net.corda.core.contracts.CommandData
import net.corda.core.crypto.entropyToKeyPair
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.testing.contracts.DummyState
import net.corda.testing.node.ledger
import org.junit.Before
import org.junit.Test
import java.math.BigInteger
import java.security.KeyPair

class CreateProposalContractTest {

  private val newMember1Key: KeyPair by lazy { entropyToKeyPair(BigInteger.valueOf(315)) }
  private val newMember1Name = CordaX500Name("m1","NewMember", "London", "GB") // has same O as newMember
  private val newMember1Party : Party get() =  Party(newMember1Name, newMember1Key.public)

  private val membershipState = membershipState(members = setOf(proposerParty))
  private val daoState = DaoState("dao", setOf(proposerParty, newMemberParty, anotherMemberParty))

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(proposerKey.public, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, proposalState())
        reference("membershipState")
        reference("daoState")
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        verifies()
      }
    }
  }

  @Test
  fun `propose should not consume state`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, proposalState())
        reference("daoState")
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("No inputs should be consumed when creating a proposal")
      }
    }
  }

  @Test
  fun `an extra dummy input should verify`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(proposerKey.public, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        reference("membershipState")
        reference("daoState")
        input(PROPOSAL_CONTRACT_ID, DummyState())
        output(PROPOSAL_CONTRACT_ID, proposalState())
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        verifies()
      }
    }
  }

  @Test
  fun `there should be one output state`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      transaction {
        val proposalState = proposalState()
        reference("daoState")
        output(PROPOSAL_CONTRACT_ID, proposalState)
        output(PROPOSAL_CONTRACT_ID, proposalState)
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("There should be one output proposal")
      }
    }
  }

  @Test
  fun `an extra dummy output should verify`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(proposerKey.public, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        reference("daoState")
        reference("membershipState")
        output(PROPOSAL_CONTRACT_ID, proposalState())
        output(PROPOSAL_CONTRACT_ID, DummyState())
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        verifies()
      }
    }
  }

  class DummyCommandData : CommandData

  @Test
  fun `an extra dummy command should verify`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(proposerKey.public, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        reference("daoState")
        reference("membershipState")
        output(PROPOSAL_CONTRACT_ID, proposalState())
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        command(proposerKey.public, DummyCommandData())
        verifies()
      }
    }
  }

  @Test
  fun `there should be a single command of type Commands`() {
    ledgerServices.ledger {
      transaction {
        output(PROPOSAL_CONTRACT_ID, proposalState())
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("there should be exactly one proposal command")
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, proposalState())
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        command(proposerKey.public, ProposalContract.Commands.AcceptProposal())
        `fails with`("there should be exactly one proposal command")
      }
    }
  }

  @Test
  fun `proposer must be only initial voter`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      transaction {
        reference("daoState")
        output(PROPOSAL_CONTRACT_ID, proposalState(supporters = setOf(daoParty)))
        command(daoKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("proposer must be only initial voter")
      }
    }
  }

  @Test
  fun `proposer must be signer`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(proposerKey.public, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        reference("daoState")
        reference("membershipState")
        output(PROPOSAL_CONTRACT_ID, proposalState())
        command(daoKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("proposer must be signer")
      }
    }
  }

  @Test
  fun `all members must be signers`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(proposerKey.public, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        reference("daoState")
        reference("membershipState")
        output(PROPOSAL_CONTRACT_ID, proposalState(members = setOf(proposerParty, daoParty)))
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("the proposal state members must be the same as the membership state's members")
      }
    }
  }

  @Test
  fun `should not be able to add member with same org as existing member`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(proposerKey.public, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        reference("daoState")
        reference("membershipState")
        output(PROPOSAL_CONTRACT_ID, proposalState(members = setOf(proposerParty, daoParty)))
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("the proposal state members must be the same as the membership state's members")
      }
    }
  }
}