/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.plutus

import io.cordite.dao.core.DAO_CONTRACT_ID
import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MEMBERSHIP_CONTRACT_ID
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.membershipState
import io.cordite.dao.proposal.PROPOSAL_CONTRACT_ID
import io.cordite.dao.proposal.ProposalContract
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dao.proposal.ProposalState
import io.cordite.dao.voting.Vote
import io.cordite.dao.voting.VotingModelData
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import io.cordite.test.utils.*
import net.corda.core.contracts.TimeWindow
import net.corda.testing.node.ledger
import org.junit.Test
import java.time.Instant
import java.time.temporal.ChronoUnit

class CreatePlutusProposalContractTest {

  private val tokenDescriptor = TokenDescriptor("XCD", proposerParty.name)
  private val tokenTypeTemplate = TokenTypeTemplate("XCD", 2, proposerParty.name)

  private val daoKey = DaoKey("theDao")

  private val plutusProposalState = ProposalState(PlutusProposal(tokenDescriptor, 20), proposerParty, setOf(Vote(proposerParty, UP)), setOf(proposerParty, newMemberParty), daoKey)
  private val nonOwnerProposed = ProposalState(PlutusProposal(tokenDescriptor, 20), newMemberParty, setOf(Vote(newMemberParty, UP)), setOf(proposerParty, newMemberParty), daoKey)
  private val plutusModelData = PlutusModelData(proposerParty.name,"account", listOf(tokenTypeTemplate), 0.14, 1000.0, Instant.now().plus(1, ChronoUnit.DAYS))
  private val votingModelData = VotingModelData(PlutusVotingStrategySelector())
  private val membershipModelData = MembershipModelData(MembershipKey(daoKey.name), 1, true, strictMode = false)
  private val dao = DaoState("theDao", setOf(proposerParty, newMemberParty), daoKey, mapOf(PlutusModelData::class.qualifiedName!! to plutusModelData, VotingModelData::class.qualifiedName!! to votingModelData, MembershipModelData::class.qualifiedName!! to membershipModelData))

  private val membershipState = membershipState(members = setOf(proposerParty, newMemberParty))

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(dao.members.map { it.owningKey }, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(dao.members.map { it.owningKey }, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, plutusProposalState)
        reference("membershipState")
        reference("dao")
        command(dao.members.map { it.owningKey }, ProposalContract.Commands.CreateProposal())
        verifies()
      }
    }
  }

  @Test
  fun `only dao admin can create plutus proposals`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(dao.members.map { it.owningKey }, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(dao.members.map { it.owningKey }, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, nonOwnerProposed)
        reference("membershipState")
        reference("dao")
        command(dao.members.map { it.owningKey }, ProposalContract.Commands.CreateProposal())
        failsWith("only admin can create plutus proposals")
      }
    }
  }


}