/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.integration

import io.cordite.braid.client.BraidClient
import io.cordite.braid.core.async.getOrThrow
import io.cordite.commons.distribution.impl.DataDistributionGroup
import io.cordite.commons.utils.contextLogger
import io.cordite.dao.DaoApi
import io.cordite.dao.assertDaoStateContainsAtleastMembers
import io.cordite.dao.assertDaoStateContainsMembers
import io.cordite.dao.coop.Address
import io.cordite.dao.coop.CoopModelData
import io.cordite.dao.core.DaoState
import io.cordite.dao.core.ModelData
import io.cordite.dao.core.SampleModelData
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.plutus.*
import io.cordite.dao.proposal.NormalProposal
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dao.proposal.ProposalState
import io.cordite.dao.test.DAOTestBraidServer
import io.cordite.dao.voting.*
import io.cordite.dgl.api.LedgerApi
import io.cordite.dgl.contract.v1.account.AccountContract
import io.cordite.dgl.contract.v1.token.TokenTypeState
import io.cordite.test.utils.*
import io.cordite.test.utils.WaitForHttpEndPoint.Companion.waitForHttpEndPoint
import io.vertx.core.Promise.promise
import io.vertx.core.Vertx
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.internal.packageName
import net.corda.core.utilities.loggerFor
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkParameters
import net.corda.testing.node.StartedMockNode
import net.corda.testing.node.internal.cordappForClasses
import net.corda.testing.node.internal.findCordapp
import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.IllegalArgumentException
import java.math.BigDecimal

class TestNode(val node: StartedMockNode, braidPortHelper: BraidPortHelper) {
  companion object {
    val log = loggerFor<TestNode>()
  }

  private val daoBraidClient: BraidClient
  private val dglBraidClient: BraidClient
  private val vertx: Vertx = Vertx.vertx()

  val party: Party = node.info.legalIdentities.first()
  val daoApi: DaoApi
  val dglApi: LedgerApi

  init {
    log.info("initialising binding for ${party.name}")
    val succeeded = promise<Unit>()
    val port = braidPortHelper.portForParty(party)
    waitForHttpEndPoint(vertx = vertx, port = port, handler = succeeded, path = "/api/")
    succeeded.future().getOrThrow()
    log.info("attempting to bind to test service on $port")

    daoBraidClient = BraidClientHelper.braidClient(port, "daoservice", "localhost", vertx)
    daoApi = daoBraidClient.bind(DaoApi::class.java)
    dglBraidClient = BraidClientHelper.braidClient(port, "ledger", "localhost", vertx)
    dglApi = dglBraidClient.bind(LedgerApi::class.java)
  }

  fun shutdown() {
    daoBraidClient.close()
    dglBraidClient.close()
    vertx.close()
  }

}

@RunWith(VertxUnitRunner::class)
class DaoIntegrationTest {

  companion object {
    private val log = contextLogger()
    private const val casaDaoBase = "casaDao"
    private const val proposalBase = "proposal"

    private lateinit var network: MockNetwork

    private val braidPortHelper = BraidPortHelper()

    private lateinit var proposer: TestNode
    private lateinit var newMember: TestNode
    private lateinit var anotherMember: TestNode
    private lateinit var someoneElse: TestNode
    private lateinit var notaryName: CordaX500Name

    private var salt = 0

    @BeforeClass
    @JvmStatic
    fun setup() {
      braidPortHelper.setSystemPropertiesFor(proposerName, newMemberName, anotherMemberName, someoneElseName)
      val cordapps = setOf(findCordapp(AccountContract::class.packageName),
        findCordapp(LedgerApi::class.packageName),
        findCordapp(DataDistributionGroup::class.packageName),
        findCordapp(DaoApi::class.packageName),
        cordappForClasses(DAOTestBraidServer::class.java)
      )

      network = MockNetwork(MockNetworkParameters(
        cordappsForAllNodes = cordapps,
        networkParameters = testNetworkParameters(minimumPlatformVersion = 4)
      ))

      val proposerNode = network.createPartyNode(proposerName)
      val newMemberNode = network.createPartyNode(newMemberName)
      val anotherMemberNode = network.createPartyNode(anotherMemberName)
      val someoneElseNode = network.createPartyNode(someoneElseName)
      network.runNetwork()

      proposer = TestNode(proposerNode, braidPortHelper)
      newMember = TestNode(newMemberNode, braidPortHelper)
      anotherMember = TestNode(anotherMemberNode, braidPortHelper)
      someoneElse = TestNode(someoneElseNode, braidPortHelper)

      network.runNetwork()

      notaryName = network.defaultNotaryIdentity.name
    }

    @AfterClass
    @JvmStatic
    fun tearDown() {
      proposer.shutdown()
      newMember.shutdown()
      anotherMember.shutdown()
      try {
        network.stopNodes()
      } catch (e: NullPointerException) {
        log.info(e.message)
      }
    }

  }

  private val saltedDaoName = casaDaoBase + salt++
  private val saltedProposalName = proposalBase + salt++

  @Test
  fun `should be able to get whitelist`() {
    val config = proposer.daoApi.nodeConfig()
    Assert.assertTrue("config should contain notary info", config.contains("NotaryInfo(identity=O=Notary Service, L=Zurich, C=CH, validating=true)"))
    Assert.assertTrue("config should contain node info", config.contains("NodeInfo(addresses=[mock.node:1000], legalIdentitiesAndCerts=[O=Proposer, L=London, C=GB], platformVersion=7"))
  }

  @Test
  fun `should be able to get node list for chris for the mo - move this`() {
    Assert.assertEquals("incorrect number of nodes", 5, proposer.daoApi.listNodes().size)
  }

  @Test
  fun `default dao should be strict and have min members of 3`() {
    val daoStateMembershipModelData = execute(network) { proposer.daoApi.createDao(saltedDaoName, notaryName) }.membershipModelData()
    Assert.assertEquals("default min members should have been 3", 3, daoStateMembershipModelData.minimumMemberCount)
    Assert.assertEquals("default strict mode should be true", true, daoStateMembershipModelData.strictMode)
  }

  @Test
  fun `should be able to create a dao`() {
    createDaoWithName(saltedDaoName, notaryName)
  }

  @Test
  fun `should be able to specify data needed for list daos table`() {
    val coopModelData = CoopModelData("these are my objects", Address.from("some", "address", "lines"))
    val mdSet: Array<ModelData> = arrayOf(coopModelData)
    val daoState = execute(network) { proposer.daoApi.createDao(saltedDaoName, mdSet, notaryName) }
    Assert.assertEquals("coop model datas should be the same", coopModelData, daoState.get(CoopModelData::class))
  }

  @Test
  fun `should be able to create another dao in the same node`() {
    // because the salt changes for each test run this is actually a different dao..
    createDaoWithName(saltedDaoName, notaryName)
  }

  @Test
  fun `should be able to get list of daos`() {
    val initialDaoCount = proposer.daoApi.listDaos().size
    createDaoWithName(saltedDaoName, notaryName)
    val newDaoCount = proposer.daoApi.listDaos().size
    Assert.assertEquals("incorrect number of daos", initialDaoCount + 1, newDaoCount)
  }

  @Test
  fun `should be able to add a new member`() {
    val dao = createDaoWithName(saltedDaoName, notaryName)
    val proposal = execute(network) { newMember.daoApi.createNewMemberProposal(saltedDaoName, proposer.party.name) }
    Assert.assertEquals("should be 1 supporters", 1, proposal.votes.size)
    Assert.assertTrue("member should be a supporter", proposal.votes.contains(Vote(newMember.party, FOR)))

    // accept member proposal
    val (acceptedProposalLifecycle, acceptedVoteResult) = execute(network) { newMember.daoApi.sponsorAcceptProposal(proposal.proposal.key(), dao.daoKey, proposer.party.name) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    Assert.assertEquals("vote result should be passed", PASSED, acceptedVoteResult)

    assertDaoStateContainsMembers(getDaoWithRetry(newMember), proposer.party, newMember.party)
  }

  @Test
  fun `should be able to add two members`() {
    createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
  }

  @Test
  fun `should be able to create, vote for and accept and list new proposal`() {
    val originalProposalCount = proposer.daoApi.listProposalKeys().size
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    createAndAcceptProposalWithName(saltedProposalName, proposer, daoState, newMember)

    val keys = proposer.daoApi.listProposalKeys()
    Assert.assertEquals("there should be only one proposal", originalProposalCount + 1, keys.size)
    val proposal = proposer.daoApi.proposalFor(keys.first())
    Assert.assertEquals("proposal should have correct key", keys.first(), proposal.proposal.key())
  }

  @Test
  fun `new member should receive proposals`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    createProposal(saltedProposalName, proposer, daoState)
    addMemberToDao(newMember, proposer, saltedDaoName)
    val proposals = newMember.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
    Assert.assertEquals("should have correct proposal", saltedProposalName, proposals.first().name)
  }

  @Test
  fun `removing member from dao should remove them from proposals`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
    createProposal(saltedProposalName, proposer, daoState)
    val proposals = newMember.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
    Assert.assertEquals("there should be 3 members", 3, proposals.first().members.size)

    val removeProposal = execute(network) { proposer.daoApi.createRemoveMemberProposal(daoState.daoKey, newMember.party.name) }

    Assert.assertEquals("should be 1 supporters", 1, removeProposal.votes.size)
    Assert.assertTrue("proposer should be a supporter", removeProposal.votes.contains(Vote(proposer.party, FOR)))

    // other member approves
    execute(network) { anotherMember.daoApi.voteForProposal(removeProposal.proposal.proposalKey, FOR) }

    val (acceptedProposalLifecycle, acceptedVoteResult) = execute(network) { proposer.daoApi.acceptProposal(removeProposal.proposal.key()) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    Assert.assertEquals("vote result should be passed", PASSED, acceptedVoteResult)

    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party, anotherMember.party)

    val currentProposals = proposer.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should still be 1 proposal", 1, currentProposals.size)
    Assert.assertEquals("there should now be 2 members", 2, currentProposals.first().members.size)
  }

  @Test
  fun `removing member from dao should remove them from proposal supporters`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
    val proposal = createProposal(saltedProposalName, proposer, daoState)

    // support the proposal
    forTheLoveOfGodIgnoreThisBit()
    execute(network) { newMember.daoApi.voteForProposal(proposal.proposal.proposalKey, FOR) }

    val proposals = newMember.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
    Assert.assertEquals("there should be 3 members", 3, proposals.first().members.size)
    Assert.assertTrue("new member should be supporter", proposals.first().votes.contains(Vote(newMember.party, FOR)))

    val removeProposal = execute(network) { proposer.daoApi.createRemoveMemberProposal(daoState.daoKey, newMember.party.name) }

    Assert.assertEquals("should be 1 supporters", 1, removeProposal.votes.size)
    Assert.assertTrue("proposer should be a supporter", removeProposal.votes.contains(Vote(proposer.party, FOR)))

    // other member approves
    execute(network) { anotherMember.daoApi.voteForProposal(removeProposal.proposal.proposalKey, FOR) }

    val (acceptedProposalLifecycle, acceptedVoteResult) = execute(network) { proposer.daoApi.acceptProposal(removeProposal.proposal.key()) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    Assert.assertEquals("vote result should be passed", PASSED, acceptedVoteResult)

    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party, anotherMember.party)

    val currentProposals = proposer.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should still be 1 proposal", 1, currentProposals.size)
    Assert.assertEquals("there should now be 2 members", 2, currentProposals.first().members.size)
    Assert.assertFalse("new member should not now be a supporter", currentProposals.first().voters.contains(newMemberParty))
  }

  @Test
  fun `should be able to create a simple model data for a dao`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)

    Assert.assertFalse(daoState.containsModelData(SampleModelData::class))

    val newModelData = SampleModelData("initial")
    val proposalState = execute(network) { proposer.daoApi.createModelDataProposal("fiddle with metering model data", newModelData, daoState.daoKey) }

    execute(network) { newMember.daoApi.voteForProposal(proposalState.proposal.proposalKey, FOR) }

    val proposals = proposer.daoApi.modelDataProposalsFor(daoState.daoKey)

    Assert.assertEquals("there should only be one proposal", 1, proposals.size)
    Assert.assertEquals("the proposal should have 2 supporters", 2, proposals.first().votes.size)
    Assert.assertTrue("the proposal should have all supporters", proposals.first().voters.containsAll(setOf(proposer.party, newMember.party)))

    val (acceptedProposalLifecycle, acceptedVoteResult) = execute(network) { proposer.daoApi.acceptProposal(proposalState.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    Assert.assertEquals("vote result should be passed", PASSED, acceptedVoteResult)

    val newDaoState = proposer.daoApi.daoFor(daoState.daoKey)
    Assert.assertEquals("dao should contain metering model data", newModelData, newDaoState.get(newModelData::class))
  }

  @Test
  fun `should be able to add plutus model data to existing dao`() {
    Assert.assertEquals("there should be no dao at the beginning", 0, proposer.daoApi.daoInfo(saltedDaoName).size)
    val membershipModelData = MembershipModelData(MembershipKey(saltedDaoName), 1, strictMode = false)
    val votingModelData = VotingModelData(PlutusVotingStrategySelector())
    val accountId = saltedDaoName
    val daoState = execute(network) { proposer.daoApi.createDao(saltedDaoName, arrayOf(membershipModelData, votingModelData), notaryName)}
    addMemberToPlutusDao(newMember, proposer, saltedDaoName)

    Assert.assertFalse("dao state currently should not have plutus model data", daoState.containsModelData(PlutusModelData::class))

    val plutusModelData = PlutusModelData(proposerName, accountId, listOf(TokenTypeTemplate("XPLT", 2, proposerName)), 0.14, 0.0)

    val proposalState = execute(network) { proposer.daoApi.createModelDataProposal("propose plutus", plutusModelData, daoState.daoKey) }

    execute(network) { newMember.daoApi.voteForProposal(proposalState.proposal.proposalKey, FOR) }

    val (acceptedProposalLifecycle, acceptedVoteResult) = execute(network) { proposer.daoApi.acceptProposal(proposalState.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    Assert.assertEquals("vote result should be passed", PASSED, acceptedVoteResult)

    // check proposer has the token type
    val tokenTypes = execute(network) { proposer.dglApi.listTokenTypes() }
    val xpltTokenType = tokenTypes.first { it.descriptor.symbol == "XPLT" }
    Assert.assertEquals("plutus token issuer should be proposer", proposer.party, xpltTokenType.issuer)

    listOf(proposer, newMember).forEach {
      val account = execute(network) { it.dglApi.getAccount(accountId) }
      Assert.assertTrue("${it.party.name} account should have dao tag", account.tags.contains(DAO_TAG))
    }
  }

  @Test
  fun `should be possible for dao owner to remove a member in the extreme`() {
    Assert.assertEquals("there should be no dao at the beginning", 0, proposer.daoApi.daoInfo(saltedDaoName).size)
    val daoState = execute(network) { proposer.daoApi.createPlutusDao(saltedDaoName, notaryName, "XCDC", 0.14) }
    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party)
    addMemberToPlutusDao(newMember, proposer, saltedDaoName)
    addMemberToPlutusDao(anotherMember, proposer, saltedDaoName)

    val removeProposal = execute(network) { proposer.daoApi.createRemoveMemberProposal(daoState.daoKey, newMember.party.name) }
    val (acceptedProposalLifecycle, acceptedVoteResult) = execute(network) { proposer.daoApi.acceptProposal(removeProposal.proposal.key()) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    Assert.assertEquals("vote result should be passed", PASSED, acceptedVoteResult)

    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party, anotherMember.party)
  }

  @Test
  fun `non dao owner should not be able to create plutus proposal`() {
    Assert.assertEquals("there should be no dao at the beginning", 0, proposer.daoApi.daoInfo(saltedDaoName).size)
    val daoState = execute(network) { proposer.daoApi.createPlutusDao(saltedDaoName, notaryName, "XOX", 0.14) }
    addMemberToPlutusDao(newMember, proposer, saltedDaoName)

    val tokenTypes = execute(network) { proposer.dglApi.listTokenTypes() }
    val xpltTokenType = tokenTypes.first { it.descriptor.symbol == "XOX" }

    try {
      execute(network) { newMember.daoApi.createPlutusProposal(xpltTokenType.descriptor, 50, daoState.daoKey) }
      Assert.fail("should have failed")
    } catch (e: RuntimeException) {
      if (e.message == null) {
        Assert.fail("should have failed with message")
      }
      Assert.assertTrue("should fail as not admin", e.message!!.contains("only admin can create plutus proposals"))
    }
  }

  @Test
  fun `non dao owner should not be able to accept plutus proposal`() {
    Assert.assertEquals("there should be no dao at the beginning", 0, proposer.daoApi.daoInfo(saltedDaoName).size)
    val daoState = execute(network) { proposer.daoApi.createPlutusDao(saltedDaoName, notaryName, "XPX", 0.14) }
    addMemberToPlutusDao(newMember, proposer, saltedDaoName)

    val tokenTypes = execute(network) { proposer.dglApi.listTokenTypes() }
    val xpltTokenType = tokenTypes.first { it.descriptor.symbol == "XPX" }

    val plutusProposal = execute(network) { proposer.daoApi.createPlutusProposal(xpltTokenType.descriptor, 50, daoState.daoKey) }

    try {
      execute(network) { newMember.daoApi.acceptProposal(plutusProposal.proposal.proposalKey) }
      Assert.fail("should have failed")
    } catch (e: RuntimeException) {
      if (e.message == null) {
        Assert.fail("should have failed with message")
      }
      Assert.assertTrue("should fail as not admin", e.message!!.contains("i should not be in the create or accept proposal responder flow as a proposer for plutus if i am admin"))
    }
  }

  // should probably be a number of tests in another test class once we've brought up the test env once
  @Test
  fun `should be able to create an initial plutus proposal`() {
    Assert.assertEquals("there should be no dao at the beginning", 0, proposer.daoApi.daoInfo(saltedDaoName).size)
    val daoState = execute(network) { proposer.daoApi.createPlutusDao(saltedDaoName, notaryName, "XDC", 0.14) }
    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party)
    addMemberToPlutusDao(newMember, proposer, saltedDaoName)

    val accountId = saltedDaoName

    val tokenTypes = execute(network) { proposer.dglApi.listTokenTypes() }
    val xpltTokenType = tokenTypes.first { it.descriptor.symbol == "XDC" }

    // add another member and check the member has the account
    addMemberToPlutusDao(anotherMember, proposer, saltedDaoName)
    val account = execute(network) { anotherMember.dglApi.getAccount(accountId) }
    Assert.assertTrue("account should have dao tag", account.tags.contains(DAO_TAG))

    createAndAcceptPlutusProposal(xpltTokenType, daoState, accountId, "500000.00", 1000000.0, 0.14)
  }

  @Test
  fun `should be able to create a second plutus proposal`() {
    val accountId = saltedDaoName
    Assert.assertEquals("there should be no dao at the beginning", 0, proposer.daoApi.daoInfo(saltedDaoName).size)
    val membershipModelData = MembershipModelData(MembershipKey(saltedDaoName), 1, strictMode = false)
    val plutusModelData = PlutusModelData(proposerName, accountId, listOf(TokenTypeTemplate("XFFS", 2, proposerName)), 0.14, 1000000.0)
    val votingModelData = VotingModelData(PlutusVotingStrategySelector())

    val daoState = execute(network) { proposer.daoApi.createDao(saltedDaoName, arrayOf(membershipModelData, votingModelData, plutusModelData), notaryName)}

    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party)
    addMemberToPlutusDao(newMember, proposer, saltedDaoName)

    val tokenTypes = execute(network) { proposer.dglApi.listTokenTypes() }
    val xpltTokenType = tokenTypes.first { it.descriptor.symbol == "XFFS" }

    // add another member and check the member has the account
    addMemberToPlutusDao(anotherMember, proposer, saltedDaoName)
    val account = execute(network) { anotherMember.dglApi.getAccount(accountId) }
    Assert.assertTrue("account should have dao tag", account.tags.contains(DAO_TAG))

    createAndAcceptPlutusProposal(xpltTokenType, daoState, accountId, "67500.00", 1135000.0, 0.135)
  }

  @Test
  fun `should not do anything if proposal is NOT_QUORUM`() {
    Assert.assertEquals("there should be no dao at the beginning", 0, proposer.daoApi.daoInfo(saltedDaoName).size)
    val daoState = execute(network) { proposer.daoApi.createPlutusDao(saltedDaoName, notaryName, "XYZ", 0.14) }
    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party)
    addMemberToPlutusDao(newMember, proposer, saltedDaoName)
    addMemberToPlutusDao(anotherMember, proposer, saltedDaoName)
    addMemberToPlutusDao(someoneElse, proposer, saltedDaoName)

    val tokenTypes = execute(network) { proposer.dglApi.listTokenTypes() }
    val xpltTokenType = tokenTypes.first { it.descriptor.symbol == "XYZ" }

    val plutusProposal = execute(network) { proposer.daoApi.createPlutusProposal(xpltTokenType.descriptor, 50, daoState.daoKey) }
    val (acceptedPlutusProposalLifecycle, acceptedPlutusVoteResult) = execute(network) { proposer.daoApi.acceptProposal(plutusProposal.proposal.proposalKey) }

    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedPlutusProposalLifecycle)
    Assert.assertEquals("vote result should be not_quorum", NOT_QUORUM, acceptedPlutusVoteResult)

    assertBalanceShouldBe(proposer, saltedDaoName, xpltTokenType, "0")
  }

  private fun createAndAcceptPlutusProposal(xpltTokenType: TokenTypeState, daoState: DaoState, accountId: String, expectedTokens: String, expectedTotalIssued: Double, expectedFinalMintingRate: Double) {
    val plutusProposal = execute(network) { proposer.daoApi.createPlutusProposal(xpltTokenType.descriptor, 50, daoState.daoKey) }
    execute(network) { newMember.daoApi.voteForProposal(plutusProposal.proposal.proposalKey, DOWN) }
    val (acceptedPlutusProposalLifecycle, acceptedPlutusVoteResult) = execute(network) { proposer.daoApi.acceptProposal(plutusProposal.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedPlutusProposalLifecycle)
    Assert.assertEquals("vote result should be down", DOWN_RESULT, acceptedPlutusVoteResult)

    // initial balances
    assertBalanceShouldBe(newMember, accountId, xpltTokenType, expectedTokens)
    assertBalanceShouldBe(proposer, accountId, xpltTokenType, expectedTokens)
    assertBalanceShouldBe(anotherMember, accountId, xpltTokenType, "0") // didn't vote - didn't get

    // race?
    val finalDaoState = proposer.daoApi.daoFor(daoState.daoKey)
    val finalPlutusModelData = finalDaoState.get(PlutusModelData::class)!!
    Assert.assertEquals("total issued tokens should be correct", expectedTotalIssued, finalPlutusModelData.totalIssued, 0.01)
    Assert.assertEquals("final minting rate should be", expectedFinalMintingRate, finalPlutusModelData.currentMintingRate, 0.001)
  }

  private fun addMemberToPlutusDao(newMemberNode: TestNode, proposerNode: TestNode, daoName: String) {
    val proposal = execute(network) { newMemberNode.daoApi.createNewMemberProposal(daoName, proposerNode.party.name) }

    Assert.assertEquals("should be 1 supporters", 1, proposal.votes.size)
    Assert.assertTrue("new member should be a supporter", proposal.votes.contains(Vote(newMemberNode.party, FOR)))

    // accept member proposal
    val (acceptedProposalLifecycle, voteResult) = execute(network) { newMember.daoApi.sponsorAcceptProposal(proposal.proposal.key(), proposal.daoKey, proposerNode.party.name) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    Assert.assertEquals("vote result should be passed", PASSED, voteResult)

    assertDaoStateContainsAtleastMembers(getDaoWithRetry(newMemberNode), proposerNode.party, newMemberNode.party)
  }

  private fun assertBalanceShouldBe(it: TestNode, accountId: String, xpltTokenType: TokenTypeState, expectedBalance: String) {
    val balance = execute(network) { it.dglApi.balanceForAccount(accountId) }
    if (BigDecimal(expectedBalance) == BigDecimal.ZERO) {
      Assert.assertTrue("there should be no balance", balance.none { it.amountType == xpltTokenType.descriptor })
    } else {
      val xpltBalance = balance.filter { it.amountType == xpltTokenType.descriptor }
      Assert.assertEquals("balance for ${it.party.name} should be correct", BigDecimal(expectedBalance), xpltBalance.first().quantity)
    }
  }

  @Test
  fun `should be able to receive dao updates using observable`(context: TestContext) {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    val async = context.async()

    // listen for expected update from...
    proposer.daoApi.listenForDaoUpdates().subscribe {
      val memberCount = it.members.size
      log.info("dao returned with member count: $memberCount")
      if (it.daoKey == daoState.daoKey && memberCount > 1) { // we get the first one back - not sure yet whether this is timing or by design.
        Assert.assertEquals("should be two members now", 2, memberCount)
        async.complete()
      }
    }

    // ...from adding a new member
    addMemberToDao(newMember, proposer, daoState.name)
  }

  @Test
  fun `should be able to receive proposal updates using observable`(context: TestContext) {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    val async = context.async()

    proposer.daoApi.listenForProposalUpdates().subscribe {
      log.info("proposal returned with key: ${it.proposal.key()}")
      if (it.daoKey == daoState.daoKey && it.proposal is MemberProposal) {
        val mp = it.proposal as MemberProposal
        Assert.assertEquals("should be new member type", MemberProposal.Type.NewMember, mp.type)
        async.complete()
      }
    }

    execute(network) { newMember.daoApi.createNewMemberProposal(daoState.name, proposer.party.name) }
  }

  private fun getDaoWithRetry(testNode: TestNode): List<DaoState> {
    (1..5).forEach { _ ->
      log.info("trying to get dao list")
      val daos = testNode.daoApi.daoInfo(saltedDaoName)
      if (daos.isNotEmpty()) {
        log.info("phew - daos returned")
        return daos
      }
      log.info("dao state not arrived yet - snoozing")
      Thread.sleep(100)
    }
    throw RuntimeException("Unable to get daos from node - are you sure this is a race condition?")
  }

  private fun createDaoWithName(daoName: String, notaryName: CordaX500Name): DaoState {
    Assert.assertEquals("there should be no dao at the beginning", 0, proposer.daoApi.daoInfo(daoName).size)
    val daoState = execute(network) { proposer.daoApi.createDao(daoName, 1, false, notaryName) }
    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party)
    return daoState
  }

  private fun addMemberToDao(newMemberNode: TestNode, proposerNode: TestNode, daoName: String, vararg extraSigners: TestNode) {
    val proposal = execute(network) { newMemberNode.daoApi.createNewMemberProposal(daoName, proposerNode.party.name) }

    Assert.assertEquals("should be 1 supporters", 1, proposal.votes.size)
    Assert.assertTrue("member should be a supporter", proposal.votes.contains(Vote(newMemberNode.party, FOR)))

    var numberOfSupporters = 1
    extraSigners.forEach {
      execute(network) { it.daoApi.voteForProposal(proposal.proposal.proposalKey, FOR) }
      val postVote = it.daoApi.proposalFor(proposal.proposal.proposalKey)
      Assert.assertEquals("there should be three supporters", ++numberOfSupporters, postVote.votes.size)
    }

    // accept member proposal
    val (acceptedProposalLifecycle, voteResult) = execute(network) { newMember.daoApi.sponsorAcceptProposal(proposal.proposal.key(), proposal.daoKey, proposerNode.party.name) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    Assert.assertEquals("vote result should be passed", PASSED, voteResult)

    assertDaoStateContainsMembers(getDaoWithRetry(newMemberNode), proposerNode.party, newMemberNode.party, *extraSigners.map { it.party }.toTypedArray())
  }

  private fun createAndAcceptProposalWithName(proposalName: String, proposalProposer: TestNode, daoState: DaoState, vararg supporters: TestNode) {
    val proposal = createProposal(proposalName, proposalProposer, daoState)

    supporters.forEach {
      execute(network) { it.daoApi.voteForProposal(proposal.proposal.proposalKey, FOR) }
    }

    val proposals = proposalProposer.daoApi.normalProposalsFor(daoState.daoKey)

    Assert.assertEquals("there should only be one proposal", 1, proposals.size)
    Assert.assertEquals("the proposal should have ${supporters.size + 1} supporters", supporters.size + 1, proposals.first().votes.size)
    Assert.assertTrue("the proposal should have all supporters", proposals.first().voters.containsAll(setOf(proposalProposer.party, *supporters.map { it.party }.toTypedArray())))

    val (acceptedProposalLifecycle, voteResult) = execute(network) { proposalProposer.daoApi.acceptProposal(proposal.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    Assert.assertEquals("vote result should be passed", PASSED, voteResult)
  }

  private fun createProposal(proposalName: String, proposalProposer: TestNode, daoState: DaoState): ProposalState<NormalProposal> {
    val proposal = execute(network) { proposalProposer.daoApi.createNormalProposal(proposalName, "some description", daoState.daoKey) }

    val origProposals = proposalProposer.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should only be one proposal", 1, origProposals.size)
    Assert.assertEquals("the proposal should have one supporter", 1, origProposals.first().votes.size)
    Assert.assertTrue("proposer should be supporter", origProposals.first().voters.containsAll(setOf(proposalProposer.party)))
    Assert.assertEquals("keys should be the same", proposal.proposal.proposalKey, origProposals.first().proposal.proposalKey)
    return proposal
  }
}
