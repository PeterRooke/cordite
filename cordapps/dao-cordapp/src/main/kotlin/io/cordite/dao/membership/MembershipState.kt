/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.membership

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import java.util.*

@CordaSerializable
data class MembershipKey(val name: String, val uuid: UUID = UUID.randomUUID()) {
  val uniqueIdentifier: UniqueIdentifier = UniqueIdentifier(name, uuid)
}

@JsonIgnoreProperties("linearId")
@BelongsToContract(MembershipContract::class)
data class MembershipState(val members: Set<Party>, val membershipKey: MembershipKey) : LinearState {

  override val participants: List<AbstractParty> = members.toList()

  override val linearId: UniqueIdentifier
    get() = membershipKey.uniqueIdentifier

  fun copyWith(newMember: Party): MembershipState {
    return MembershipState(members + newMember, membershipKey)
  }

  fun copyWithout(oldMember: Party): MembershipState {
    return MembershipState(members - oldMember, membershipKey)
  }

}