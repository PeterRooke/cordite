/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.utils.contextLogger
import io.cordite.dao.core.DaoKey
import io.cordite.dao.data.DataHelper
import io.cordite.dao.finality.ReceiveUtterFinalityFlow
import io.cordite.dao.finality.UtterFinalityFlow
import io.cordite.dao.membership.MembershipState
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.unwrap

// this is a distributed system.  in order to avoid bottlenecks, there are race conditions
// in this case it's possible that we don't know about all the proposals in the dao
// for the moment we will provide a way to ask a dao peer for proposals that we
// don't yet know about.  later we will fix this with gossip...
// pulling feels slightly preferable to pushing (on new membership) as pushing would imply that
// you had them all at the end, which you might not...


// Alice calls this to start conversation with another random member
@InitiatingFlow
@StartableByService
class RequestProposalConsistencyCheckFlow(val daoKey: DaoKey): FlowLogic<Set<ProposalKey>>() {

    companion object {
        private val log = contextLogger()
    }

    @Suspendable
    override fun call(): Set<ProposalKey> {
        log.info("asking a random member to check for $daoKey proposal consistency")
        val me = serviceHub.myInfo.legalIdentities.first()

        val dao = DataHelper.daoStateFor(daoKey, serviceHub)
        val otherMember = dao.members.first { it != me } // TODO pick random member? or parametrise? https://gitlab.com/cordite/cordite/issues/282

        val proposerFlowSession = initiateFlow(otherMember)
        proposerFlowSession.sendAndReceive<Set<ProposalKey>>(dao.daoKey).unwrap {
            return it
        }
    }

}

// Bob, another dao member, gets this, delegates to real flow
// hacks around the flow session issue
@InitiatedBy(RequestProposalConsistencyCheckFlow::class)
class RequestProposalConsistencyCheckFlowHack(val flowSession: FlowSession) : FlowLogic<Unit>() {

    companion object {
        private val log = contextLogger()
    }

    @Suspendable
    override fun call() {
        log.info("sponsor CreateNewMemberProposalFlow")
        flowSession.receive<DaoKey>().unwrap {
            val them = flowSession.counterparty

            val membershipState = DataHelper.membershipStateFor(it, serviceHub)
            if (!membershipState.members.contains(them)) {
                throw IllegalStateException("$them is not a member of $it")
            }

            val proposalKeys = subFlow(MakeDaoMembersConsistentWithProposalsFlow(it.name, membershipState))

            flowSession.send(proposalKeys)
        }
    }
}

// Bob does real work here
@InitiatingFlow
@StartableByService
class MakeDaoMembersConsistentWithProposalsFlow(private val daoName: String, private val membershipState: MembershipState): FlowLogic<Set<ProposalKey>>() {
// TODO ^^ switch to daoKey - this is effectively covered in
    companion object {
        private val log = contextLogger()
    }

    @Suspendable
    override fun call(): Set<ProposalKey> {
        val me = serviceHub.myInfo.legalIdentities.first()

        val daoState = DataHelper.daoStateFor(daoName, serviceHub) // this will be removed later...

        val proposalKeys = DataHelper.findProposalKeysWithInconsistentDaoMembers(daoState.daoKey, serviceHub)
        val notary = DataHelper.daoStateAndRefFor(daoState.daoKey, serviceHub).state.notary

        val inputProposals = DataHelper.proposalStateAndRefsFor(proposalKeys, serviceHub)

        if (inputProposals.isEmpty()) {
            return emptySet()
        }

        val txBuilder = ProposalContract.generateMemberConsistencyProposal(inputProposals, membershipState, notary)

        txBuilder.verify(serviceHub)

        val signedTx = serviceHub.signInitialTransaction(txBuilder)

        val nonSigningSessions = (inputProposals.flatMap { it.state.data.members + it.state.data.voters } - daoState.members).map { initiateFlow(it) }
        val flowSessions = daoState.members.filter{ it != me }.map{ initiateFlow(it) }
        log.info("there are ${flowSessions.size} sigs to get...")

        val fullySignedUnNotarisedTx = subFlow(CollectSignaturesFlow(signedTx, flowSessions, CollectSignaturesFlow.tracker()))

        val finalTx = subFlow(UtterFinalityFlow(fullySignedUnNotarisedTx, flowSessions + nonSigningSessions))

        log.info("the following proposals are now consistent with dao members: $proposalKeys in TxHash: $finalTx")
        return proposalKeys
    }

}


@InitiatedBy(MakeDaoMembersConsistentWithProposalsFlow::class)
class MakeDaoMembersConsistentWithProposalsFlowResponder(val creatorSession: FlowSession) : FlowLogic<Unit>() {

    companion object {
        private val log = contextLogger()
    }

    @Suspendable
    override fun call() {

        val signTransactionFlow = object : SignTransactionFlow(creatorSession, tracker()) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                // the tx has already been verified but we need to confirm that all the dao state members are signers

                val transaction = stx.toLedgerTransaction(serviceHub, false)
                val commands = transaction.commandsOfType(ProposalContract.Commands.DaoMemberConsistencyUpdate::class.java)
                @Suppress("UNCHECKED_CAST")
                val outputStates = transaction.outputStates as List<ProposalState<*>>

                "there should be exactly one command" using (commands.size == 1)

                val signers = commands.first().signers
                val proposalState = outputStates.first()
                val daoKey = proposalState.daoKey

                "no output state should be for a different dao" using (outputStates.none { it.daoKey != daoKey })

                val daoState = DataHelper.daoStateFor(daoKey, serviceHub)

                outputStates.forEach {
                    "all dao members must be proposal members" using ( it.members == daoState.members )
                }

                "all dao members must be signers" using ( signers.containsAll(daoState.members.map { it.owningKey }))

                log.info("all dao members are signers - accepting proposal")
            }
        }

        val stx = subFlow(signTransactionFlow)

        subFlow(ReceiveUtterFinalityFlow(creatorSession, expectedTxId = stx.id))
    }
}


