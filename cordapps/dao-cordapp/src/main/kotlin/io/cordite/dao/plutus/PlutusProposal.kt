/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.plutus

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.core.ChangeModelDataFlow
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MembershipState
import io.cordite.dao.proposal.CreateProposalFlowResponder
import io.cordite.dao.proposal.Proposal
import io.cordite.dao.proposal.ProposalKey
import io.cordite.dao.proposal.ProposalState
import io.cordite.dao.voting.NOT_QUORUM
import io.cordite.dao.voting.Vote
import io.cordite.dao.voting.VoteResult
import io.cordite.dgl.api.flows.token.flows.IssueTokensFlow
import io.cordite.dgl.api.flows.token.flows.MultiAccountTokenTransferFlow
import io.cordite.dgl.api.flows.tokentypes.findTokenTypesIssuesByMe
import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.token.BigDecimalAmount
import io.cordite.dgl.contract.v1.token.TokenContract
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import io.cordite.dgl.contract.v1.token.splitReasonablyEvently
import net.corda.core.contracts.Requirements.using
import net.corda.core.contracts.TimeWindow
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.TransactionBuilder
import java.math.BigDecimal
import java.math.RoundingMode

@CordaSerializable
data class PlutusProposal(val tokenType: TokenDescriptor, val basisPointChange: Int, val proposalKey: ProposalKey = ProposalKey("Issuance:${tokenType.symbol}:$basisPointChange")) : Proposal {

  init {
    checkValid()
  }

  override fun key() = proposalKey

  override fun verifyCreate(state: ProposalState<*>, daoState: DaoState) {
    "proposer must be only initial voter" using (state.voters == setOf(state.proposer))
    "there must be a plutus model data" using (daoState.containsModelData(PlutusModelData::class))
    val pmd = daoState.get(PlutusModelData::class)!!
    "only admin can create plutus proposals" using (state.proposer.name == pmd.plutusDaoOwner)
    checkValid()
  }

  @Suspendable
  fun checkValid() {
    "basis point change >= 0" using (basisPointChange >= 0)
    "basis point change <= 100" using (basisPointChange <= 100)
  }

  override fun verify(state: ProposalState<*>, membershipState: MembershipState) {
    super.verify(state, membershipState)
    "voters must be members" using (state.members.containsAll(state.voters))
  }

  override fun initialVotes(proposer: Party): Set<Vote> {
    return setOf(Vote(proposer, DOWN))
  }

  @Suspendable
  override fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    handleIssuanceAndTransferOnlyOnIssuer(inputDao, flowLogic, notary, proposalState.voteResult, proposalState.voters)
  }

  @Suspendable
  override fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    handleIssuanceAndTransferOnlyOnIssuer(inputDao, flowLogic, notary, proposalState.voteResult, proposalState.voters)
  }

  override fun verifyAccept(daoState: DaoState, proposalState: ProposalState<*>, tx: LedgerTransaction) {
    "there must be a time window" using (tx.timeWindow != null)
    "there must be a plutus model data" using (daoState.containsModelData(PlutusModelData::class))
    val timeWindow = tx.timeWindow!!
    "time window should be open ended" using (timeWindow.untilTime == null)
    val pmd = daoState.get(PlutusModelData::class)!!
    "time window start date should be same as modeldata min time" using (timeWindow.fromTime == pmd.minNextProposal)
    "only admin can accept plutus proposals" using (proposalState.proposer.name == pmd.plutusDaoOwner)
  }

  override fun extendTxn(txBuilder: TransactionBuilder, dao: DaoState) {
    txBuilder.setTimeWindow(TimeWindow.fromOnly(dao.get(PlutusModelData::class)!!.minNextProposal))
  }

  @Suspendable
  private fun handleIssuanceAndTransferOnlyOnIssuer(inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party, voteResult: VoteResult, voters: Set<Party>) {
    val myIdentity = flowLogic.ourIdentity
    when (tokenType.issuerName) {
      myIdentity.name -> {
        if (!inputDao.containsModelData(PlutusModelData::class)) {
          throw RuntimeException("you cannot issue plutus currency without a PlutusModelData")
        }

        if (voteResult == NOT_QUORUM) {
          flowLogic.logger.info("Plutus proposal $proposalKey was accepted with voting result $voteResult so doing nothing")
          return
        }

        flowLogic.logger.info("Plutus proposal accepted with voting result $voteResult")
        val plutusModelData = inputDao.get(PlutusModelData::class)!!

        val (newMintingRate, amountToIssue, newTotalIssued) = extractNewValues(plutusModelData, voteResult, flowLogic)

        flowLogic.logger.info("${tokenType.issuerName} is me, i am the issuer, so issuing $amountToIssue tokens to each of ${inputDao.members.size} members and transferring")
        val tokenTypeStateAndRef = flowLogic.serviceHub.findTokenTypesIssuesByMe(tokenType.symbol)
        val accountId = plutusModelData.accountName

        if (amountToIssue > 0) {
          val token = TokenContract.generateIssuance(flowLogic.serviceHub, amountToIssue.toString(), tokenTypeStateAndRef, accountId, myIdentity)
          val flow = IssueTokensFlow(token = token, notary = notary, description = "Token issuance")
          flowLogic.subFlow(flow)

          val portions = if (voters.any { it == myIdentity }) voters.size else voters.size + 1

          val splitQuantity = token.amount.quantity.splitReasonablyEvently(portions, 2).first() // issuer can have the last bit (ie rounding), others ought to be the same
          val splitAmount = BigDecimalAmount(splitQuantity, tokenTypeStateAndRef.state.data.descriptor)
          val fromAccount = AccountAddress(accountId, myIdentity.name)
          flowLogic.logger.info("non-issuers get $splitQuantity each")
          val from = listOf(fromAccount).zip(listOf(splitAmount))

          voters.minus(myIdentity).forEach {
            val to = listOf(AccountAddress(accountId, it.name)).zip(listOf(splitAmount))
            flowLogic.subFlow(
                MultiAccountTokenTransferFlow(from = from, to = to,
                    description = "issuance transfer of ${splitAmount.quantity} to ${it.name}", notary = notary))
          }
        }

        flowLogic.logger.info("now need to update model data so that the issued amount is $newTotalIssued and the new minting rate is $newMintingRate")
        val newPlutusModelData = plutusModelData.copyWith(newMintingRate, newTotalIssued)
        flowLogic.logger.info("min time for next proposal acceptance is ${newPlutusModelData.minNextProposal}")
        flowLogic.subFlow(ChangeModelDataFlow(newPlutusModelData, this.key(), inputDao.daoKey))
      }
      else -> {
        flowLogic.logger.info("not the issuer, so ignoring issuance acceptance...")
      }
    }
  }

  @Suspendable
  fun extractNewValues(plutusModelData: PlutusModelData, voteResult: VoteResult, flowLogic: FlowLogic<*>? = null): Triple<Double, Double, Double> {
    // if nothing has been issued, let's start with 1000 XCD
    if (plutusModelData.totalIssued == 0.0) {
      flowLogic?.logger?.info("initial issuance so current rate will be set to ${plutusModelData.currentMintingRate} and initial issuance will be ${plutusModelData.initialIssuance}")
      return Triple(plutusModelData.currentMintingRate, plutusModelData.initialIssuance, plutusModelData.initialIssuance)
    }

    val amountToApplyRateTo = if (plutusModelData.totalIssued > 0.0) plutusModelData.totalIssued else 7142858.41
    val proposedMintingRate = when (voteResult) {
      UP_RESULT -> plutusModelData.currentMintingRate + basisPointChange/10000.0
      else -> plutusModelData.currentMintingRate - basisPointChange/10000.0
    }
    val newMintingRate = plutusModelData.getBoundedRate(proposedMintingRate)

    val amountToIssue = BigDecimal(newMintingRate * amountToApplyRateTo).setScale(2, RoundingMode.HALF_EVEN).toDouble()
    val newTotalIssued = amountToApplyRateTo + amountToIssue

    if (flowLogic != null) {
      flowLogic.logger.info("amountToApplyRateTo: $amountToApplyRateTo")
      flowLogic.logger.info("currentMiningRate: ${plutusModelData.currentMintingRate}")
      flowLogic.logger.info("basisPointChange: $basisPointChange")
      flowLogic.logger.info("voting result: ${voteResult.result}")
      flowLogic.logger.info("newMintingRate: $newMintingRate")
      flowLogic.logger.info("amountToInfo: $amountToIssue")
      flowLogic.logger.info("newTotalIssued: $newTotalIssued")
    }

    return Triple(newMintingRate, amountToIssue, newTotalIssued)
  }

  @Suspendable
  override fun createResponderCheck(proposalState: ProposalState<*>, tx: LedgerTransaction, me: Party, flowLogic: FlowLogic<*>) {
    checkCreateAndAcceptResponder(flowLogic, tx, me)
  }

  @Suspendable
  override fun acceptResponderCheck(proposalState: ProposalState<*>, tx: LedgerTransaction, me: Party, flowLogic: FlowLogic<*>) {
    checkCreateAndAcceptResponder(flowLogic, tx, me)
  }

  private fun checkCreateAndAcceptResponder(flowLogic: FlowLogic<*>, tx: LedgerTransaction, me: Party) {
    // we can check a bit less here as we have been through verification
    flowLogic.logger.info("checking that the admin isn't being impersonated")
    val daoState = tx.referenceInputsOfType(DaoState::class.java).first()
    val pmd = daoState.get(PlutusModelData::class)!!

    // if i am the admin, i must not sign create or accept plutus proposals in responder flows - only i can create them
    // so i just plain should not be here - and i know that i am running this code, though not everyone else will
    "i should not be in the create or accept proposal responder flow as a proposer for plutus if i am admin" using (me.name != pmd.plutusDaoOwner)
  }
}