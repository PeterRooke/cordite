/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.membership

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.core.DaoState
import io.cordite.dao.proposal.MakeDaoMembersConsistentWithProposalsFlow
import io.cordite.dao.proposal.Proposal
import io.cordite.dao.proposal.ProposalKey
import io.cordite.dao.proposal.ProposalState
import io.cordite.dao.voting.FOR
import io.cordite.dao.voting.Vote
import net.corda.core.contracts.Requirements.using
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.TransactionBuilder

@CordaSerializable
data class MemberProposal(val member: Party, val daoName: String, val type: Type = Type.NewMember, val proposalKey: ProposalKey = ProposalKey("${type.name}:$daoName:${member.name}")) : Proposal {

  @CordaSerializable
  enum class Type { NewMember, RemoveMember }

  override fun key() = proposalKey

  override fun verifyCreate(state: ProposalState<*>, daoState: DaoState) {
    "there should be 1 supporter" using (state.votes.size == 1)
    if (type == Type.NewMember) {
      "new member must be a voter" using (state.voters.contains(member))
      enforceOneOrgRuleIfNecessary(daoState)
    } else {
      "proposer must be a voter" using (state.voters.contains(state.proposer))
    }
  }

  private fun enforceOneOrgRuleIfNecessary(daoState: DaoState) {
    if (daoState.membershipModelData().oneMemberPerOrg && type == Type.NewMember) {
      val newMemberOrg = member.name.organisation
      "new member must not belong to existing organisation" using (daoState.members.none { it.name.organisation == newMemberOrg })
    }
  }

  override fun verify(state: ProposalState<*>, membershipState: MembershipState) {
    when (type) {
      Type.NewMember -> {
        "new member must be a voter" using (state.voters.contains(member))
        "the proposal state members must be the same as the membership state's members with new member" using (state.members == membershipState.members + member)
      }
      Type.RemoveMember -> {
        "the proposal state members must be existing membership state's members without the removed member" using (state.members == membershipState.members - member)
      }
    }
    "other voters must be members" using (state.members.containsAll(state.votes.map{ it.voter }.filter { it != member }))
  }

  override fun initialVotes(proposer: Party): Set<Vote> {
    return when (type) {
      Type.NewMember -> setOf(Vote(member,FOR))
      Type.RemoveMember -> setOf(Vote(proposer, FOR))
    }
  }

  override fun initialParticipants(currentMembers: Set<Party>, proposer: Party): Set<Party> {
    return when(type) {
      Type.RemoveMember -> currentMembers - member
      Type.NewMember -> currentMembers + member
    }
  }

  @Suspendable
  override fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    val membershipState = flowLogic.subFlow(ChangeMemberFlow(this))
    flowLogic.subFlow(MakeDaoMembersConsistentWithProposalsFlow(daoName, membershipState))
    when (type) {
      Type.NewMember -> inputDao.propogateModelDataEvent(NewMemberModelDataEvent(member), flowLogic, notary)
      Type.RemoveMember -> {}
    }
  }

  @Suspendable
  override fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    when (type) {
      Type.NewMember -> inputDao.propogateModelDataEvent(NewMemberModelDataEvent(member), flowLogic, notary)
      Type.RemoveMember -> {}
    }
  }

  override fun verifyAccept(daoState: DaoState, proposalState: ProposalState<*>, tx: LedgerTransaction) {
    enforceOneOrgRuleIfNecessary(daoState)
  }

  override fun extendTxn(txBuilder: TransactionBuilder, dao: DaoState) {
    // not needed
  }
}