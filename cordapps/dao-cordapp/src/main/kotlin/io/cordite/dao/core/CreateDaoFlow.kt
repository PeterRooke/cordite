/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.utils.contextLogger
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.proposal.ProposalModelData
import net.corda.core.flows.*
import net.corda.core.identity.Party

@InitiatingFlow
@StartableByRPC
@StartableByService
class CreateDaoFlow(val name: String, val notary: Party, private val initialModelDataSet: Set<ModelData>) : FlowLogic<DaoState>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call(): DaoState {
    log.info("creating new Dao: $name")
    val me = serviceHub.myInfo.legalIdentities.first()

    val modelDataSet = initialModelDataSet.toMutableSet()
    if (initialModelDataSet.none { it is MembershipModelData }) modelDataSet.add(MembershipModelData(MembershipKey(name)))
    if (initialModelDataSet.none { it is ProposalModelData }) modelDataSet.add(ProposalModelData())

    val membershipModelData = modelDataSet.first { it is MembershipModelData } as MembershipModelData

    val (newMembershipState, txBuilder) = MembershipContract.generateCreate(me, membershipModelData.membershipKey, notary)
    val newDaoState = DaoContract.generateCreate(name, newMembershipState.members, modelDataSet, txBuilder = txBuilder)

    txBuilder.verify(serviceHub)

    val signedTx = serviceHub.signInitialTransaction(txBuilder)

    val finalTx = subFlow(FinalityFlow(signedTx, emptyList()))
    log.info("dao: $name created.  TxHash: $finalTx")

    // probably ought to see whether any of the model datas we've added have some sort of consequence
    log.info("allowing model data to handle dao creation...")
    initialModelDataSet.forEach { it.handleAcceptanceOnCreation(newDaoState, this) }

    log.info("model data creation handling completed....returning dao: $name")
    return newDaoState
  }

}