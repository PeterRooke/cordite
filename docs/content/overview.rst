Introduction
============

.. toctree::
   :hidden:
   
   overview/history
   overview/architecture
   overview/roadmap


One of the key unsolved challenges is how to support a decentralised
business model. When building decentralised applications, we believe
that there is no point replacing one centralised business model with
another. 2018 will be the year that many in the blockchain domain wake
up to the fact that there is little point in adopting DLT and
distributing their technology if they are not going to distribute the
business model as well.

Cordite provides decentralised economic and governance services
including:

-  decentralised stores and transfers of value allowing new financial
   instruments to be created inside the existing regulatory framework.
   eg. tokens, crypto-coins, digital cash, virtual currency, distributed
   fees, micro-billing
-  decentralised forms of governance allowing new digital autonomous
   organisations to be created using existing legal entities eg. digital
   mutual societies or digital joint stock companies
-  decentralised consensus in order to remove the need for a central
   operator, owner or authority. Allowing Cordite to be more resilient,
   cheaper, agile and private than incumbent market infrastructure

Built on Corda
~~~~~~~~~~~~~~

Distributed Ledger Technology (DLT) (‘blockchain’) is a disruptive
technology that could enable institutions to reduce costs, improve
product offerings and increase speed. There has been an explosion of
activity - with institutions actively researching, testing and investing
in this technology. Financial institutions have remained at the
forefront of this development, and have established a strong insight
into the underlying technology, the market landscape and the potential
value applications for their businesses.

Cordite is built on `Corda <http://corda.net>`__, a finance grade
distributed ledger technology, meeting the highest standards of the
banking industry, yet it is applicable to any commercial scenario. The
outcome of over two years of intense research and development by over 80
of the world’s largest financial institutions.

Cordite is open source, regulatory friendly, enterprise ready and
finance grade.


Cordite Design Principles
~~~~~~~~~~~~~~~~~~~~~~~~~

1. Don't fork Corda. Stronger together
2. Minimal core system with flexibility to extend
3. Support widest functionality and be least prescriptive
4. Provide exemplar CorDapps using Cordite
5. Minimise CorDapp knowledge of Cordite
6. Code is not law. Cordite will re-use existing legal frameworks to make law is code

Cordite Cordapp Design Principles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Don't be opinionated. You lose people for every opinion
2. Don't implement anything more than the minimum required. Let others extend
3. Don't wait. WYSIWYG. Raise feature requests on Corda and work round
4. Be Open Source (Presently 24 contributors, multiple maintainers from disparate orgs)
5. No manual test. Aggressive use of automated testing from unit to full scale environments
6. Sustain code quality and test coverage with code reviews and commit gates
7. Maintain a bi-weekly cadence. Have the ability to release at any moment in time from a "green" build
  
  