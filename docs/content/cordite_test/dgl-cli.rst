.. _dglcli:

DGL CLI Demonstration
=====================

.. admonition:: Current node addresses

    -  https://amer-test.cordite.foundation
    -  https://apac-test.cordite.foundation
    -  https://emea-test.cordite.foundation

In this CLI demonstration we will:
- Create two accounts on seperate nodes
- Create a token type
- Issue tokens of the created type to an account
- Look at the balance of that account to check the issuance was succesful
- transfer those tokens to the other account on the other node
- Look the balances of the accounts to check the transaction was succesful

.. raw:: html

    <script src="https://asciinema.org/a/ZemZQY7hOMr8427uhqHJVpS5z.js" id="asciicast-ZemZQY7hOMr8427uhqHJVpS5z" async data-theme="solarized-light"></script>

Commands used
-------------

+--------+-------------------------------------------------------------------------------------------------------------+
|  Node  | Command                                                                                                     |
+========+=============================================================================================================+
| amer   | ``notaries``                                                                                                |
+--------+-------------------------------------------------------------------------------------------------------------+
| amer   | ``ledger.createAccount("myBankAMER4", "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB")`` |
+--------+-------------------------------------------------------------------------------------------------------------+
| apac   | ``ledger.createAccount("myBankAPAC4", "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB")`` |
+--------+-------------------------------------------------------------------------------------------------------------+
| amer   | ``ledger.createTokenType("TOK4", 2, "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB")``   |
+--------+-------------------------------------------------------------------------------------------------------------+
| amer   | ``ledger.issueToken("myBankAMER4", 100, "TOK4", "my first token", "OU=Cordite Foundation, O=Cordite         |
|        | Guardian Notary, L=London, C=GB")``                                                                         |
+--------+-------------------------------------------------------------------------------------------------------------+
| amer   | ``ledger.balanceForAccount("myBankAMER4")``                                                                 |
+--------+-------------------------------------------------------------------------------------------------------------+
| amer   | ``ledger.transferToken(10, "TOK4:2:OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",           |
|        | "myBankAMER4", "myBankAPAC4@OU=Cordite Foundation, O=Cordite APAC, L=Singapore, C=SG", "my first            |
|        | transfer", "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB")``                            |
+--------+-------------------------------------------------------------------------------------------------------------+
| apac   | ``ledger.balanceForAccount("myBankAPAC4")``                                                                 |
+--------+-------------------------------------------------------------------------------------------------------------+
| amer   | ``ledger.balanceForAccount("myBankAMER4")``                                                                 |
+--------+-------------------------------------------------------------------------------------------------------------+

