<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->

# Cordite CLI

![cordite console in action](output.gif)

# Contents

- [Cordite CLI](#cordite-cli)
- [Contents](#contents)
- [Build](#build)
- [Parameters](#parameters)
- [Startup](#startup)
- [Shutdown](#shutdown)
- [API](#api)
- [Record a Demo](#record-a-demo)

# Simple installation

```
npm install -g cordite-cli
```

# Build

To build from a clean clone:

```
npm install
npm run build
```

Binaries will be placed in the respective folders under the [target](target) directory.

To clean the output:

```
npm run clean
```

# Parameters

The parameters can be listed using `cordite --help`.

```
node cordite.js --help

Cordite

  Client to connect to braid-enabled Corda nodes

Options

  --help                     print this usage guide
  -u, --url string           connection <protocol>://<host>:<port>
  -s, --strictSSL            enable or disable strict SSL checks
  -c, --credentials string   credentials payload
  -f, --script string        script file to be executed

example:
    cordite https://myhost.com:8081 --credentials '{ "username": "admin", "password": "admin"}' --strictSSL
```

The url can be passed as the only non-flagged parameter.

# Startup

Assuming you are in the directory with the binary for your OS

```
./cordite https://apac-test.cordite.foundation
```

You can also run the cordite client in a non-interactive mode by passing in a reference to a script file using the `-f` flag.

```
cordite -f myscript.js http://localhost:8083
```

The script can use ES6 async/await calls.

Here's an example of a script:

```javascript
console.log(notaries);
const notary = notaries.notaryService.name;
try {
  await ledger.createAccount("fuzz", notary);
} catch (err) {
  console.log(err.message);
}
const accounts = await ledger.listAccounts();
console.log(`accounts: ${JSON.stringify(accounts, null, 2)}`);
```

# Shutdown

Either type `.exit` or press `ctrl-c` twice.

# API

The CLI automatically creates top-level constants for each of the services available.

The following constants are presently available:

- `corda` - top-level proxy to the corda node
- `ledger` - the asset ledger
- `dao` - the service for managing decentralised autonomous organistaions
- `metering` - the service responsible for metering of transactions
- `network` - the Corda network
- `flows` - access to the raw Corda flows framework

You can discover what methods are available on each by typing the constant name and pressing enter.

You can also discover the documentation on a given method by invoking the `docs()` method on the method!

e.g.

`ledger.createAccount.docs()`

# Record a Demo

- [Install](https://asciinema.org/docs/installation) Asciinema
- [Install](https://github.com/asciinema/asciicast2gif) AsciiCast2Gif (suggest `npm install -g asciicast2gif`)
- Ensure you have an up-to-date version of `screen` (e.g. `brew install screen`)
- Startup a server somewhere. This guide assumes you have started this using `io/cordite/SimpleStandaloneNetwork.kt`
- Open a terminal window. Make sure it is going to be large enough for your demo. Resizing mid-demo is _not_ recommended!
- Run: `asciinema rec ~/tmp/recording.json`
- Either:
  - Single node: `./target/osx-x64/cordite [<url>]`
  - Multi node: `screen -c cordite-screen.rc`
    - To tab between each screen-split using `ctrl-a <Tab>`
- When done with your demo, exit from `cordite` using either `ctrl-c ctrl-c` or entering the `.exit` command
- Press `ctrl-d` to finish the `asciinema` recording
- Generate the gif using: `asciicast2gif -t solarized-light ~/tmp/recording.json ~/tmp/output.gif`
- Publish your gif here: `https://gitlab.com/cordite/cordite/wikis/cli-demos`
- Broadcast the gif to your network and become famous!
